using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System.Globalization;
using System.Xml;
using System.Xml.Linq;
using System.Collections.Generic;
using System.IO;
using System.Collections;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.Publishing;

namespace nteam.Layout.ZAP.Intranet.Features.MySitePrivate
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("1ddae76f-e27a-4362-a9db-000756069e60")]
    public class MySitePrivateEventReceiver : SPFeatureReceiver
    {
        // Auskommentierung der Methode unten aufheben, um das Ereignis zu behandeln, das nach der Aktivierung einer Funktion ausgel�st wird.

        //Masterpage Namen

        private const string MASTERSEITE = "ZAP-MySitePrivate.master";
        private const string _defaultMaster = "mysite15.master";


        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            ForceModules(properties);
            SetMasterpages(properties);
        }

        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {
            SetDefaultMasterpages(properties);
        }

        private void SetDefaultMasterpages(SPFeatureReceiverProperties properties)
        {
            try
            {

                SPWeb web = ((SPSite)properties.Feature.Parent).RootWeb;
                string oldMasterUrl = web.AllProperties["OldMasterUrl"].ToString();
                string oldCustomMasterUrl = web.AllProperties["OldCustomMasterUrl"].ToString();
                try
                {
                    bool fileExists = web.GetFile(oldMasterUrl).Exists;
                    if (fileExists)
                        web.MasterUrl = web.AllProperties["OldMasterUrl"].ToString();

                    bool CustomfileExists = web.GetFile(oldCustomMasterUrl).Exists;
                    if (CustomfileExists)
                        web.CustomMasterUrl = web.AllProperties["OldCustomMasterUrl"].ToString();
                }
                catch (ArgumentException)
                {
                    web.MasterUrl = web.MasterUrl.Substring(0, web.MasterUrl.LastIndexOf('/') + 1) + _defaultMaster;
                    web.CustomMasterUrl = web.CustomMasterUrl.Substring(0, web.CustomMasterUrl.LastIndexOf('/') + 1) + _defaultMaster;
                }

                // Alte Properties entfernen
                web.Properties["OldMasterUrl"] = null;
                web.AllProperties.Remove("OldMasterUrl");
                web.Properties["OldCustomMasterUrl"] = null;
                web.AllProperties.Remove("OldCustomMasterUrl");

                web.Update();
            }
            catch (Exception ex)
            {
                SPUtility.TransferToErrorPage("Fehler beim Entfernen der Masterpage: " + ex.Message);
            }
        }

        private void SetMasterpages(SPFeatureReceiverProperties properties)
        {
            try
            {
                SPWeb web = ((SPSite)properties.Feature.Parent).RootWeb;

                //alte Masterseite merken
                web.AllProperties["OldMasterUrl"] = web.MasterUrl;
                web.AllProperties["OldCustomMasterUrl"] = web.CustomMasterUrl;

                web.MasterUrl = web.MasterUrl.Substring(0, web.MasterUrl.LastIndexOf('/') + 1) + MASTERSEITE;
                web.CustomMasterUrl = web.CustomMasterUrl.Substring(0, web.CustomMasterUrl.LastIndexOf('/') + 1) + MASTERSEITE;
                web.Update();
            }
            catch (Exception ex)
            {
                SPUtility.TransferToErrorPage("Fehler beim Setzen der neuen Masterpage: " + ex.Message);
            }

        }

        private void ForceModules(SPFeatureReceiverProperties properties)
        {
            if (properties != null)
            {
                using (SPSite currentSite = (SPSite)properties.Feature.Parent)
                {

                    string forceModules = properties.Feature.Properties["ForceModules"].Value;
                    List<string> forceModule = new List<string>();
                    if (!string.IsNullOrEmpty(forceModules))
                        forceModule = forceModules.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                    using (var web = currentSite.OpenWeb())
                    {
                        var ElementDefinitions = properties.Definition.GetElementDefinitions(CultureInfo.CurrentCulture);

                        if (forceModule.Count == 0)
                            foreach (SPElementDefinition ElementDefinition in ElementDefinitions)
                            {
                                if (ElementDefinition.ElementType == "Module")
                                {
                                    Helper.UpdateFilesInModule(ElementDefinition, web);
                                }
                            }
                        else
                            foreach (SPElementDefinition ElementDefinition in ElementDefinitions)
                            {
                                XmlElement xmlElementNode = (XmlElement)ElementDefinition.XmlDefinition;
                                string moduleName = xmlElementNode.GetAttribute("Name");

                                if (ElementDefinition.ElementType == "Module" && !string.IsNullOrEmpty(moduleName) && forceModule.Contains(moduleName))
                                {
                                    Helper.UpdateFilesInModule(ElementDefinition, web);
                                }
                            }
                    }
                }
            }
        }
    }

    internal static class Helper
    {
        internal static void UpdateFilesInModule(SPElementDefinition elementDefinition, SPWeb web)
        {
            XElement xml = elementDefinition.XmlDefinition.ToXElement();
            XNamespace xmlns = "http://schemas.microsoft.com/sharepoint/";
            string featureDir = elementDefinition.FeatureDefinition.RootDirectory;
            Module module = (from m in xml.DescendantsAndSelf()
                             select new Module
                             {
                                 ProvisioningUrl = m.Attribute("Url").Value,
                                 ModuleName = m.Attribute("Name").Value,
                                 PhysicalPath = m.Attribute("Path") != null ? Path.Combine(featureDir, m.Attribute("Path").Value) : "",
                                 Files =
                                     (from f in m.Elements(xmlns.GetName("File"))
                                      select new Module.File
                                      {
                                          Name2 = f.Attribute("Name") != null ? f.Attribute("Name").Value : "",
                                          Name = f.Attribute("Url").Value,
                                          Properties = (
                                              from p in f.Elements(xmlns.GetName("Property"))
                                              select p).ToDictionary(n => n.Attribute("Name").Value, v => v.Attribute("Value").Value)
                                      }).ToArray()
                             }).First();
            if (module == null || string.IsNullOrEmpty(module.PhysicalPath))
            {
                return;
            }
            foreach (Module.File file in module.Files)
            {
                string physicalPath = Path.Combine(module.PhysicalPath, file.Name);
                string virtualPath = string.Empty;
                if (module.ModuleName != "PreviewImages")
                    virtualPath = string.Concat(web.Url, "/", module.ProvisioningUrl, "/", file.Name);
                else
                    virtualPath = string.Concat(web.Url, "/", module.ProvisioningUrl, "/", file.Name2);
                if (File.Exists(physicalPath))
                {
                    using (StreamReader sreader = new StreamReader(physicalPath))
                    {
                        if (!CheckOutStatus(web.GetFile(virtualPath)))
                        {
                            web.GetFile(virtualPath).CheckOut();
                        }
                        SPFile spFile = web.Files.Add(virtualPath, sreader.BaseStream, new Hashtable(file.Properties), true);
                        spFile.CheckIn("Updated", SPCheckinType.MajorCheckIn);
                        if (CheckContentApproval(spFile.Item))
                        {
                            spFile.Approve("Updated");
                        }
                        spFile.Update();
                    }
                }
            }
        }
        private static bool CheckOutStatus(SPFile file)
        {
            if (file.CheckOutStatus != SPFile.SPCheckOutStatus.None)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private static bool CheckContentApproval(SPListItem listitem)
        {
            bool isContentApprovalEnabled = listitem.ParentList.EnableModeration;
            return isContentApprovalEnabled;
        }
        public static XElement ToXElement(this XmlNode node)
        {
            XDocument xDoc = new XDocument();
            using (XmlWriter xmlWriter = xDoc.CreateWriter()) node.WriteTo(xmlWriter);
            return xDoc.Root;
        }
    }
    public class Module
    {
        public string ProvisioningUrl
        {
            get;
            set;
        }
        public string PhysicalPath
        {
            get;
            set;
        }
        public string _ModuleName;

        public string ModuleName
        {
            get { return _ModuleName; }
            set { _ModuleName = value; }
        }

        public Module.File[] Files
        {
            get;
            set;
        }
        public class File
        {
            public string Name
            {
                get;
                set;
            }
            public string Name2
            {
                get;
                set;
            }
            public Dictionary<string, string> Properties
            {
                get;
                set;
            }
        }
    }
}