﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="0e35b1d1-b28c-4d0b-9525-91250dbcebed" activateOnDefault="false" description="Überwacht das Anlegen von SubSites und stellt die Vererbung der Gestaltungsvorlage ein." featureId="0e35b1d1-b28c-4d0b-9525-91250dbcebed" imageUrl="zap/nteamSolution.png" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="ZAP - SubSite Stapler 2.0" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="30194db9-8235-4946-9fd4-c161b23d150c" />
  </projectItems>
</feature>