﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="862ac3fd-a913-439a-9cab-cbea945f8203" activateOnDefault="false" alwaysForceInstall="true" description="Stellt das Websitedesign für Anwendungen der ZAP ein. Voraussetzungen: Feature &quot;SharePoint Server-Veröffentlichungsinfrastruktur&quot; und Feature &quot;ZAP - Websitespalten und Inhaltstypen&quot;" featureId="862ac3fd-a913-439a-9cab-cbea945f8203" imageUrl="zap/nteamSolution.png" receiverAssembly="$SharePoint.Project.AssemblyFullName$" receiverClass="$SharePoint.Type.57d3acd9-66f2-4ddb-a42a-b03db81034aa.FullName$" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="ZAP - Layout 2.1" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <activationDependencies>
    <referencedFeatureActivationDependency minimumVersion="" itemId="cfaece80-a80d-426b-a4a9-a4cf85ab3edc" />
  </activationDependencies>
  <projectItems>
    <projectItemReference itemId="2a573383-e5b3-421a-9f03-1a6034ee928b" />
    <projectItemReference itemId="3dc72fe1-d7e8-4273-847b-db49f548b5d2" />
    <projectItemReference itemId="abfa95f3-6da6-4e37-98a1-a18060251321" />
    <projectItemReference itemId="2bf02b81-9088-4059-b7dc-827aa3897191" />
    <projectItemReference itemId="ec0aa36f-1727-4079-812d-79ee7388b14f" />
    <projectItemReference itemId="92c0f326-ae39-4abe-8dc9-cc150d0e445b" />
    <projectItemReference itemId="a7d7578a-94cb-45d8-aa22-4277b909f9d9" />
    <projectItemReference itemId="9ba68b25-9518-4bb2-a521-060a23c8fe22" />
  </projectItems>
</feature>