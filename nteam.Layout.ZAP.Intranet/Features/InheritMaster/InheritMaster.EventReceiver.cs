using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;

namespace nteam.Layout.ZAP.Intranet.Features.InheritMaster
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("52c33fe6-427e-4c89-89a0-5131c811ce6c")]
    public class InheritMasterEventReceiver : SPFeatureReceiver
    {
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            SPWeb curweb = properties.Feature.Parent as SPWeb;
            //Masterpagevererbung
            SetMasterpage(curweb);
            //Theme
            //SetTheme(curweb);



        }

        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {

        }

        public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        {

        }

        public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        {

        }

        private void SetMasterpage(SPWeb curweb)
        {
            curweb.AllowUnsafeUpdates = true;

            bool success = true;
            string errorMessage = string.Empty;
            var rootWeb = curweb.Site.RootWeb;
            try
            {
                curweb.MasterUrl = rootWeb.MasterUrl;
                curweb.Update();
                SetWebPropertyValue(curweb, "__InheritsMasterUrl", "True");
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                success = false;
            }

            if (success)
            {
                try
                {
                    curweb.CustomMasterUrl = rootWeb.CustomMasterUrl;
                    curweb.Update();
                    SetWebPropertyValue(curweb, "__InheritsCustomMasterUrl", "True");
                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                    success = false;
                }
            }

            if (success)
            {
                try
                {
                    curweb.AlternateCssUrl = rootWeb.AlternateCssUrl;
                    curweb.Update();
                    SetWebPropertyValue(curweb, "__InheritsAlternateCssUrl", "True");
                }

                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                    success = false;
                }
            }

            if (!success)
                throw new ArgumentException("Can't inherit Masterpage: " + errorMessage);
        }

        private void SetWebPropertyValue(SPWeb targetWeb, object key, object value)
        {
            if (targetWeb.AllProperties.ContainsKey(key))
            {
                targetWeb.AllProperties[key] = value;
            }
            else
            {
                targetWeb.AllProperties.Add(key, value);
            }
            targetWeb.Update();
        }
        //private void SetTheme(SPWeb curweb)
        //{
        // curweb.ApplyTheme("ZAP");
        //}
    }
}