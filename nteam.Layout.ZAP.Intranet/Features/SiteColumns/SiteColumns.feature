﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="4500e21d-7ba7-45f0-9b11-5b0cc59ebe3a" activateOnDefault="false" description="Erstellt Websitespalten und Inhaltstypen für Intranet Anwendungen der ZAP." featureId="4500e21d-7ba7-45f0-9b11-5b0cc59ebe3a" imageUrl="zap/nteamSolution.png" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="ZAP - Websitespalten und Inhaltstypen 1.0" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <activationDependencies>
    <customFeatureActivationDependency minimumVersion="" featureTitle="SharePoint Server-Veröffentlichungsinfrastruktur" featureId="f6924d36-2fa8-4f0b-b16d-06b7250180fa" solutionId="00000000-0000-0000-0000-000000000000" solutionUrl="" />
  </activationDependencies>
  <projectItems>
    <projectItemReference itemId="4a7d4802-b308-48b4-a772-64a289d55508" />
    <projectItemReference itemId="df8cf8c1-8484-4d8f-b62c-b7c63054de01" />
    <projectItemReference itemId="66b7fa2b-8243-4bf0-9fa4-646bbc7d6398" />
    <projectItemReference itemId="1d340457-7181-4422-a480-80a857182183" />
    <projectItemReference itemId="42afe5b6-26f8-4ec5-b0cb-58e9193a4016" />
    <projectItemReference itemId="ddedcba9-df07-4d05-85c2-58b43c9c8d19" />
    <projectItemReference itemId="ab130cf5-c99b-41ce-8a12-ddb26b6b2319" />
    <projectItemReference itemId="10566e94-088c-4574-9405-3ee2895f74d8" />
    <projectItemReference itemId="5788d643-7ab6-4b22-baab-a418bb098225" />
    <projectItemReference itemId="8be0bee9-f02f-4a10-a14c-0f071a573dab" />
    <projectItemReference itemId="6b89287b-440c-46e9-86d6-584f0599a16b" />
    <projectItemReference itemId="3a18ee56-ef51-4cd2-a3bd-9dea22092236" />
    <projectItemReference itemId="96f30a0d-6f18-485b-8a3c-998739fcb45b" />
    <projectItemReference itemId="4f6afcd6-d782-4760-9749-88fd04960d33" />
    <projectItemReference itemId="e953981a-934a-4dc2-9f8f-9b02e1465a2a" />
    <projectItemReference itemId="cfd48cb7-95fa-4845-a023-7472ce8e9e64" />
    <projectItemReference itemId="796ff60c-92bf-4549-b8be-fb4115ccc305" />
    <projectItemReference itemId="02444f5f-e9c7-4ba5-b4f1-685447667738" />
    <projectItemReference itemId="d3c66386-721e-450a-b774-609b41211ab0" />
    <projectItemReference itemId="5bb18cba-7d96-42b7-aa69-abe3c3e75471" />
    <projectItemReference itemId="c20c6ea4-ce92-4a56-aeb8-1ceebec71bf9" />
    <projectItemReference itemId="90a92069-478f-4a33-acb8-a4b1ff378619" />
    <projectItemReference itemId="39065043-1a80-4863-a04a-6540a5c7f575" />
    <projectItemReference itemId="0435f1fe-7ca1-42c8-ab5e-50c501c4a75e" />
    <projectItemReference itemId="5ab54f37-9fa7-4bd9-b4a0-3ae58c991770" />
    <projectItemReference itemId="8ead9435-c2fa-4d4b-bcad-66768507df3b" />
    <projectItemReference itemId="c45cb3c8-5a82-4673-8891-ca90ab4bd4ad" />
    <projectItemReference itemId="34ea4965-dc75-4a7d-b276-c404f0b5fab7" />
    <projectItemReference itemId="49fe0d60-b9b0-4d19-9114-90a92cc424f4" />
    <projectItemReference itemId="693bea7d-45d8-473d-b165-6a16a6b53fe2" />
  </projectItems>
</feature>