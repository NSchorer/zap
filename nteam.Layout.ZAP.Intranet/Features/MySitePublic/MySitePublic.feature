﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="34c697d3-59af-4541-ac34-d490975fdd81" activateOnDefault="false" description="Stellt das ZAP Websitedesign für Profilseiten (MySite Public) ein. Nur in der UserProfil-Websitesammlung aktivieren!" featureId="34c697d3-59af-4541-ac34-d490975fdd81" imageUrl="zap/nteamSolution.png" receiverAssembly="$SharePoint.Project.AssemblyFullName$" receiverClass="$SharePoint.Type.8217ecf4-5080-494c-a247-189e46ab42c3.FullName$" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="ZAP - Master MySite Public 2.0" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <activationDependencies>
    <customFeatureActivationDependency minimumVersion="" featureTitle="SharePoint Server-Veröffentlichungsinfrastruktur" featureId="f6924d36-2fa8-4f0b-b16d-06b7250180fa" solutionId="00000000-0000-0000-0000-000000000000" solutionUrl="" />
  </activationDependencies>
  <projectItems>
    <projectItemReference itemId="0d7a3968-5ea6-42f2-bea6-fe1f623b94f8" />
  </projectItems>
</feature>