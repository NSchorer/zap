<xsl:stylesheet
    version="1.0"
    exclude-result-prefixes="x xsl cmswrt cbq"
    xmlns:x="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:cmswrt="http://schemas.microsoft.com/WebPart/v3/Publishing/runtime"
    xmlns:cbq="urn:schemas-microsoft-com:ContentByQueryWebPart">
  <xsl:output method="xml" indent="no" media-type="text/html" omit-xml-declaration="yes"/>
  <xsl:param name="cbq_isgrouping" />
  <xsl:param name="cbq_columnwidth" />
  <xsl:param name="Group" />
  <xsl:param name="GroupType" />
  <xsl:param name="cbq_iseditmode" />
  <xsl:param name="cbq_viewemptytext" />
  <xsl:param name="cbq_errortext" />
  <xsl:param name="SiteId" />
  <xsl:param name="WebUrl" />
  <xsl:param name="PageId" />
  <xsl:param name="WebPartId" />
  <xsl:param name="FeedPageUrl" />
  <xsl:param name="FeedEnabled" />
  <xsl:param name="SiteUrl" />
  <xsl:param name="BlankTitle" />
  <xsl:param name="BlankGroup" />
  <xsl:param name="UseCopyUtil" />
  <xsl:param name="DataColumnTypes" />
  <xsl:param name="ClientId" />
  <xsl:param name="Source" />
  <xsl:param name="RootSiteRef" />
  <xsl:param name="CBQPageUrl" />
  <xsl:param name="CBQPageUrlQueryStringForFilters" />
  <xsl:param name="EffectiveDeviceChannel" />

  <xsl:variable name="BeginList" select="string('&lt;ul class=&quot;dfwp-list&quot;&gt;')" />
  <xsl:variable name="EndList" select="string('&lt;/ul&gt;')" />
  <xsl:variable name="BeginListItem" select="string('&lt;li class=&quot;dfwp-item&quot;&gt;')" />
  <xsl:variable name="EndListItem" select="string('&lt;/li&gt;')" />

  <xsl:variable name="BeginListBildergalerie" select="string('&lt;link rel=&quot;stylesheet&quot; type=&quot;text/css&quot; href=&quot;/_layouts/15/styles/ZAP/gallery/style.css&quot; /&gt;		&lt;link rel=&quot;stylesheet&quot; type=&quot;text/css&quot; href=&quot;/_layouts/15/styles/ZAP/gallery/elastislide.css&quot; /&gt;&lt;div id=&quot;rg-gallery&quot; class=&quot;rg-gallery&quot; &gt;	&lt;div class=&quot;rg-thumbs&quot;&gt;		&lt;div class=&quot;es-carousel-wrapper&quot; &gt;			&lt;div class=&quot;es-nav&quot;&gt;				&lt;span class=&quot;es-nav-prev&quot;&gt;Previous&lt;/span&gt;				&lt;span class=&quot;es-nav-next&quot;&gt;Next&lt;/span&gt;			&lt;/div&gt;			&lt;div class=&quot;es-carousel ntm_Gallery&quot;&gt;				&lt;ul&gt;')" />
  <xsl:variable name="EndListBildergalerie" select="string('&lt;/ul&gt;			&lt;/div&gt;		&lt;/div&gt;	&lt;/div&gt;&lt;/div&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;/_layouts/15/scripts/ZAP/gallery/jquery.tmpl.min.js&quot;&gt;&lt;/script&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;/_layouts/15/scripts/ZAP/gallery/jquery.easing.1.3.js&quot;&gt;&lt;/script&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;/_layouts/15/scripts/ZAP/gallery/jquery.elastislide.js&quot;&gt;&lt;/script&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;/_layouts/15/scripts/ZAP/gallery/gallery-light.js&quot;&gt;&lt;/script&gt;&lt;script id=&quot;img-wrapper-tmpl&quot; type=&quot;text/x-jquery-tmpl&quot;&gt;	&lt;div class=&quot;rg-image-wrapper&quot;&gt;		{{if itemsCount &gt; 1}}			&lt;div class=&quot;rg-image-nav&quot;&gt;				&lt;a href=&quot;#&quot; class=&quot;rg-image-nav-prev&quot;&gt;Previous Image&lt;/a&gt;				&lt;a href=&quot;#&quot; class=&quot;rg-image-nav-next&quot;&gt;Next Image&lt;/a&gt;			&lt;/div&gt;		{{/if}}		&lt;div class=&quot;rg-image&quot;&gt;&lt;/div&gt;		&lt;div class=&quot;rg-loading&quot;&gt;&lt;/div&gt;		&lt;div class=&quot;rg-caption-wrapper&quot;&gt;			&lt;div class=&quot;rg-caption&quot; style=&quot;display:none;&quot;&gt;				&lt;p&gt;&lt;/p&gt;			&lt;/div&gt;		&lt;/div&gt;	&lt;/div&gt;&lt;/script&gt;')" />
  <xsl:variable name="BeginListItemBildergalerie" select="string('&lt;li&gt;')" />
  <xsl:variable name="EndListItemBildergalerie" select="string('&lt;/li&gt;')" />

  <xsl:variable name="BeginListBildergalerieLight" select="string('&lt;link rel=&quot;stylesheet&quot; type=&quot;text/css&quot; href=&quot;/_layouts/15/styles/ZAP/gallery/style.css&quot; /&gt;		&lt;link rel=&quot;stylesheet&quot; type=&quot;text/css&quot; href=&quot;/_layouts/15/styles/ZAP/gallery/elastislide.css&quot; /&gt;  &lt;div id=&quot;rg-gallery&quot; class=&quot;rg-gallery ntm_GalleryLight&quot; &gt;	&lt;div class=&quot;rg-thumbs&quot;&gt;		&lt;div class=&quot;es-carousel-wrapper&quot; &gt;			&lt;div class=&quot;es-nav&quot;&gt;				&lt;span class=&quot;es-nav-prev&quot;&gt;Previous&lt;/span&gt;				&lt;span class=&quot;es-nav-next&quot;&gt;Next&lt;/span&gt;			&lt;/div&gt;			&lt;div class=&quot;es-carousel ntm_GalleryLight&quot;&gt;				&lt;ul&gt;')" />
  <xsl:variable name="EndListBildergalerieLight" select="string('&lt;/ul&gt;			&lt;/div&gt;		&lt;/div&gt;	&lt;/div&gt;&lt;/div&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;/_layouts/15/scripts/ZAP/gallery/jquery.tmpl.min.js&quot;&gt;&lt;/script&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;/_layouts/15/scripts/ZAP/gallery/jquery.easing.1.3.js&quot;&gt;&lt;/script&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;/_layouts/15/scripts/ZAP/gallery/jquery.elastislide-light.js&quot;&gt;&lt;/script&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;/_layouts/15/scripts/ZAP/gallery/gallery-light.js&quot;&gt;&lt;/script&gt;  &lt;script id=&quot;img-wrapper-tmpl&quot; type=&quot;text/x-jquery-tmpl&quot;&gt;	&lt;div class=&quot;rg-image-wrapper ntm_GalleryLight&quot; &gt;	{{if itemsCount &gt; 1}}			&lt;div class=&quot;rg-image-nav&quot;&gt;				&lt;a href=&quot;#&quot; class=&quot;rg-image-nav-prev&quot;&gt;Previous Image&lt;/a&gt;				&lt;a href=&quot;#&quot; class=&quot;rg-image-nav-next&quot;&gt;Next Image&lt;/a&gt;			&lt;/div&gt;		{{/if}}		&lt;div class=&quot;rg-image&quot;&gt;&lt;/div&gt;		&lt;div class=&quot;rg-loading&quot;&gt;&lt;/div&gt;		&lt;div class=&quot;rg-caption-wrapper&quot;&gt;			&lt;div class=&quot;rg-caption ntm_GalleryLight&quot; &gt;				&lt;p&gt;&lt;/p&gt;			&lt;/div&gt;		&lt;/div&gt;	&lt;/div&gt;&lt;/script&gt;')" />
  <xsl:variable name="BeginListItemBildergalerieLight" select="string('&lt;li&gt;')" />
  <xsl:variable name="EndListItemBildergalerieLight" select="string('&lt;/li&gt;')" />

  <xsl:variable name="BeginListZweispaltig" select="string('&lt;div class=&quot;ntm_ZweiSpaltig&quot;&gt;')" />
  <xsl:variable name="BeginListBilderfade" select="string('&lt;div class=&quot;ntm_DivCycler&quot;&gt;')" />
  <xsl:variable name="BeginListAkkordeon" select="string('&lt;div id=&quot;NTM_Gruppierung&quot; class=&quot;ntm_Akkordeon&quot;&gt;')" />
  <xsl:variable name="BeginListSlider" select="string('&lt;section class=&quot;slider ntm_SliderHold ntm_SliderStandard&quot;&gt;&lt;div class=&quot;flexslider&quot;&gt;&lt;ul class=&quot;slides&quot;&gt;')" />
  <xsl:variable name="BeginListSliderOne" select="string('&lt;section class=&quot;slider ntm_SliderHold ntm_SliderOne&quot;&gt;&lt;div class=&quot;flexslider&quot;&gt;&lt;ul class=&quot;slides&quot;&gt;')" />
  <xsl:variable name="BeginListSliderMore" select="string('&lt;section class=&quot;slider ntm_SliderHold ntm_SliderMore&quot;&gt;&lt;div class=&quot;flexslider&quot;&gt;&lt;ul class=&quot;slides&quot;&gt;')" />
  <xsl:variable name="BeginListSliderA" select="string('&lt;div class=&quot;ntm_SliderHold&quot;&gt;&lt;div class=&quot;flex-containerA&quot;&gt;&lt;div class=&quot;flexslider flexsliderA&quot;&gt;&lt;ul class=&quot;slides&quot;&gt;')" />
  <xsl:variable name="BeginListSliderIcons" select="string('&lt;section class=&quot;ntm_IconSliderHold&quot;&gt;&lt;div class=&quot;flexslider ntm_IconSlider&quot; &gt;&lt;ul class=&quot;slides&quot; &gt;')" />
  <xsl:variable name="EndListSlider" select="string('&lt;/ul&gt;&lt;/div&gt;&lt;/section&gt;')" />
  <xsl:variable name="BeginListItemSlider" select="string('&lt;li&gt;')" />

  <xsl:variable name="BeginListTable" select="string('&lt;div class=&quot;ntm_Table&quot;&gt;')" />
  <xsl:variable name="BeginListBlank" select="string('&lt;div class=&quot;ntm_Blank&quot;&gt;')" />
  <xsl:variable name="EndListBlank" select="string('&lt;/div&gt;')" />
  <xsl:variable name="BeginListItemBlank" select="string('')" />
  <xsl:variable name="EndListItemBlank" select="string('')" />

  <xsl:variable name="ItemStyle" select="@Style" />
  <xsl:template match="/">
    <xsl:call-template name="OuterTemplate" />
  </xsl:template>
  <xsl:template name="OuterTemplate">
    <xsl:variable name="Rows" select="/dsQueryResponse/Rows/Row" />
    <xsl:variable name="RowCount" select="count($Rows)" />
    <xsl:variable name="IsEmpty" select="$RowCount = 0" />
    <div id="{concat('cbqwp', $ClientId)}" class="cbq-layout-main">
      <xsl:if test="$cbq_iseditmode = 'True' and string-length($cbq_errortext) != 0">
        <div class="wp-content description">
          <xsl:value-of disable-output-escaping="yes" select="$cbq_errortext" />
        </div>
      </xsl:if>
      <xsl:choose>
        <xsl:when test="$IsEmpty and $ItemStyle='SliderQuicklinks'">
          <xsl:call-template name="OuterTemplate.QuickEmpty" >
            <xsl:with-param name="EditMode" select="$cbq_iseditmode" />
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="$IsEmpty">
          <xsl:call-template name="OuterTemplate.Empty" >
            <xsl:with-param name="EditMode" select="$cbq_iseditmode" />
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="OuterTemplate.Body">
            <xsl:with-param name="Rows" select="$Rows" />
            <xsl:with-param name="FirstRow" select="1" />
            <xsl:with-param name="LastRow" select="$RowCount" />
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </div>
    <xsl:if test="$FeedEnabled = 'True' and $PageId != ''">
      <div class="cqfeed">
        <xsl:variable name="FeedUrl1" select="concat($SiteUrl,$FeedPageUrl,'xsl=1&amp;web=',$WebUrl,'&amp;page=',$PageId,'&amp;wp=',$WebPartId,'&amp;pageurl=',$CBQPageUrl,$CBQPageUrlQueryStringForFilters)" />
        <a href="{cmswrt:RegisterFeedUrl( $FeedUrl1, 'application/rss+xml')}">
          <img src="/_layouts/15/images/rss.gif?rev=23" border="0" alt="{cmswrt:GetPublishingResource('CbqRssAlt')}"/>
        </a>
      </div>
    </xsl:if>
  </xsl:template>
  <xsl:template name="OuterTemplate.Empty">
    <xsl:param name="EditMode" />
    <div class="ntm_Empty">
      &#160;
      <xsl:if test="$EditMode = 'True' and string-length($cbq_errortext) = 0">
        <div class="wp-content description">
          <xsl:value-of disable-output-escaping="yes" select="$cbq_viewemptytext" />
        </div>
      </xsl:if>
    </div>
  </xsl:template>
  <xsl:template name="OuterTemplate.QuickEmpty">
    <xsl:param name="EditMode" />
    <div class="ntm_Empty ntm_QuickEmpty">
      &#160;
      <xsl:if test="$EditMode = 'True' and string-length($cbq_errortext) = 0">
        <div class="wp-content description">
          <xsl:value-of disable-output-escaping="yes" select="$cbq_viewemptytext" />
        </div>
      </xsl:if>
    </div>
  </xsl:template>
  <xsl:template name="OuterTemplate.Body">
    <xsl:param name="Rows" />
    <xsl:param name="FirstRow" />
    <xsl:param name="LastRow" />
    <xsl:variable name="BeginColumn1" select="string('&lt;ul class=&quot;dfwp-column dfwp-list&quot; style=&quot;width:')" />
    <xsl:variable name="BeginColumn2" select="string('%&quot; &gt;')" />
    <xsl:variable name="BeginColumn" select="concat($BeginColumn1, $cbq_columnwidth, $BeginColumn2)" />
    <xsl:variable name="EndColumn" select="string('&lt;/ul&gt;')" />

    <xsl:variable name="BeginColumnBildergalerie" select="string('&lt;link rel=&quot;stylesheet&quot; type=&quot;text/css&quot; href=&quot;/_layouts/15/styles/ZAP/gallery/style.css&quot; /&gt;		&lt;link rel=&quot;stylesheet&quot; type=&quot;text/css&quot; href=&quot;/_layouts/15/styles/ZAP/gallery/elastislide.css&quot; /&gt;&lt;div id=&quot;rg-gallery&quot; class=&quot;rg-gallery&quot;&gt;	&lt;div class=&quot;rg-thumbs&quot;&gt;		&lt;div class=&quot;es-carousel-wrapper&quot;&gt;			&lt;div class=&quot;es-nav&quot;&gt;				&lt;span class=&quot;es-nav-prev&quot;&gt;Previous&lt;/span&gt;				&lt;span class=&quot;es-nav-next&quot;&gt;Next&lt;/span&gt;			&lt;/div&gt;			&lt;div class=&quot;es-carousel ntm_Gallery&quot;&gt;				&lt;ul&gt;')" />
    <xsl:variable name="EndColumnBildergalerie" select="string('&lt;/ul&gt;			&lt;/div&gt;		&lt;/div&gt;	&lt;/div&gt;&lt;/div&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;/_layouts/15/scripts/ZAP/gallery/jquery.tmpl.min.js&quot;&gt;&lt;/script&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;/_layouts/15/scripts/ZAP/gallery/jquery.easing.1.3.js&quot;&gt;&lt;/script&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;/_layouts/15/scripts/ZAP/gallery/jquery.elastislide.js&quot;&gt;&lt;/script&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;/_layouts/15/scripts/ZAP/gallery/gallery-light.js&quot;&gt;&lt;/script&gt;&lt;script id=&quot;img-wrapper-tmpl&quot; type=&quot;text/x-jquery-tmpl&quot;&gt;	&lt;div class=&quot;rg-image-wrapper&quot;&gt;		{{if itemsCount &gt; 1}}			&lt;div class=&quot;rg-image-nav&quot;&gt;				&lt;a href=&quot;#&quot; class=&quot;rg-image-nav-prev&quot;&gt;Previous Image&lt;/a&gt;				&lt;a href=&quot;#&quot; class=&quot;rg-image-nav-next&quot;&gt;Next Image&lt;/a&gt;			&lt;/div&gt;		{{/if}}		&lt;div class=&quot;rg-image&quot;&gt;&lt;/div&gt;		&lt;div class=&quot;rg-loading&quot;&gt;&lt;/div&gt;		&lt;div class=&quot;rg-caption-wrapper&quot;&gt;			&lt;div class=&quot;rg-caption&quot; style=&quot;display:none;&quot;&gt;				&lt;p&gt;&lt;/p&gt;			&lt;/div&gt;		&lt;/div&gt;	&lt;/div&gt;&lt;/script&gt;')" />

    <xsl:variable name="BeginColumnBildergalerieLight" select="string('&lt;link rel=&quot;stylesheet&quot; type=&quot;text/css&quot; href=&quot;/_layouts/15/styles/ZAP/gallery/style.css&quot; /&gt;		&lt;link rel=&quot;stylesheet&quot; type=&quot;text/css&quot; href=&quot;/_layouts/15/styles/ZAP/gallery/elastislide.css&quot; /&gt;  &lt;div id=&quot;rg-gallery&quot; class=&quot;rg-gallery ntm_GalleryLight&quot; &gt;	&lt;div class=&quot;rg-thumbs&quot;&gt;		&lt;div class=&quot;es-carousel-wrapper&quot; &gt;			&lt;div class=&quot;es-nav&quot;&gt;				&lt;span class=&quot;es-nav-prev&quot;&gt;Previous&lt;/span&gt;				&lt;span class=&quot;es-nav-next&quot;&gt;Next&lt;/span&gt;			&lt;/div&gt;			&lt;div class=&quot;es-carousel ntm_GalleryLight&quot;&gt;				&lt;ul&gt;')" />
    <xsl:variable name="EndColumnBildergalerieLight" select="string('&lt;/ul&gt;			&lt;/div&gt;		&lt;/div&gt;	&lt;/div&gt;&lt;/div&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;/_layouts/15/scripts/ZAP/gallery/jquery.tmpl.min.js&quot;&gt;&lt;/script&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;/_layouts/15/scripts/ZAP/gallery/jquery.easing.1.3.js&quot;&gt;&lt;/script&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;/_layouts/15/scripts/ZAP/gallery/jquery.elastislide-light.js&quot;&gt;&lt;/script&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;/_layouts/15/scripts/ZAP/gallery/gallery-light.js&quot;&gt;&lt;/script&gt;  &lt;script id=&quot;img-wrapper-tmpl&quot; type=&quot;text/x-jquery-tmpl&quot;&gt;	&lt;div class=&quot;rg-image-wrapper ntm_GalleryLight&quot; &gt;	{{if itemsCount &gt; 1}}			&lt;div class=&quot;rg-image-nav&quot;&gt;				&lt;a href=&quot;#&quot; class=&quot;rg-image-nav-prev&quot;&gt;Previous Image&lt;/a&gt;				&lt;a href=&quot;#&quot; class=&quot;rg-image-nav-next&quot;&gt;Next Image&lt;/a&gt;			&lt;/div&gt;		{{/if}}		&lt;div class=&quot;rg-image&quot;&gt;&lt;/div&gt;		&lt;div class=&quot;rg-loading&quot;&gt;&lt;/div&gt;		&lt;div class=&quot;rg-caption-wrapper&quot;&gt;			&lt;div class=&quot;rg-caption ntm_GalleryLight&quot; &gt;				&lt;p&gt;&lt;/p&gt;			&lt;/div&gt;		&lt;/div&gt;	&lt;/div&gt;&lt;/script&gt;')" />

    <xsl:variable name="BeginColumnTable" select="string('&lt;div class=&quot;ntm_Table&quot;&gt;')" />
    <xsl:variable name="BeginColumnBlank" select="string('&lt;div class=&quot;ntm_Blank&quot;&gt;')" />
    <xsl:variable name="BeginColumnZweispaltig" select="string('&lt;div class=&quot;ntm_ZweiSpaltig&quot;&gt;')" />
    <xsl:variable name="BeginColumnBilderfade" select="string('&lt;div class=&quot;ntm_DivCycler&quot;&gt;')" />
    <xsl:variable name="BeginColumnAkkordeon" select="string('&lt;div id=&quot;NTM_Gruppierung&quot; class=&quot;ntm_Akkordeon&quot;&gt;')" />
    <xsl:variable name="BeginColumnSlider" select="string('&lt;section class=&quot;slider ntm_SliderHold ntm_SliderStandard&quot;&gt;&lt;div class=&quot;flexslider&quot;&gt;&lt;ul class=&quot;slides&quot;&gt;')" />
    <xsl:variable name="BeginColumnSliderOne" select="string('&lt;section class=&quot;slider ntm_SliderHold ntm_SliderOne&quot;&gt;&lt;div class=&quot;flexslider&quot;&gt;&lt;ul class=&quot;slides&quot;&gt;')" />
    <xsl:variable name="BeginColumnSliderMore" select="string('&lt;section class=&quot;slider ntm_SliderHold ntm_SliderMore&quot;&gt;&lt;div class=&quot;flexslider&quot;&gt;&lt;ul class=&quot;slides&quot;&gt;')" />
    <xsl:variable name="BeginColumnSliderA" select="string('&lt;div class=&quot;ntm_SliderHold&quot;&gt;&lt;div class=&quot;flex-containerA&quot;&gt;&lt;div class=&quot;flexslider flexsliderA&quot;&gt;&lt;ul class=&quot;slides&quot; &gt;')" />
    <xsl:variable name="BeginColumnSliderIcons" select="string('&lt;section class=&quot;ntm_IconSliderHold&quot;&gt;&lt;div class=&quot;flexslider ntm_IconSlider&quot; &gt;&lt;ul class=&quot;slides&quot; &gt;')" />
    <xsl:variable name="EndColumnSlider" select="string('&lt;/ul&gt;&lt;/div&gt;&lt;/section&gt;')" />
    <xsl:variable name="EndColumnBlank" select="string('&lt;/div&gt;')" />
    <xsl:for-each select="$Rows">
      <xsl:variable name="CurPosition" select="position()" />
      <xsl:if test="($CurPosition &gt;= $FirstRow and $CurPosition &lt;= $LastRow)">
        <xsl:variable name="StartNewGroup" select="@__begingroup = 'True'" />
        <xsl:variable name="StartNewColumn" select="@__begincolumn = 'True'" />
        <xsl:choose>
          <xsl:when test="$cbq_isgrouping != 'True'">
            <xsl:if test="$CurPosition = $FirstRow">
              <xsl:choose>
                <xsl:when test="@Style='AbfrageZweispaltig' or @Style='AbfrageZweispaltig1' or @Style='AbfrageZweispaltig2'">
                  <xsl:value-of disable-output-escaping="yes" select="$BeginColumnZweispaltig" />
                </xsl:when>
                <xsl:when test="@Style='BigNews' or @Style='Quicklinks' or @Style='ThemenBoxKlein' or @Style='ThemenBox' or @Style='KachelnDocs' or @Style='LinkButtons' or @Style='HistorischerKalender' or @Style='Zitate'">
                  <xsl:value-of disable-output-escaping="yes" select="$BeginColumnBlank" />
                </xsl:when>
                <xsl:when test="@Style='Bilderfade'">
                  <xsl:value-of disable-output-escaping="yes" select="$BeginColumnBilderfade" />
                </xsl:when>
                <xsl:when test="@Style='Akkordeon'">
                  <xsl:value-of disable-output-escaping="yes" select="$BeginColumnAkkordeon" />
                </xsl:when>
                <xsl:when test="@Style='Slider'">
                  <xsl:value-of disable-output-escaping="yes" select="$BeginColumnSlider" />
                </xsl:when>
                <xsl:when test="@Style='NewsSliderOne'">
                  <xsl:value-of disable-output-escaping="yes" select="$BeginColumnSliderOne" />
                </xsl:when>
                <xsl:when test="@Style='IconSlider'">
                  <xsl:value-of disable-output-escaping="yes" select="$BeginColumnSliderIcons" />
                </xsl:when>
                <xsl:when test="@Style='Bilderslider' or @Style='NewsSliderMore'">
                  <xsl:value-of disable-output-escaping="yes" select="$BeginColumnSliderMore" />
                </xsl:when>
                <xsl:when test="@Style='SliderQuicklinks'">
                  <xsl:value-of disable-output-escaping="yes" select="$BeginColumnSliderA" />
                </xsl:when>
                <xsl:when test="@Style='Bildergalerie'">
                  <xsl:value-of disable-output-escaping="yes" select="$BeginColumnBildergalerie" />
                </xsl:when>
                <xsl:when test="@Style='BildergalerieLight'">
                  <xsl:value-of disable-output-escaping="yes" select="$BeginColumnBildergalerieLight" />
                </xsl:when>
                <xsl:when test="@Style='Tabelle'">
                  <xsl:value-of disable-output-escaping="yes" select="$BeginColumnTable" />
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of disable-output-escaping="yes" select="$BeginColumn" />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:if>
          </xsl:when>
          <xsl:when test="$StartNewGroup and $StartNewColumn">
            <xsl:choose>
              <xsl:when test="$CurPosition = $FirstRow">
                <xsl:choose>
                  <xsl:when test="@Style='AbfrageZweispaltig' or @Style='AbfrageZweispaltig1' or @Style='AbfrageZweispaltig2'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnZweispaltig" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateZweispaltig"/>
                  </xsl:when>
                  <xsl:when test="@Style='BigNews' or @Style='Quicklinks' or @Style='ThemenBoxKlein' or @Style='ThemenBox' or @Style='KachelnDocs' or @Style='LinkButtons' or @Style='HistorischerKalender' or @Style='Zitate'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnBlank" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateBlank"/>
                  </xsl:when>
                  <xsl:when test="@Style='Bilderfade'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnBilderfade" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateBilderfade"/>
                  </xsl:when>
                  <xsl:when test="@Style='Akkordeon'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnAkkordeon" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateAkkordeon"/>
                  </xsl:when>
                  <xsl:when test="@Style='Slider'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnSlider" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateSlider"/>
                  </xsl:when>
                  <xsl:when test="@Style='NewsSliderOne'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnSliderOne" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateSliderOne"/>
                  </xsl:when>
                  <xsl:when test="@Style='IconSlider'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnSliderIcons" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateSliderIcons"/>
                  </xsl:when>
                  <xsl:when test="@Style='Bilderslider' or @Style='NewsSliderMore'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnSliderMore" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateSliderMore"/>
                  </xsl:when>
                  <xsl:when test="@Style='SliderQuicklinks'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnSliderA" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateSliderA"/>
                  </xsl:when>
                  <xsl:when test="@Style='Bildergalerie'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnBildergalerie" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateBildergalerie"/>
                  </xsl:when>
                  <xsl:when test="@Style='BildergalerieLight'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnBildergalerieLight" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateBildergalerieLight"/>
                  </xsl:when>
                  <xsl:when test="@Style='Tabelle'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnTable" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateTable"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumn" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplate"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="@Style='AbfrageZweispaltig' or @Style='AbfrageZweispaltig1' or @Style='AbfrageZweispaltig2'">
                    <xsl:call-template name="OuterTemplate.CallFooterTemplateBlank"/>
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnBlank, $BeginColumnZweispaltig)" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateZweispaltig"/>
                  </xsl:when>
                  <xsl:when test="@Style='BigNews' or @Style='Quicklinks' or @Style='ThemenBoxKlein' or @Style='ThemenBox' or @Style='KachelnDocs' or @Style='LinkButtons' or @Style='HistorischerKalender' or @Style='Zitate'">
                    <xsl:call-template name="OuterTemplate.CallFooterTemplateBlank"/>
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnBlank, $BeginColumnBlank)" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateBlank"/>
                  </xsl:when>
                  <xsl:when test="@Style='Bilderfade'">
                    <xsl:call-template name="OuterTemplate.CallFooterTemplateBlank"/>
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnBlank, $BeginColumnBilderfade)" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateBilderfade"/>
                  </xsl:when>
                  <xsl:when test="@Style='Akkordeon'">
                    <xsl:call-template name="OuterTemplate.CallFooterTemplateBlank"/>
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnBlank, $BeginColumnAkkordeon)" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateAkkordeon"/>
                  </xsl:when>
                  <xsl:when test="@Style='Slider'">
                    <xsl:call-template name="OuterTemplate.CallFooterTemplateSlider"/>
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnSlider, $BeginColumnSlider)" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateSlider"/>
                  </xsl:when>
                  <xsl:when test="@Style='NewsSliderOne'">
                    <xsl:call-template name="OuterTemplate.CallFooterTemplateSlider"/>
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnSlider, $BeginColumnSliderOne)" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateSliderOne"/>
                  </xsl:when>
                  <xsl:when test="@Style='IconSlider'">
                    <xsl:call-template name="OuterTemplate.CallFooterTemplateSlider"/>
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnSlider, $BeginColumnSliderIcons)" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateSliderIcons"/>
                  </xsl:when>
                  <xsl:when test="@Style='Bilderslider' or @Style='NewsSliderMore'">
                    <xsl:call-template name="OuterTemplate.CallFooterTemplateSlider"/>
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnSlider, $BeginColumnSliderMore)" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateSliderMore"/>
                  </xsl:when>
                  <xsl:when test="@Style='SliderQuicklinks'">
                    <xsl:call-template name="OuterTemplate.CallFooterTemplateSlider"/>
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnSlider, $BeginColumnSliderA)" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateSliderA"/>
                  </xsl:when>
                  <xsl:when test="@Style='Bildergalerie'">
                    <xsl:call-template name="OuterTemplate.CallFooterTemplateBildergalerie"/>
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnBildergalerie, $BeginColumnBildergalerie)" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateBildergalerie"/>
                  </xsl:when>
                  <xsl:when test="@Style='BildergalerieLight'">
                    <xsl:call-template name="OuterTemplate.CallFooterTemplateBildergalerieLight"/>
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnBildergalerieLight, $BeginColumnBildergalerieLight)" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateBildergalerieLight"/>
                  </xsl:when>
                  <xsl:when test="@Style='Tabelle'">
                    <xsl:call-template name="OuterTemplate.CallFooterTemplateBlank"/>
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnBlank, $BeginColumnTable)" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplateTable"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:call-template name="OuterTemplate.CallFooterTemplate"/>
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumn, $BeginColumn)" />
                    <xsl:call-template name="OuterTemplate.CallHeaderTemplate"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:when test="$StartNewGroup">
            <xsl:choose>
              <xsl:when test="@Style='AbfrageZweispaltig' or @Style='AbfrageZweispaltig1' or @Style='AbfrageZweispaltig2'">
                <xsl:call-template name="OuterTemplate.CallFooterTemplateBlank"/>
                <xsl:call-template name="OuterTemplate.CallHeaderTemplateZweispaltig"/>
              </xsl:when>
              <xsl:when test="@Style='BigNews' or @Style='Quicklinks' or @Style='ThemenBoxKlein' or @Style='ThemenBox' or @Style='KachelnDocs' or @Style='LinkButtons' or @Style='HistorischerKalender' or @Style='Zitate'">
                <xsl:call-template name="OuterTemplate.CallFooterTemplateBlank"/>
                <xsl:call-template name="OuterTemplate.CallHeaderTemplateBlank"/>
              </xsl:when>
              <xsl:when test="@Style='Bilderfade'">
                <xsl:call-template name="OuterTemplate.CallFooterTemplateBlank"/>
                <xsl:call-template name="OuterTemplate.CallHeaderTemplateBilderfade"/>
              </xsl:when>
              <xsl:when test="@Style='Akkordeon'">
                <xsl:call-template name="OuterTemplate.CallFooterTemplateBlank"/>
                <xsl:call-template name="OuterTemplate.CallHeaderTemplateAkkordeon"/>
              </xsl:when>
              <xsl:when test="@Style='Slider'">
                <xsl:call-template name="OuterTemplate.CallFooterTemplateSlider"/>
                <xsl:call-template name="OuterTemplate.CallHeaderTemplateSlider"/>
              </xsl:when>
              <xsl:when test="@Style='NewsSliderOne'">
                <xsl:call-template name="OuterTemplate.CallFooterTemplateSlider"/>
                <xsl:call-template name="OuterTemplate.CallHeaderTemplateSliderOne"/>
              </xsl:when>
              <xsl:when test="@Style='IconSlider'">
                <xsl:call-template name="OuterTemplate.CallFooterTemplateSlider"/>
                <xsl:call-template name="OuterTemplate.CallHeaderTemplateSliderIcons"/>
              </xsl:when>
              <xsl:when test="@Style='Bilderslider' or @Style='NewsSliderMore'">
                <xsl:call-template name="OuterTemplate.CallFooterTemplateSlider"/>
                <xsl:call-template name="OuterTemplate.CallHeaderTemplateSliderMore"/>
              </xsl:when>
              <xsl:when test="@Style='SliderQuicklinks'">
                <xsl:call-template name="OuterTemplate.CallFooterTemplateSlider"/>
                <xsl:call-template name="OuterTemplate.CallHeaderTemplateSliderA"/>
              </xsl:when>
              <xsl:when test="@Style='Bildergalerie'">
                <xsl:call-template name="OuterTemplate.CallFooterTemplateBildergalerie"/>
                <xsl:call-template name="OuterTemplate.CallHeaderTemplateBildergalerie"/>
              </xsl:when>
              <xsl:when test="@Style='BildergalerieLight'">
                <xsl:call-template name="OuterTemplate.CallFooterTemplateBildergalerieLight"/>
                <xsl:call-template name="OuterTemplate.CallHeaderTemplateBildergalerieLight"/>
              </xsl:when>
              <xsl:when test="@Style='Tabelle'">
                <xsl:call-template name="OuterTemplate.CallFooterTemplateBlank"/>
                <xsl:call-template name="OuterTemplate.CallHeaderTemplateTable"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:call-template name="OuterTemplate.CallFooterTemplate"/>
                <xsl:call-template name="OuterTemplate.CallHeaderTemplate"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:when test="$StartNewColumn">
            <xsl:choose>
              <xsl:when test="$CurPosition = $FirstRow">
                <xsl:choose>
                  <xsl:when test="@Style='AbfrageZweispaltig' or @Style='AbfrageZweispaltig1' or @Style='AbfrageZweispaltig2'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnZweispaltig" />
                  </xsl:when>
                  <xsl:when test="@Style='BigNews' or @Style='Quicklinks' or @Style='ThemenBoxKlein' or @Style='ThemenBox' or @Style='KachelnDocs' or @Style='LinkButtons' or @Style='HistorischerKalender' or @Style='Zitate'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnBlank" />
                  </xsl:when>
                  <xsl:when test="@Style='Bilderfade'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnBilderfade" />
                  </xsl:when>
                  <xsl:when test="@Style='Akkordeon'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnAkkordeon" />
                  </xsl:when>
                  <xsl:when test="@Style='Slider'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnSlider" />
                  </xsl:when>
                  <xsl:when test="@Style='NewsSliderOne'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnSliderOne" />
                  </xsl:when>
                  <xsl:when test="@Style='IconSlider'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnSliderIcons" />
                  </xsl:when>
                  <xsl:when test="@Style='Bilderslider' or @Style='NewsSliderMore'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnSliderMore" />
                  </xsl:when>
                  <xsl:when test="@Style='SliderQuicklinks'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnSliderA" />
                  </xsl:when>
                  <xsl:when test="@Style='Bildergalerie'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnBildergalerie" />
                  </xsl:when>
                  <xsl:when test="@Style='BildergalerieLight'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnBildergalerieLight" />
                  </xsl:when>
                  <xsl:when test="@Style='Tabelle'">
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumnTable" />
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of disable-output-escaping="yes" select="$BeginColumn" />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="@Style='AbfrageZweispaltig' or @Style='AbfrageZweispaltig1' or @Style='AbfrageZweispaltig2'">
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnBlank, $BeginColumnZweispaltig)" />
                  </xsl:when>
                  <xsl:when test="@Style='BigNews' or @Style='Quicklinks' or @Style='ThemenBoxKlein' or @Style='ThemenBox' or @Style='KachelnDocs' or @Style='LinkButtons' or @Style='HistorischerKalender' or @Style='Zitate'">
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnBlank, $BeginColumnBlank)" />
                  </xsl:when>
                  <xsl:when test="@Style='Bilderfade'">
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnBlank, $BeginColumnBilderfade)" />
                  </xsl:when>
                  <xsl:when test="@Style='Akkordeon'">
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnBlank, $BeginColumnAkkordeon)" />
                  </xsl:when>
                  <xsl:when test="@Style='Slider'">
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnSlider, $BeginColumnSlider)" />
                  </xsl:when>
                  <xsl:when test="@Style='NewsSliderOne'">
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnSlider, $BeginColumnSliderOne)" />
                  </xsl:when>
                  <xsl:when test="@Style='IconSlider'">
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnSlider, $BeginColumnSliderIcons)" />
                  </xsl:when>
                  <xsl:when test="@Style='Bilderslider' or @Style='NewsSliderMore'">
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnSlider, $BeginColumnSliderMore)" />
                  </xsl:when>
                  <xsl:when test="@Style='SliderQuicklinks'">
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnSlider, $BeginColumnSliderA)" />
                  </xsl:when>
                  <xsl:when test="@Style='Bildergalerie'">
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnBildergalerie, $BeginColumnBildergalerie)" />
                  </xsl:when>
                  <xsl:when test="@Style='BildergalerieLight'">
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnBildergalerieLight, $BeginColumnBildergalerieLight)" />
                  </xsl:when>
                  <xsl:when test="@Style='Tabelle'">
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumnBlank, $BeginColumnTable)" />
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of disable-output-escaping="yes" select="concat($EndColumn, $BeginColumn)" />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:call-template name="OuterTemplate.CallItemTemplate">
          <xsl:with-param name="CurPosition" select="$CurPosition" />
        </xsl:call-template>
        <xsl:if test="$CurPosition = $LastRow">
          <xsl:if test="$cbq_isgrouping = 'True'">
            <xsl:choose>
              <xsl:when test="@Style='AbfrageZweispaltig' or @Style='AbfrageZweispaltig1' or @Style='AbfrageZweispaltig2' or @Style='LinkButtons' or @Style='HistorischerKalender' or @Style='Zitate' or @Style='BigNews' or @Style='Bilderfade' or @Style='Akkordeon' or @Style='Quicklinks' or @Style='ThemenBoxKlein' or @Style='ThemenBox' or @Style='KachelnDocs' or @Style='Tabelle'">
                <xsl:call-template name="OuterTemplate.CallFooterTemplateBlank"/>
              </xsl:when>
              <xsl:when test="@Style='Slider' or @Style='NewsSliderOne' or @Style='Bilderslider' or @Style='NewsSliderMore' or @Style='SliderQuicklinks' or @Style='IconSlider'">
                <xsl:call-template name="OuterTemplate.CallFooterTemplateSlider"/>
              </xsl:when>
              <xsl:when test="@Style='Bildergalerie'">
                <xsl:call-template name="OuterTemplate.CallFooterTemplateBildergalerie"/>
              </xsl:when>
              <xsl:when test="@Style='BildergalerieLight'">
                <xsl:call-template name="OuterTemplate.CallFooterTemplateBildergalerieLight"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:call-template name="OuterTemplate.CallFooterTemplate"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
          <xsl:choose>
            <xsl:when test="@Style='AbfrageZweispaltig' or @Style='AbfrageZweispaltig1' or @Style='AbfrageZweispaltig2' or @Style='LinkButtons' or @Style='HistorischerKalender' or @Style='Zitate' or @Style='BigNews' or @Style='Bilderfade' or @Style='Akkordeon' or @Style='Quicklinks' or @Style='ThemenBoxKlein' or @Style='ThemenBox' or @Style='KachelnDocs' or @Style='Tabelle'">
              <xsl:value-of disable-output-escaping="yes" select="$EndColumnBlank" />
            </xsl:when>
            <xsl:when test="@Style='Slider' or @Style='NewsSliderOne' or @Style='Bilderslider' or @Style='NewsSliderMore' or @Style='SliderQuicklinks' or @Style='IconSlider'">
              <xsl:value-of disable-output-escaping="yes" select="$EndColumnSlider" />
            </xsl:when>
            <xsl:when test="@Style='Bildergalerie'">
              <xsl:value-of disable-output-escaping="yes" select="$EndColumnBildergalerie" />
            </xsl:when>
            <xsl:when test="@Style='BildergalerieLight'">
              <xsl:value-of disable-output-escaping="yes" select="$EndColumnBildergalerieLight" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of disable-output-escaping="yes" select="$EndColumn" />
            </xsl:otherwise>
          </xsl:choose>
        </xsl:if>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>


  <xsl:template name="OuterTemplate.CallHeaderTemplate">
    <xsl:value-of disable-output-escaping="yes" select="$BeginListItem" />
    <xsl:apply-templates select="." mode="header">
    </xsl:apply-templates>
    <xsl:value-of disable-output-escaping="yes" select="$BeginList" />
  </xsl:template>

  <xsl:template name="OuterTemplate.CallHeaderTemplateZweispaltig">
    <xsl:value-of disable-output-escaping="yes" select="$BeginListItemBlank" />
    <xsl:apply-templates select="." mode="header">
    </xsl:apply-templates>
    <xsl:value-of disable-output-escaping="yes" select="$BeginListZweispaltig" />
  </xsl:template>

  <xsl:template name="OuterTemplate.CallHeaderTemplateBlank">
    <xsl:value-of disable-output-escaping="yes" select="$BeginListItemBlank" />
    <xsl:apply-templates select="." mode="header">
    </xsl:apply-templates>
    <xsl:value-of disable-output-escaping="yes" select="$BeginListBlank" />
  </xsl:template>

  <xsl:template name="OuterTemplate.CallHeaderTemplateTable">
    <xsl:value-of disable-output-escaping="yes" select="$BeginListItemBlank" />
    <xsl:apply-templates select="." mode="header">
    </xsl:apply-templates>
    <xsl:value-of disable-output-escaping="yes" select="$BeginListTable" />
  </xsl:template>

  <xsl:template name="OuterTemplate.CallHeaderTemplateSliderIcons">
    <xsl:value-of disable-output-escaping="yes" select="$BeginListItemSlider" />
    <xsl:apply-templates select="." mode="header">
    </xsl:apply-templates>
    <xsl:value-of disable-output-escaping="yes" select="$BeginListSliderIcons" />
  </xsl:template>

  <xsl:template name="OuterTemplate.CallHeaderTemplateBilderfade">
    <xsl:value-of disable-output-escaping="yes" select="$BeginListItemBlank" />
    <xsl:apply-templates select="." mode="header">
    </xsl:apply-templates>
    <xsl:value-of disable-output-escaping="yes" select="$BeginListBilderfade" />
  </xsl:template>

  <xsl:template name="OuterTemplate.CallHeaderTemplateAkkordeon">
    <xsl:value-of disable-output-escaping="yes" select="$BeginListItemBlank" />
    <xsl:apply-templates select="." mode="header">
    </xsl:apply-templates>
    <xsl:value-of disable-output-escaping="yes" select="$BeginListAkkordeon" />
  </xsl:template>

  <xsl:template name="OuterTemplate.CallHeaderTemplateSlider">
    <xsl:value-of disable-output-escaping="yes" select="$BeginListItemSlider" />
    <xsl:apply-templates select="." mode="header">
    </xsl:apply-templates>
    <xsl:value-of disable-output-escaping="yes" select="$BeginListSlider" />
  </xsl:template>
  <xsl:template name="OuterTemplate.CallHeaderTemplateSliderOne">
    <xsl:value-of disable-output-escaping="yes" select="$BeginListItemSlider" />
    <xsl:apply-templates select="." mode="header">
    </xsl:apply-templates>
    <xsl:value-of disable-output-escaping="yes" select="$BeginListSliderOne" />
  </xsl:template>
  <xsl:template name="OuterTemplate.CallHeaderTemplateSliderMore">
    <xsl:value-of disable-output-escaping="yes" select="$BeginListItemSlider" />
    <xsl:apply-templates select="." mode="header">
    </xsl:apply-templates>
    <xsl:value-of disable-output-escaping="yes" select="$BeginListSliderMore" />
  </xsl:template>

  <xsl:template name="OuterTemplate.CallHeaderTemplateSliderA">
    <xsl:value-of disable-output-escaping="yes" select="$BeginListItemSlider" />
    <xsl:apply-templates select="." mode="header">
    </xsl:apply-templates>
    <xsl:value-of disable-output-escaping="yes" select="$BeginListSliderA" />
  </xsl:template>

  <xsl:template name="OuterTemplate.CallHeaderTemplateBildergalerie">
    <xsl:value-of disable-output-escaping="yes" select="$BeginListItemBildergalerie" />
    <xsl:apply-templates select="." mode="header">
    </xsl:apply-templates>
    <xsl:value-of disable-output-escaping="yes" select="$BeginListBildergalerie" />
  </xsl:template>

  <xsl:template name="OuterTemplate.CallHeaderTemplateBildergalerieLight">
    <xsl:value-of disable-output-escaping="yes" select="$BeginListItemBildergalerieLight" />
    <xsl:apply-templates select="." mode="header">
    </xsl:apply-templates>
    <xsl:value-of disable-output-escaping="yes" select="$BeginListBildergalerieLight" />
  </xsl:template>

  <xsl:template name="OuterTemplate.CallItemTemplate">
    <xsl:param name="CurPosition" />

    <xsl:choose>
      <xsl:when test="@Style='AbfrageZweispaltig' or @Style='AbfrageZweispaltig1' or @Style='AbfrageZweispaltig2' or @Style='LinkButtons' or @Style='HistorischerKalender' or @Style='Zitate' or @Style='BigNews' or @Style='Bilderfade' or @Style='Akkordeon' or @Style='Quicklinks' or @Style='ThemenBoxKlein' or @Style='ThemenBox' or @Style='KachelnDocs' or @Style='Tabelle'">
        <xsl:apply-templates select="." mode="itemstyle">
          <xsl:with-param name="CurPos" select="$CurPosition" />
        </xsl:apply-templates>
      </xsl:when>
      <xsl:when test="@Style='Slider' or @Style='NewsSliderOne' or @Style='Bilderslider' or @Style='NewsSliderMore' or @Style='SliderQuicklinks' or @Style='IconSlider'">
        <xsl:value-of disable-output-escaping="yes" select="$BeginListItemSlider" />
        <xsl:apply-templates select="." mode="itemstyle">
          <xsl:with-param name="CurPos" select="$CurPosition" />
        </xsl:apply-templates>
        <xsl:value-of disable-output-escaping="yes" select="$EndListItem" />
      </xsl:when>
      <xsl:when test="@Style='NewsRollUpItem'">
        <xsl:value-of disable-output-escaping="yes" select="$BeginListItem" />
        <xsl:apply-templates select="." mode="itemstyle">
          <xsl:with-param name="EditMode" select="$cbq_iseditmode" />
          <xsl:with-param name="CurPos" select="$CurPosition" />
        </xsl:apply-templates>
        <xsl:value-of disable-output-escaping="yes" select="$EndListItem" />
      </xsl:when>
      <xsl:when test="@Style='NewsBigItem'">
        <xsl:value-of disable-output-escaping="yes" select="$BeginListItem" />
        <xsl:apply-templates select="." mode="itemstyle">
          <xsl:with-param name="CurPos" select="$CurPosition" />
        </xsl:apply-templates>
        <xsl:value-of disable-output-escaping="yes" select="$EndListItem" />
      </xsl:when>
      <xsl:when test="@Style='NewsCategoryItem'">
        <xsl:value-of disable-output-escaping="yes" select="$BeginListItem" />
        <xsl:apply-templates select="." mode="itemstyle">
          <xsl:with-param name="CurPos" select="$CurPosition" />
        </xsl:apply-templates>
        <xsl:value-of disable-output-escaping="yes" select="$EndListItem" />
      </xsl:when>
      <xsl:when test="@Style='Bildergalerie'">
        <xsl:value-of disable-output-escaping="yes" select="$BeginListItemBildergalerie" />
        <xsl:apply-templates select="." mode="itemstyle">
          <xsl:with-param name="CurPos" select="$CurPosition" />
        </xsl:apply-templates>
        <xsl:value-of disable-output-escaping="yes" select="$EndListItemBildergalerie" />
      </xsl:when>
      <xsl:when test="@Style='BildergalerieLight'">
        <xsl:value-of disable-output-escaping="yes" select="$BeginListItemBildergalerieLight" />
        <xsl:apply-templates select="." mode="itemstyle">
          <xsl:with-param name="CurPos" select="$CurPosition" />
        </xsl:apply-templates>
        <xsl:value-of disable-output-escaping="yes" select="$EndListItemBildergalerieLight" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of disable-output-escaping="yes" select="$BeginListItem" />
        <xsl:apply-templates select="." mode="itemstyle">
          <xsl:with-param name="CurPos" select="$CurPosition" />
        </xsl:apply-templates>
        <xsl:value-of disable-output-escaping="yes" select="$EndListItem" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="OuterTemplate.CallFooterTemplate">
    <xsl:value-of disable-output-escaping="yes" select="$EndList" />
    <xsl:value-of disable-output-escaping="yes" select="$EndListItem" />
  </xsl:template>
  <xsl:template name="OuterTemplate.CallFooterTemplateSlider">
    <xsl:value-of disable-output-escaping="yes" select="$EndListSlider" />
    <xsl:value-of disable-output-escaping="yes" select="$EndListItem" />
  </xsl:template>
  <xsl:template name="OuterTemplate.CallFooterTemplateBildergalerie">
    <xsl:value-of disable-output-escaping="yes" select="$EndListBildergalerie" />
    <xsl:value-of disable-output-escaping="yes" select="$EndListItemBildergalerie" />
  </xsl:template>
  <xsl:template name="OuterTemplate.CallFooterTemplateBildergalerieLight">
    <xsl:value-of disable-output-escaping="yes" select="$EndListBildergalerieLight" />
    <xsl:value-of disable-output-escaping="yes" select="$EndListItemBildergalerieLight" />
  </xsl:template>
  <xsl:template name="OuterTemplate.CallFooterTemplateBlank">
    <xsl:value-of disable-output-escaping="yes" select="$EndListBlank" />
    <xsl:value-of disable-output-escaping="yes" select="$EndListItemBlank" />
  </xsl:template>
  <xsl:template name="OuterTemplate.GetSafeLink">
    <xsl:param name="UrlColumnName"/>
    <xsl:if test="$UseCopyUtil = 'True'">
      <xsl:value-of select="concat($RootSiteRef, '/_layouts/15/CopyUtil.aspx?Use=id&amp;Action=dispform&amp;ItemId=',@ID,'&amp;ListId=',@ListId,'&amp;WebId=',@WebId,'&amp;SiteId=',$SiteId,'&amp;Source=',$Source)"/>
    </xsl:if>
    <xsl:if test="$UseCopyUtil != 'True'">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="$UrlColumnName"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  <xsl:template name="OuterTemplate.GetTitle">
    <xsl:param name="Title"/>
    <xsl:param name="UrlColumnName"/>
    <xsl:param name="UseFileName" select="0"/>
    <xsl:choose>
      <xsl:when test="string-length($Title) != 0 and $UseFileName = 0">
        <xsl:value-of select="$Title" />
      </xsl:when>
      <xsl:when test="$UseCopyUtil = 'True' and $UseFileName = 0">
        <xsl:value-of select="$BlankTitle" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="FileNameWithExtension">
          <xsl:call-template name="OuterTemplate.GetPageNameFromUrl">
            <xsl:with-param name="UrlColumnName" select="$UrlColumnName" />
          </xsl:call-template>
        </xsl:variable>
        <xsl:choose>
          <xsl:when test="$UseFileName = 1">
            <xsl:call-template name="OuterTemplate.GetFileNameWithoutExtension">
              <xsl:with-param name="input" select="$FileNameWithExtension" />
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$FileNameWithExtension" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="OuterTemplate.FormatColumnIntoUrl">
    <xsl:param name="UrlColumnName"/>
    <xsl:variable name="Value" select="@*[name()=$UrlColumnName]"/>
    <xsl:if test="contains($DataColumnTypes,concat(';',$UrlColumnName,',URL;'))">
      <xsl:call-template name="OuterTemplate.FormatValueIntoUrl">
        <xsl:with-param name="Value" select="$Value"/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test="not(contains($DataColumnTypes,concat(';',$UrlColumnName,',URL;')))">
      <xsl:value-of select="$Value"/>
    </xsl:if>
  </xsl:template>
  <xsl:template name="OuterTemplate.FormatValueIntoUrl">
    <xsl:param name="Value"/>
    <xsl:if test="not(contains($Value,', '))">
      <xsl:value-of select="$Value"/>
    </xsl:if>
    <xsl:if test="contains($Value,', ')">
      <xsl:call-template name="OuterTemplate.Replace">
        <xsl:with-param name="Value" select="substring-before($Value,', ')"/>
        <xsl:with-param name="Search" select="',,'"/>
        <xsl:with-param name="Replace" select="','"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  <xsl:template name="OuterTemplate.Replace">
    <xsl:param name="Value"/>
    <xsl:param name="Search"/>
    <xsl:param name="Replace"/>
    <xsl:if test="contains($Value,$Search)">
      <xsl:value-of select="concat(substring-before($Value,$Search),$Replace)"/>
      <xsl:call-template name="OuterTemplate.Replace">
        <xsl:with-param name="Value" select="substring-after($Value,$Search)"/>
        <xsl:with-param name="Search" select="$Search"/>
        <xsl:with-param name="Replace" select="$Replace"/>
      </xsl:call-template>
    </xsl:if>
    <xsl:if test="not(contains($Value,$Search))">
      <xsl:value-of select="$Value"/>
    </xsl:if>
  </xsl:template>
  <xsl:template name="OuterTemplate.GetSafeStaticUrl">
    <xsl:param name="UrlColumnName"/>
    <xsl:variable name="Url">
      <xsl:call-template name="OuterTemplate.FormatColumnIntoUrl">
        <xsl:with-param name="UrlColumnName" select="$UrlColumnName"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="cmswrt:EnsureIsAllowedProtocol($Url)"/>
  </xsl:template>
  <xsl:template name="OuterTemplate.GetColumnDataForUnescapedOutput">
    <xsl:param name="Name"/>
    <xsl:param name="MustBeOfType"/>
    <xsl:if test="contains($DataColumnTypes,concat(';',$Name,',',$MustBeOfType,';'))">
      <xsl:value-of select="@*[name()=$Name]"/>
    </xsl:if>
  </xsl:template>
  <xsl:template name="OuterTemplate.GetPageNameFromUrl">
    <xsl:param name="UrlColumnName"/>
    <xsl:variable name="Url">
      <xsl:call-template name="OuterTemplate.FormatColumnIntoUrl">
        <xsl:with-param name="UrlColumnName" select="$UrlColumnName"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name="OuterTemplate.GetPageNameFromUrlRecursive">
      <xsl:with-param name="Url" select="$Url"/>
    </xsl:call-template>
  </xsl:template>
  <xsl:template name="OuterTemplate.GetPageNameFromUrlRecursive">
    <xsl:param name="Url"/>
    <xsl:choose>
      <xsl:when test="contains($Url,'/') and substring($Url,string-length($Url)) != '/'">
        <xsl:call-template name="OuterTemplate.GetPageNameFromUrlRecursive">
          <xsl:with-param name="Url" select="substring-after($Url,'/')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$Url"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="OuterTemplate.GetGroupName">
    <xsl:param name="GroupName"/>
    <xsl:param name="GroupType"/>
    <xsl:choose>
      <xsl:when test="string-length(normalize-space($GroupName)) = 0">
        <xsl:value-of select="$BlankGroup"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$GroupType='URL'">
            <xsl:variable name="Url">
              <xsl:call-template name="OuterTemplate.FormatValueIntoUrl">
                <xsl:with-param name="Value" select="$GroupName"/>
              </xsl:call-template>
            </xsl:variable>
            <xsl:call-template name="OuterTemplate.GetPageNameFromUrlRecursive">
              <xsl:with-param name="Url" select="$Url"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$GroupName" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="OuterTemplate.CallPresenceStatusIconTemplate">
    <xsl:if test="string-length(@SipAddress) != 0">
      <span class="ms-imnSpan">
        <a href="#" onclick="IMNImageOnClick(event);return false;" class="ms-imnlink">
          <span class="ms-spimn-presenceWrapper ms-imnImg ms-spimn-imgSize-10x10">
            <img src="/_layouts/15/images/spimn.png?rev=23" class="ms-spimn-img ms-spimn-presence-disconnected-10x10x32" onload="IMNRC('{@SipAddress}')" ShowOfflinePawn="1" alt="" id="{concat('MWP_pawn_',$ClientId,'_',@ID,'type=sip')}"/>
          </span>
        </a>
      </span>
    </xsl:if>
  </xsl:template>
  <xsl:template name="OuterTemplate.GetFileNameWithoutExtension">
    <xsl:param name="input"/>
    <xsl:variable name="extension">
      <xsl:value-of select="substring-after($input, '.')"/>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="contains($extension, '.')">
        <xsl:variable name="afterextension">
          <xsl:call-template name="OuterTemplate.GetFileNameWithoutExtension">
            <xsl:with-param name="input" select="$extension"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="concat(substring-before($input, '.'), $afterextension)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="contains($input, '.')">
            <xsl:value-of select="substring-before($input, '.')"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$input"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
