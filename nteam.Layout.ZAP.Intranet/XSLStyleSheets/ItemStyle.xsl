<xsl:stylesheet
	version="1.0"
	exclude-result-prefixes="x d xsl msxsl cmswrt"
	xmlns:x="http://www.w3.org/2001/XMLSchema"
	xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
	xmlns:cmswrt="http://schemas.microsoft.com/WebParts/v3/Publishing/runtime"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxsl="urn:schemas-microsoft-com:xslt"
	xmlns:fn="http://www.w3.org/2004/07/xpath-functions"
	xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime">
  <xsl:param name="ItemsHaveStreams">
    <xsl:value-of select="'False'" />
  </xsl:param>
  <xsl:variable name="OnClickTargetAttribute" select="string('javascript:this.target=&quot;_blank&quot;')" />
  <xsl:variable name="ImageWidth" />
  <xsl:variable name="ImageHeight" />
  <xsl:template name="Default" match="*" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <div class="item">
      <xsl:if test="string-length($SafeImageUrl) != 0">
        <div class="image-area-left">
          <a href="{$SafeLinkUrl}">
            <xsl:if test="$ItemsHaveStreams = 'True'">
              <xsl:attribute name="onclick">
                <xsl:value-of select="@OnClickForWebRendering"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
              <xsl:attribute name="onclick">
                <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
              </xsl:attribute>
            </xsl:if>
            <img class="image" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
              <xsl:if test="$ImageWidth != ''">
                <xsl:attribute name="width">
                  <xsl:value-of select="$ImageWidth" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="$ImageHeight != ''">
                <xsl:attribute name="height">
                  <xsl:value-of select="$ImageHeight" />
                </xsl:attribute>
              </xsl:if>
            </img>
          </a>
        </div>
      </xsl:if>
      <div class="link-item">
        <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
        <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
          <xsl:if test="$ItemsHaveStreams = 'True'">
            <xsl:attribute name="onclick">
              <xsl:value-of select="@OnClickForWebRendering"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
            <xsl:attribute name="onclick">
              <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:value-of select="$DisplayTitle"/>
        </a>
        <div class="description">
          <xsl:value-of select="@Description" />
        </div>
      </div>
    </div>
  </xsl:template>

  <xsl:template name="Akkordeon" match="Row[@Style='Akkordeon']" mode="itemstyle">
    <xsl:param name="CurPos" />
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <h3 class="ntm_AkkordeonTitle" pos="{$CurPos}">
      <div class="ntm_OpenIco">
        <img src="/_layouts/images/blank.gif" />
      </div>
      <xsl:value-of select="$DisplayTitle"/>
    </h3>
    <div class="ntm_AkkordeonText">
      <p>
        <xsl:value-of select="@Description" disable-output-escaping="yes" />
      </p>
    </div>
  </xsl:template>
  <xsl:template name="LinkListe" match="Row[@Style='LinkListe']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:value-of select="substring-after(@URL,', ')"/>
    </xsl:variable>
    <xsl:variable name="LinkHref">
      <xsl:value-of select="substring-before(@URL,', ')"/>
    </xsl:variable>
    <xsl:variable name="LinkTarget">
      <xsl:choose>
        <xsl:when test="@_OpenInNewWindow = 'True'">_blank</xsl:when>
        <xsl:otherwise>_self</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <div class="ntm_LinkItem">
      <div class="ntm_Title">
        <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
        <a href="{$LinkHref}" target="{$LinkTarget}" title="{@Comments}">
          <xsl:value-of select="$DisplayTitle"/>
        </a>
      </div>
      <div class="ntm_Description">
        <xsl:value-of select="@Comments"/>
      </div>
    </div>
    <xsl:if test="count(following-sibling::*)=0">
      <span style="display:none;">
        <xsl:value-of select="@_OpenInNewWindow"/>
      </span>
    </xsl:if>
  </xsl:template>


  <xsl:template name="LinkButtons" match="Row[@Style='LinkButtons']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:value-of select="substring-after(@URL,', ')"/>
    </xsl:variable>
    <xsl:variable name="LinkHref">
      <xsl:value-of select="substring-before(@URL,', ')"/>
    </xsl:variable>
    <xsl:variable name="LinkTarget">
      <xsl:choose>
        <xsl:when test="@_OpenInNewWindow = 'True'">_blank</xsl:when>
        <xsl:otherwise>_self</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <div class="ntm_LinkButton">
      <a href="{$LinkHref}" target="{$LinkTarget}" title="{@_Tooltip}">
        <xsl:value-of select="$DisplayTitle"/>
      </a>
      <xsl:text >&#160;</xsl:text>
    </div>

    <xsl:if test="count(following-sibling::*)=0">
      <span style="display:none;">
        <xsl:value-of select="@_OpenInNewWindow"/>
        <xsl:value-of select="@_Tooltip"/>
      </span>
    </xsl:if>
  </xsl:template>
  <xsl:template name="News" match="Row[@Style='News']" mode="itemstyle">
    <xsl:param name="CurPos" />
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="clearLabel">
      <xsl:call-template name="OuterTemplate.Replace">
        <xsl:with-param name="Value" select="@_Label"/>
        <xsl:with-param name="Search" select="';#'"/>
        <xsl:with-param name="Replace" select="' '"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="count(preceding-sibling::*)=0">
      <div class="ntm_NewsHeader">
        <span>News</span>
      </div>
    </xsl:if>
    <div class="ntm_News">
      <xsl:choose>
        <xsl:when test="@_TopNews = 'True'">
          <xsl:attribute name="class">
            <xsl:text>ntm_News ntm_Topnews</xsl:text>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:choose>
        <xsl:when test="string-length($SafeImageUrl) != 0">
          <div class="ntm_newsImg">
            <a href="{$SafeLinkUrl}" title="{@_LinkToolTip}">
              <img src="{$SafeImageUrl}?RenditionID=5" title="{@ImageUrlAltText}" />
            </a>
          </div>
        </xsl:when>
        <xsl:otherwise>
          <div class="ntm_newsImgPH">
            <a href="{$SafeLinkUrl}" title="{@_LinkToolTip}">
              <img src="/_layouts/15/images/blank.gif" alt="Platzhalter Bild" />
            </a>
          </div>
        </xsl:otherwise>
      </xsl:choose>

      <div class="ntm_NewsDate">
        <xsl:if test="string-length(@_ArticleStartDate) != 0" >
          <xsl:value-of select="ddwrt:FormatDateTime(string(@_ArticleStartDate) ,1033 ,'dd.MM.yyyy')" />
        </xsl:if>
        <xsl:if test="string-length(@_ArticleStartDate) != 0 and string-length(@_Label) != 0">
          <span class="ntm_Trenner">
            <xsl:text >, </xsl:text>
          </span>
        </xsl:if>
        <xsl:if test="string-length(@_Label) != 0">
          <xsl:value-of select="$clearLabel" />
        </xsl:if>
        &#160;
      </div>

      <div class="ntm_NewsTitle">
        <a href="{$SafeLinkUrl}" title="{@_LinkToolTip}">
          <xsl:value-of select="$DisplayTitle" disable-output-escaping="yes"/>
        </a>
      </div>

      <div class="ntm_NewsText">
        <xsl:value-of select="@Einleitungstext" disable-output-escaping="yes" />
      </div>
      <div class="ntm_NewsWeiter">
        <a href="{$SafeLinkUrl}" title="{@_LinkToolTip}" class="ntm_MehrLink">Artikel lesen</a>
      </div>

    </div>
    <xsl:if test="count(following-sibling::*)=0">
      <div class="ntm_NewsFooter">
        <img src="/_layouts/15/images/blank.gif" alt="Blank" />
        <span style="display:none;">
          <xsl:value-of select="@_TopNews"/>
          <xsl:value-of select="@_LinkToolTip"/>
        </span>
      </div>
    </xsl:if>
  </xsl:template>
  <xsl:template name="NewsStartseite" match="Row[@Style='NewsStartseite']" mode="itemstyle">
    <xsl:param name="CurPos" />
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="clearLabel">
      <xsl:call-template name="OuterTemplate.Replace">
        <xsl:with-param name="Value" select="@_Label"/>
        <xsl:with-param name="Search" select="';#'"/>
        <xsl:with-param name="Replace" select="' '"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="count(preceding-sibling::*)=0">
    </xsl:if>
    <div class="ntm_News ntm_NewsStartseite">
      <xsl:choose>
        <xsl:when test="@_TopNews = 'True'">
          <xsl:attribute name="class">
            <xsl:text>ntm_News ntm_NewsStartseite ntm_Topnews</xsl:text>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:choose>
        <xsl:when test="string-length($SafeImageUrl) != 0">
          <div class="ntm_newsImg">
            <a href="{$SafeLinkUrl}" title="{@_LinkToolTip}">
              <img src="{$SafeImageUrl}?RenditionID=6" title="{@ImageUrlAltText}" />
            </a>
            <xsl:if test="string-length(@_Label) != 0">
              <div class="ntm_FlexLabel">
                <xsl:value-of select="$clearLabel" disable-output-escaping="yes" />
              </div>
            </xsl:if>
          </div>
        </xsl:when>
        <xsl:otherwise>
          <div class="ntm_newsImgPH">
            <a href="{$SafeLinkUrl}" title="{@_LinkToolTip}">
              <img src="/_layouts/15/images/blank.gif" alt="Platzhalter Bild" />
            </a>
            <xsl:if test="string-length(@_Label) != 0">
              <div class="ntm_FlexLabel">
                <xsl:value-of select="$clearLabel" disable-output-escaping="yes" />
              </div>
            </xsl:if>
          </div>
        </xsl:otherwise>
      </xsl:choose>
      <div class="ntm_NewsTitle">
        <a href="{$SafeLinkUrl}" title="{@_LinkToolTip}">
          <xsl:value-of select="$DisplayTitle"/>
        </a>
      </div>
      <div class="ntm_NewsText">
        <xsl:value-of select="@Einleitungstext" disable-output-escaping="yes" />
      </div>
      <div class="ntm_NewsWeiter">
        <a href="{$SafeLinkUrl}" title="{@_LinkToolTip}" class="ntm_MehrLink">Artikel lesen</a>
      </div>
    </div>
    <xsl:if test="count(following-sibling::*)=0">
      <div class="ntm_NewsFooter">
        <img src="/_layouts/15/images/blank.gif" alt="Blank" />
        <span style="display:none;">
          <xsl:value-of select="@_TopNews"/>
          <xsl:value-of select="@_LinkToolTip"/>
        </span>
      </div>
    </xsl:if>


  </xsl:template>
  <xsl:template name="Meldungen" match="Row[@Style='Meldungen']" mode="itemstyle">
    <xsl:param name="CurPos" />
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'URL'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="LinkTarget">
      <xsl:choose>
        <xsl:when test="@OpenInNewWindow = 'True'">_blank</xsl:when>
        <xsl:otherwise>_self</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:if test="count(preceding-sibling::*)=0">
      <div class="ntm_NewsHeader">
        <span>Neues</span>
      </div>
    </xsl:if>
    <div class="ntm_Neuigkeiten {@Kategorie}">
      <div class="ntm_NewsDate">
        <xsl:if test="string-length(@AnzeigeVon) != 0" >
          <xsl:value-of select="ddwrt:FormatDateTime(string(@AnzeigeVon) ,1033 ,'dd.MM.yyyy')" />
        </xsl:if>
        <xsl:if test="string-length(@Kategorie) != 0" >
          &#160;|&#160;<xsl:value-of select="@Kategorie"/>
        </xsl:if>
      </div>
      <xsl:choose>
        <xsl:when test="string-length(@URL) != 0">
          <div class="ntm_TeaserTitle">
            <a href="{$SafeLinkUrl}" title="{@LinkToolTip}" target="{$LinkTarget}" >
              <xsl:value-of select="$DisplayTitle" disable-output-escaping="yes"/>
            </a>
          </div>
        </xsl:when>
        <xsl:otherwise>
          <div class="ntm_TeaserTitle">
            <xsl:value-of select="$DisplayTitle" disable-output-escaping="yes"/>
          </div>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:if test="string-length(@Newstext) != 0">
        <div class="ntm_NeuesText">
          <xsl:value-of select="@Newstext" disable-output-escaping="yes" /> &#160;
        </div>
        <xsl:if test="string-length(@URL) != 0">
          <div class="ntm_NewsWeiter">
            <a href="{$SafeLinkUrl}" title="{$DisplayTitle}" target="{$LinkTarget}" class="ntm_MehrLink">
              Mehr lesen<span class="ntm_LinkIcon">&#160;</span>
            </a>
          </div>
        </xsl:if>
      </xsl:if>
      <xsl:if test="string-length(@HTMLText) != 0">
        <div class="ntm_NeuesHTML">
          <xsl:value-of select="@HTMLText" disable-output-escaping="yes" />&#160;
        </div>
        <xsl:if test="string-length(@URL) != 0">
          <div class="ntm_NewsWeiter">
            <a href="{$SafeLinkUrl}" title="{$DisplayTitle}" target="{$LinkTarget}" class="ntm_MehrLink">
              Mehr lesen<span class="ntm_LinkIcon">&#160;</span>
            </a>
          </div>
        </xsl:if>
      </xsl:if>
    </div>
    <xsl:if test="count(following-sibling::*)=0">
      <div class="ntm_NewsFooter ntm_Neuigkeiten">
        <span style="display:none;">
          <xsl:value-of select="@URL"/>
          <xsl:value-of select="@Newstext"/>
          <xsl:value-of select="@HTMLText"/>
          <xsl:value-of select="@Kategorie"/>
          <xsl:value-of select="@LinkToolTip"/>
          <xsl:value-of select="@AnzeigeVon"/>
          <xsl:value-of select="@OpenInNewWindow" />
        </span>
        <div class="ntm_WeiterButton">
          <a href="../Lists/Kurzmeldungen/" title="Alle News" target="_self" class="ntm_MehrLink">
            Alle Meldungen anzeigen<span class="ntm_LinkIcon">&#160;</span>
          </a>
        </div>
      </div>
    </xsl:if>
  </xsl:template>

  <xsl:template name="Termine" match="Row[@Style='Termine']" mode="itemstyle">
    <xsl:param name="CurPos" />
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="EventDateHelper">
      <xsl:value-of select="number(ddwrt:FormatDateTime(ddwrt:FormatDate(string(@EndDate),1033,1),1033,'yyyyMMdd'))"/> - <xsl:value-of select="ddwrt:FormatDateTime(ddwrt:FormatDate(string(@EventDate),1031,1),1031,'d MMMM yyyy')"/>
    </xsl:variable>
    <xsl:variable name="EventDay">
      <xsl:value-of select="ddwrt:FormatDateTime(ddwrt:FormatDate(string(@EventDate),1033,1),1031,'dd')"/>
    </xsl:variable>
    <xsl:variable name="EventMonth">
      <xsl:value-of select="ddwrt:FormatDateTime(ddwrt:FormatDate(string(@EventDate),1033,1),1031,'MMM')"/>
    </xsl:variable>
    <xsl:variable name="EventYear">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@EventDate),1031,'yyyy')" />
    </xsl:variable>
    <xsl:variable name="Beginn">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@EventDate),1031,'HH:mm')" />
    </xsl:variable>
    <xsl:variable name="Ende">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@EndDate),1031,'HH:mm')" />
    </xsl:variable>
    <xsl:variable name="TerminBeschreibung">
      <xsl:value-of select="substring-after(substring-before(@Description,'&#60;&#47;'),'&#62;')" disable-output-escaping="yes" />
    </xsl:variable>
    <xsl:variable name="Serientermin">
      <xsl:value-of select="@_fRecurrence"/>
    </xsl:variable>
    <xsl:variable name="Ganztagstermin">
      <xsl:value-of select="@_fAllDayEvent"/>
    </xsl:variable>
    <xsl:variable name="GanzT">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@_EventDate) ,1031 ,'dd.MM.yyyy')" />
    </xsl:variable>
    <xsl:variable name="BUhrzeit">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@_EventDate) ,1031 ,'HH:mm')" /> Uhr
    </xsl:variable>
    <xsl:variable name="EventDate">
      <xsl:choose>
        <xsl:when test="$Serientermin = 1 and $Ganztagstermin = 1">
          Ganzt&#228;giger Serientermin
        </xsl:when>
        <xsl:when test="$Serientermin = 0 and $Ganztagstermin = 1">
          <xsl:value-of select="$GanzT"/>
        </xsl:when>
        <xsl:when test="$Serientermin = 1 and $Ganztagstermin = 0">
          Serientermin <xsl:value-of select="$BUhrzeit"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="ddwrt:FormatDateTime(string(@_EventDate) ,1031 ,'dd.MM.yyyy, HH:mm')" /> Uhr
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <div class="ntm_Termin ntm_Tagestermine">
      <div class="ntm_DateBox">
        <div class="ntm_DateBoxDay">
          <xsl:value-of select="$EventDay"/>
        </div>
        <div class="ntm_DateBoxMonth">
          <div>
            <xsl:value-of select="$EventMonth"/>
          </div>
        </div>
      </div>
      <div class="ntm_TitelAuthorHold">
        <div class="ntm_Zeitraum">
          <xsl:value-of select="$EventDate"/>
        </div>
        <div class="ntm_TerminTitle">
          <a href="{$SafeLinkUrl}" title="{$TerminBeschreibung}" class="ntm_TermineTitleLink">
            <xsl:value-of select="@Title"/>
          </a>
        </div>
        <xsl:if test="string-length(@Location) != 0">
          <div class="ntm_Description">
            <a href="{$SafeLinkUrl}" title="{$TerminBeschreibung}" class="ntm_TermineTitleLink">
              <xsl:value-of select="@Location"/>
            </a>
          </div>
        </xsl:if>
      </div>
    </div>
  </xsl:template>
  <xsl:template name="TermineBoxen" match="Row[@Style='TermineBoxen']" mode="itemstyle">
    <xsl:param name="CurPos" />
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="EventDateHelper">
      <xsl:value-of select="number(ddwrt:FormatDateTime(ddwrt:FormatDate(string(@EndDate),1033,1),1033,'yyyyMMdd'))"/> - <xsl:value-of select="ddwrt:FormatDateTime(ddwrt:FormatDate(string(@EventDate),1031,1),1031,'d MMMM yyyy')"/>
    </xsl:variable>
    <xsl:variable name="EventDay">
      <xsl:value-of select="ddwrt:FormatDateTime(ddwrt:FormatDate(string(@EventDate),1033,1),1031,'dd')"/>
    </xsl:variable>
    <xsl:variable name="Wochentag">
      <xsl:value-of select="ddwrt:FormatDateTime(ddwrt:FormatDate(string(@EventDate),1033,1),1031,'dddd')"/>
    </xsl:variable>
    <xsl:variable name="EventMonth">
      <xsl:value-of select="ddwrt:FormatDateTime(ddwrt:FormatDate(string(@EventDate),1033,1),1031,'MMMM')"/>
    </xsl:variable>
    <xsl:variable name="EventYear">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@EventDate),1031,'yyyy')" />
    </xsl:variable>
    <xsl:variable name="Beginn">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@EventDate),1031,'HH:mm')" />
    </xsl:variable>
    <xsl:variable name="Ende">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@EndDate),1031,'HH:mm')" />
    </xsl:variable>
    <xsl:variable name="Serientermin">
      <xsl:value-of select="@_fRecurrence"/>
    </xsl:variable>
    <xsl:variable name="Ganztagstermin">
      <xsl:value-of select="@_fAllDayEvent"/>
    </xsl:variable>
    <xsl:variable name="GanzT">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@_EventDate) ,1031 ,'dd.MM.yyyy')" />
    </xsl:variable>
    <xsl:variable name="BUhrzeit">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@_EventDate) ,1031 ,'HH:mm')" /> Uhr
    </xsl:variable>
    <xsl:variable name="EventDate">
      <xsl:choose>
        <xsl:when test="$Serientermin = 1 and $Ganztagstermin = 1">
          Ganzt&#228;giger Serientermin
        </xsl:when>
        <xsl:when test="$Serientermin = 0 and $Ganztagstermin = 1">
          <xsl:value-of select="$GanzT"/>
        </xsl:when>
        <xsl:when test="$Serientermin = 1 and $Ganztagstermin = 0">
          Serientermin <xsl:value-of select="$BUhrzeit"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="ddwrt:FormatDateTime(string(@_EventDate) ,1031 ,'dd.MM.yyyy, HH:mm')" /> Uhr
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <a href="{$SafeLinkUrl}" title="{@Title}" class="ntm_Nachladen"><div class="ntm_TerminBox ntm_Tagestermine">
      <div class="ntm_DateBox">
        <div class="ntm_Wochentag">
          <xsl:value-of select="$Wochentag"/>
        </div>
        <div class="ntm_DateBoxDay">
          <xsl:value-of select="$EventDay"/>
        </div>
        <div class="ntm_DateBoxMonth">
          <div>
            <xsl:value-of select="$EventMonth"/>
          </div>
        </div>
      </div>
      <div class="ntm_TitelAuthorHold">
        <div class="ntm_TerminTitle">
          
            <xsl:value-of select="@Title"/>

        </div>
        <div class="ntm_Zeitraum">
          <xsl:value-of select="$EventDate"/>
        </div>
        <xsl:if test="string-length(@Location) != 0">
          <div class="ntm_Location">
              <xsl:value-of select="@Location"/>
          </div>
        </xsl:if>
        <xsl:if test="string-length(@Description) != 0">
          <div class="ntm_Description">
              <xsl:value-of select="@Description" disable-output-escaping="yes"/>
          </div>
        </xsl:if>
      </div>
    </div></a>
    <xsl:if test="count(following-sibling::*)=0">
      <span id="NTM_BtnLoad" class="ntm_NewsNachladen">
        <div class="ntm_BTNNachladen" title="Elemente nachladen">
          <img src="/_layouts/15/images/blank.gif" alt="Nachladen" /><br/>
          Weitere anzeigen
        </div>
      </span>
	</xsl:if>
  </xsl:template>

	<xsl:template name="NewsSliderOne" match="Row[@Style='NewsSliderOne']" mode="itemstyle">
		<xsl:variable name="DisplayTitle">
			<xsl:call-template name="OuterTemplate.GetTitle">
				<xsl:with-param name="Title" select="@Title"/>
				<xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="SafeLinkUrl">
			<xsl:call-template name="OuterTemplate.GetSafeLink">
				<xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="SafeImageUrl">
			<xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
				<xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="Einleitungstext">
			<xsl:choose>
				<xsl:when test="string-length(@_Comments) != 0">
					<xsl:value-of select="@_Comments"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@_Bildbeschreibung"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="clearLabel">
		  <xsl:call-template name="OuterTemplate.Replace">
			<xsl:with-param name="Value" select="@_Label"/>
			<xsl:with-param name="Search" select="';#'"/>
			<xsl:with-param name="Replace" select="' '"/>
		  </xsl:call-template>
		</xsl:variable>
		
		<xsl:if test="string-length($SafeImageUrl) != 0">
			<div class="ntm_SliderHoldItem">

				<xsl:choose>
					<xsl:when test="string-length($SafeImageUrl) != 0">
						<a href="{$SafeLinkUrl}" title="{$DisplayTitle}"><div class="ntm_Img"><img src="{$SafeImageUrl}?RenditionID=5" alt="{$DisplayTitle}" /></div></a>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$SafeLinkUrl}" title="{$DisplayTitle}"><div class="ntm_ImgPH"><img src="/_layouts/15/images/blank.gif" alt="Platzhalter Bild" /></div></a>
					</xsl:otherwise>
				</xsl:choose>
				
				<xsl:if test="string-length(@_Label) != 0">
					<div class="ntm_FlexLabel">
						<xsl:value-of select="$clearLabel" disable-output-escaping="yes" />
					</div>
				</xsl:if>
				
				<div class="ntm_FlexCaption"><div>
					<div class="ntm_FlexCaptionTitle">
						<xsl:value-of select="$DisplayTitle" disable-output-escaping="yes" />
					</div>
					<div class="ntm_FlexCaptionText">
						<xsl:value-of select="substring($Einleitungstext, 1, 210)" disable-output-escaping="yes" />
						<xsl:if test="string-length($Einleitungstext) > 210"> ...</xsl:if>
					</div>
					<div class="ntm_LinkZeile">
						<a href="{$SafeLinkUrl}" title="{$DisplayTitle}"><span class="ntm_MehrLink">mehr lesen</span></a>
					</div>
				</div></div>
			</div>
		</xsl:if>
		<xsl:if test="count(following-sibling::*)=0">
			<span style="display:none;">
				<xsl:value-of select="@_Comments"/>
				<xsl:value-of select="@_Bildbeschreibung"/>
				<xsl:value-of select="@_Label"/>
			</span>
		</xsl:if>
	</xsl:template>

	<xsl:template name="NewsSliderMore" match="Row[@Style='NewsSliderMore']" mode="itemstyle">
		<xsl:param name="CurPos" />
		<xsl:variable name="SafeLinkUrl">
			<xsl:call-template name="OuterTemplate.GetSafeLink">
				<xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="SafeImageUrl">
			<xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
				<xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="DisplayTitle">
			<xsl:call-template name="OuterTemplate.GetTitle">
				<xsl:with-param name="Title" select="@Title"/>
				<xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="clearLabel">
			<xsl:call-template name="OuterTemplate.Replace">
				<xsl:with-param name="Value" select="@_Label"/>
				<xsl:with-param name="Search" select="';#'"/>
				<xsl:with-param name="Replace" select="' '"/>
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:if test="count(preceding-sibling::*)=0">
		</xsl:if>

		<div class="ntm_NewsSlider contentBox">
			<xsl:choose>
				<xsl:when test="string-length($SafeImageUrl) != 0">
					<div class="ntm_NewsImg">
						<a href="{$SafeLinkUrl}" title="{@_LinkToolTip}">
							<img src="{$SafeImageUrl}?RenditionID=6" title="{@ImageUrlAltText}" />
						</a>
						<xsl:if test="string-length(@_Label) != 0">
							<div class="ntm_FlexLabel">
								<xsl:value-of select="$clearLabel" disable-output-escaping="yes" />
							</div>
						</xsl:if>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<div class="ntm_NewsImgPH">
						<a href="{$SafeLinkUrl}" title="{@_LinkToolTip}">
							<img src="/_layouts/15/images/blank.gif" alt="Platzhalter Bild" />
						</a>
						<xsl:if test="string-length(@_Label) != 0">
							<div class="ntm_FlexLabel">
								<xsl:value-of select="$clearLabel" disable-output-escaping="yes" />
							</div>
						</xsl:if>
					</div>
				</xsl:otherwise>
			</xsl:choose>

			<div class="ntm_dateBox"></div>
			<div class="ntm_NewsTitle">
				<a href="{$SafeLinkUrl}" title="{@_LinkToolTip}">
					<xsl:value-of select="$DisplayTitle"/>
				</a>
			</div>
			<div class="ntm_NewsText">
				<xsl:value-of select="@Einleitungstext" disable-output-escaping="yes" />
			</div>
			<div class="ntm_NewsWeiter">
				<a href="{$SafeLinkUrl}" title="{@_LinkToolTip}" ><span class="ms-rteStyle-LinkButton">Mehr</span></a>
			</div>
		</div>

		<xsl:if test="count(following-sibling::*)=0">
		</xsl:if>
	</xsl:template>

<xsl:template name="Tabelle" match="Row[@Style='Tabelle']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
    <div class="ntm_TableRow">
      <div class="ntm_TableCell ntm_TableCell1">
        <div>
          <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
            <xsl:if test="$ItemsHaveStreams = 'True'">
              <xsl:attribute name="onclick">
                <xsl:value-of select="@OnClickForWebRendering"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
              <xsl:attribute name="onclick">
                <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:value-of select="$DisplayTitle"/>
          </a>
        </div>
      </div>
      <div class="ntm_TableCell ntm_TableCell2">
        <div>
          <xsl:if test="string-length(@Description) != 0">
            <xsl:value-of select="@Description" /> &#160;
          </xsl:if>
          <a href="{$SafeLinkUrl}" title="{@LinkToolTip}" class="ntm_MehrLink">Lesen</a>
        </div>
      </div>
      <xsl:if test="string-length(@_DritteSpalte) != 0">
        <div class="ntm_TableCell ntm_TableCell3">
          <div>
            <xsl:value-of select="@_DritteSpalte" />
          </div>
        </div>
      </xsl:if>
    </div>
  </xsl:template>
  <xsl:template name="IconSlider" match="Row[@Style='IconSlider']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:value-of select="substring-after(@URL,', ')"/>
    </xsl:variable>
    <xsl:variable name="LinkHref">
      <xsl:value-of select="substring-before(@URL,', ')"/>
    </xsl:variable>
    <xsl:variable name="Tooltip">
      <xsl:value-of select="@Tooltip" disable-output-escaping="yes"/>
    </xsl:variable>
    <xsl:variable name="LaunchBehavior_">
      <xsl:choose>
        <xsl:when test="@OpenInNewWindow_ = 'True'">_blank</xsl:when>
        <xsl:otherwise>_self</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
	<a title="{$Tooltip}" href="{$LinkHref}" target="{$LaunchBehavior_}"><div class="ntm_IconBox" style="background-image: url('{$SafeImageUrl}');">
		<div class="ntm_IconTitle">
			<xsl:value-of select="$DisplayTitle"/>
		</div>
	</div></a>
  </xsl:template>
  <xsl:template name="Bilderslider" match="Row[@Style='Bilderslider']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="Titel">
      <xsl:value-of select="@_Titel" />
    </xsl:variable>
    <xsl:variable name="Einleitungstext">
      <xsl:choose>
        <xsl:when test="string-length(@_Comments) != 0">
          <xsl:value-of select="@_Comments"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="@_Bildbeschreibung"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:if test="string-length($SafeImageUrl) != 0">
      <a href="{$SafeLinkUrl}" title="{@_Titel}" class="image-link">
        <img src="{$SafeImageUrl}" alt="{@_Titel} {$Einleitungstext}" />
      </a>
    </xsl:if>
    <xsl:if test="count(following-sibling::*)=0">
      <span style="display:none;">
        <xsl:value-of select="@_Titel"/>
        <xsl:value-of select="@_Comments"/>
        <xsl:value-of select="@_Bildbeschreibung"/>
      </span>
    </xsl:if>
  </xsl:template>
  <xsl:template name="Bilder" match="Row[@Style='Bilder']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <div class="ntm_Bild">
      <xsl:if test="string-length($SafeImageUrl) != 0">
        <a href="{$SafeLinkUrl}" title="{@_Bildbeschreibung} - {@_Titel}" data-title="{@_Titel}" class="image-link" data-lightbox="imagesetX">
          <img class="ntm_Image" src="{$SafeImageUrl}" alt="{@_Titel} {@_Bildbeschreibung}" />
        </a>
      </xsl:if>
      <span style="display:none;">
        <xsl:value-of select="@_Titel" />
        <xsl:value-of select="@_Bildbeschreibung"/>
        <xsl:value-of select="@_Titel"/>
      </span>
    </div>
  </xsl:template>
  <xsl:template name="Bildergalerie" match="Row[@Style='Bildergalerie']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="string-length($SafeImageUrl) != 0">
      <a href="{$SafeLinkUrl}" data-lightbox="bildergalerie" title="{@_Bildbeschreibung} {@_Titel}">
        <img src="{$SafeImageUrl}" data-large="{$SafeImageUrl}" alt="{@_Bildbeschreibung} {@_Titel}" data-description="{@_Bildbeschreibung}" />
      </a>
    </xsl:if>
    <span style="display:none;">
      <xsl:value-of select="@_Titel"/>
      <xsl:value-of select="@_Bildbeschreibung"/>
    </span>
  </xsl:template>

  <xsl:template name="Bilderfade" match="Row[@Style='Bilderfade']" mode="itemstyle">
    <xsl:param name="CurPos" />
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="Titel">
      <xsl:value-of select="@_Titel" />
    </xsl:variable>
    <xsl:variable name="Bildbeschreibung">
      <xsl:value-of select="@_Bildbeschreibung" />
    </xsl:variable>
    <xsl:variable name="Datum">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@_Datum) ,1033 ,'dd.MM.yyyy')" />
    </xsl:variable>
    <div class="ntm_DivCyclerDiv">
      <xsl:if test="string-length($SafeImageUrl) != 0">
        <a href="{$SafeLinkUrl}" target="_blank" class="image-link" data-lightbox="imageset" title="{$Titel}">
          <img class="ntm_DivCyclerImage" src="{$SafeImageUrl}" alt="{$Titel} {$Bildbeschreibung}" />
        </a>
        <xsl:if test="string-length(@_Titel) != 0">
          <div class="ntm_CyclerTitle">
            <xsl:value-of select="$Titel"/>
          </div>
        </xsl:if>
        <xsl:if test="string-length(@_Datum) != 0">
          <div class="ntm_CyclerDatum">
            <xsl:value-of select="$Datum"/>
          </div>
        </xsl:if>
        <div class="ntm_CyclerComments">
          <xsl:value-of select="$Bildbeschreibung" />
        </div>
      </xsl:if>
      <div style="display:none;">
        <xsl:value-of select="@_Titel" />
        <xsl:value-of select="@_Bildbeschreibung" />
        <xsl:value-of select="@_Datum" />
      </div>
    </div>
  </xsl:template>


  <xsl:template name="Kacheln" match="Row[@Style='Kacheln']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="LaunchBehavior_">
      <xsl:choose>
        <xsl:when test="@OpenInNewWindow_ = 'True'">_blank</xsl:when>
        <xsl:otherwise>_self</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="HoverImageUrl_">
      <xsl:value-of select="substring-before(@BackgroundImageHover_,', ')"/>
    </xsl:variable>
    <div class="ntm_KachelnNorm boxgrid caption" title="{$DisplayTitle}">
      <div class="ntm_CtIcon {@TileColor}" hoverbg="{$HoverImageUrl_}" style="background-image: url('{$SafeImageUrl}');"></div>
      <div class="cover boxcaption" link="{$SafeLinkUrl}" target="{$LaunchBehavior_}">
        <div class="ntm_BoxCT">
          <div class="ntm_BoxTitle">
            <xsl:value-of select="$DisplayTitle" />
          </div>
        </div>
        <div class="ntm_BoxCT">
          <div class="ntm_BoxText">
            <xsl:value-of select="@ShortText_" disable-output-escaping="yes" />
          </div>
        </div>
        <div style="display:none;">
          <xsl:value-of select="@OpenInNewWindow_" />
          <xsl:value-of select="@BackgroundImage_" />
          <xsl:value-of select="@BackgroundImageHover_" />
        </div>
      </div>
    </div>
  </xsl:template>
  <xsl:template name="KachelnBig" match="Row[@Style='KachelnBig']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkDestination'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'BackgroundImage_'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="LaunchBehavior_">
      <xsl:choose>
        <xsl:when test="@OpenInNewWindow_ = 'True'">_blank</xsl:when>
        <xsl:otherwise>_self</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="HoverImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'BackgroundImageHover_'"/>
      </xsl:call-template>
    </xsl:variable>

    <div class="ntm_KachelnBig boxgrid caption" title="{$DisplayTitle}">
      <div class="ntm_CtIcon {@TileColor}" hoverbg="{$HoverImageUrl}" style="background-image: url('{$SafeImageUrl}');"></div>
      <div class="cover coverA boxcaption" link="{$SafeLinkUrl}" target="{$LaunchBehavior_}">
        <div class="ntm_BoxCT">
          <div class="ntm_BoxTitle">
            <xsl:value-of select="$DisplayTitle" />
          </div>
        </div>
        <div style="display:none;">
          <xsl:value-of select="@OpenInNewWindow_" />
          <xsl:value-of select="@BackgroundImage_" />
          <xsl:value-of select="@BackgroundImageHover_" />
        </div>
      </div>
      <div class="cover coverB boxcaption" link="{$SafeLinkUrl}" target="{$LaunchBehavior_}">
        <div class="ntm_BoxCT">
          <div class="ntm_BoxText">
            <xsl:value-of select="@ShortText_" disable-output-escaping="yes" />
          </div>
        </div>
      </div>
    </div>
  </xsl:template>
  <xsl:template name="KachelnEinfach" match="Row[@Style='KachelnEinfach']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkDestination'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'BackgroundImage_'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="LaunchBehavior_">
      <xsl:choose>
        <xsl:when test="@OpenInNewWindow_ = 'True'">_blank</xsl:when>
        <xsl:otherwise>_self</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="HoverImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'BackgroundImageHover_'"/>
      </xsl:call-template>
    </xsl:variable>

    <div class="ntm_KachelnEinfach boxgrid caption" title="{$DisplayTitle}">
      <div class="ntm_CtIcon {@TileColor}" hoverbg="{$HoverImageUrl}" style="background-image: url('{$SafeImageUrl}');"></div>
      <div class="cover coverA boxcaption" link="{$SafeLinkUrl}" target="{$LaunchBehavior_}">
        <div class="ntm_BoxCT">
          <div class="ntm_BoxTitle">
            <xsl:value-of select="$DisplayTitle" />
          </div>
        </div>
        <div style="display:none;">
          <xsl:value-of select="@OpenInNewWindow_" />
          <xsl:value-of select="@BackgroundImage_" />
          <xsl:value-of select="@BackgroundImageHover_" />
        </div>
      </div>
      <div class="cover coverB boxcaption" link="{$SafeLinkUrl}" target="{$LaunchBehavior_}">
        <div class="ntm_BoxCT">
          <div class="ntm_BoxText">
            <xsl:value-of select="@ShortText_" disable-output-escaping="yes" />
          </div>
        </div>
      </div>
    </div>
  </xsl:template>
  <xsl:template name="IconLinkButton" match="Row[@Style='IconLinkButton']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="LaunchBehavior_">
      <xsl:choose>
        <xsl:when test="@OpenInNewWindow_ = 'True'">_blank</xsl:when>
        <xsl:otherwise>_self</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <div class="ntm_IconLinkButton {@TileColor}" title="{$DisplayTitle}">
      <a href="{$SafeLinkUrl}" target="{$LaunchBehavior_}" class="ntm_Icon" style="background-image: url('{$SafeImageUrl}');">
        &#160;
      </a>
      <a href="{$SafeLinkUrl}" target="{$LaunchBehavior_}" class="ntm_BoxTitle" >
        <div class="ntm_BoxTitle">
          <xsl:value-of select="$DisplayTitle" disable-output-escaping="yes" />
        </div>
      </a>
      <div style="display:none;">
        <xsl:value-of select="@OpenInNewWindow_" />
        <xsl:value-of select="@BackgroundImage_" />
      </div>
    </div>
  </xsl:template>

  <xsl:template name="Dokumente" match="Row[@Style='Dokumente']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="LinkTarget">
      <xsl:choose>
        <xsl:when test="@OpenInNewWindow = 'True'">_blank</xsl:when>
        <xsl:otherwise>_self</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="Modified">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@Modified) ,1033 ,'dd.MM.yyyy, HH:mm')" /> Uhr
    </xsl:variable>
    <xsl:variable name="Author">
      <xsl:call-template name="OuterTemplate.GetGroupName">
        <xsl:with-param name="GroupName" select="@Author"/>
        <xsl:with-param name="GroupType" select="'User'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="Editor">
      <xsl:call-template name="OuterTemplate.GetGroupName">
        <xsl:with-param name="GroupName" select="@Editor"/>
        <xsl:with-param name="GroupType" select="'User'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DocType">
      <xsl:value-of select="@DocIcon"/>
    </xsl:variable>
    <xsl:variable name="SafeImageUrl">
      <xsl:choose>
        <xsl:when test="@DocIcon = 'webpart' or @DocIcon = 'wsp' or @DocIcon = 'eps' or @DocIcon = 'xaml' or @DocIcon = 'svg'" >
          <xsl:value-of select="concat('/_layouts/15/images/icgen.gif', '')"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="concat('/_layouts/15/images/ic',$DocType,'.gif')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <div id="linkitem" class="ntm_Documents item">
      <div class="xlink-item">
        <xsl:if test="string-length(@DocIcon) != 0">
          <a href="{$SafeLinkUrl}" target="{$LinkTarget}" class="ntm_DocIcon">
            <img class="image" src="{$SafeImageUrl}" alt="DocIcon" />
          </a>
        </xsl:if>
        <span class="ntm_Title">
          <a href="{$SafeLinkUrl}" target="{$LinkTarget}" title="{@LinkToolTip}">
            <xsl:value-of select="$DisplayTitle"/>
          </a>
          <xsl:if test="string-length(@_UIVersionString) != 0">
            - V<xsl:value-of select="@_UIVersionString"/>
          </xsl:if>

        </span>
        <div class="ntm_DocModified">
          <xsl:value-of select="$Modified"/> von <span>
            <xsl:value-of select="$Editor" />
          </span>
        </div>
        <div class="ntm_Description">
          <xsl:value-of select="@DocDescription" disable-output-escaping="yes"/>
        </div>
        <div class="ntm_Pfad">
          <xsl:value-of select="substring(@FileRef, 0, 300)" disable-output-escaping="yes"/>
        </div>
      </div>
    </div>
  </xsl:template>
  <xsl:template name="DokumenteBox" match="Row[@Style='DokumenteBox']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="LinkFilename">
      <xsl:value-of select="@LinkFilename"/>
    </xsl:variable>
    <xsl:variable name="LinkFilenameNoMenu">
      <xsl:value-of select="@LinkFilenameNoMenu"/>
    </xsl:variable>
    <xsl:variable name="FileLeafRef">
      <xsl:value-of select="@FileLeafRef"/>
    </xsl:variable>
    <xsl:variable name="Bibliothek">
      <xsl:value-of select="substring-before($SafeLinkUrl,$LinkFilename)"/>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="LinkTarget">
      <xsl:choose>
        <xsl:when test="@OpenInNewWindow = 'True'">_blank</xsl:when>
        <xsl:otherwise>_self</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="Modified">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@Modified),1033 ,'dd.MM.yyyy')" />
    </xsl:variable>
    <xsl:variable name="Editor">
      <xsl:call-template name="OuterTemplate.GetGroupName">
        <xsl:with-param name="GroupName" select="@Editor"/>
        <xsl:with-param name="GroupType" select="'User'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="FileExtension">
      <xsl:choose>
        <xsl:when test="@FileExtension = 'xls' or @FileExtension = 'xslx'" >
          xlsx
        </xsl:when>
        <xsl:when test="@FileExtension = 'doc' or @FileExtension = 'docx'" >
          docx
        </xsl:when>
        <xsl:when test="@FileExtension = 'ppt' or @FileExtension = 'pptx'" >
          pptx
        </xsl:when>
        <xsl:when test="@FileExtension = 'pdf'" >
          pdf
        </xsl:when>
        <xsl:when test="@FileExtension = 'gif' or @FileExtension = 'png' or @FileExtension = 'svg' or @FileExtension = 'jpg' or @FileExtension = 'tif' or @FileExtension = 'bmp'" >
          image
        </xsl:when>
        <xsl:when test="@FileExtension = 'mp4' or @FileExtension = 'mp3'" >
          media
        </xsl:when>
        <xsl:when test="@FileExtension = 'vsd' or @FileExtension = 'vsdx'" >
          vsdx
        </xsl:when>
        <xsl:when test="@FileExtension = 'htm' or @FileExtension = 'html' or @FileExtension = 'aspx' or @FileExtension = 'php'" >
          web
        </xsl:when>
        <xsl:when test="@FileExtension = ''" >
          folder
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="@DocIcon"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
	<div class="ntm_Nachladen">
	<xsl:choose>
        <xsl:when test="@FileExtension = ''" >
			<a href="{$SafeLinkUrl}" target="_blank" title="Den Ordner öffnen">
				<div class="ntm_DocumentBox">
					<div class="ntm_Title"><xsl:value-of select="$DisplayTitle"/></div>
					<div class="ntm_Infos">
						<div class="ntm_CtnDocIcon {$FileExtension}"><img class="ntm_DocIcon" src="{@DocumentIconImageUrl}" alt="DocIcon" /></div>
						<span><xsl:value-of select="$Editor" /></span><br/>
						<span><xsl:value-of select="$Modified"/></span>
					</div>
				</div>
			</a>
		</xsl:when>
		<xsl:otherwise>
			<div class="ntm_DocumentBox">
				<a href="{$SafeLinkUrl}" target="_blank" title="Das Dokument öffnen"><div class="ntm_Title"><xsl:value-of select="$DisplayTitle"/></div></a>
				<div class="ntm_Infos">
					<a href="{$SafeLinkUrl}" target="_blank" title="Das Dokument öffnen ({@FileRef})"><div class="ntm_CtnDocIcon {$FileExtension}"><img class="ntm_DocIcon" src="{@DocumentIconImageUrl}" alt="DocIcon" /></div></a>
					<span><xsl:value-of select="$Editor" /></span><br/>
					<span><xsl:value-of select="$Modified"/></span><br/>
					<a href="{$Bibliothek}" target="_blank" title="Die Bibliothek öffnen, in der das Dokument abgelegt ist" ><span>Bibliothek öffnen</span></a>
				</div>
			</div>
		</xsl:otherwise>
	</xsl:choose>
	</div>
    <xsl:if test="count(following-sibling::*)=0">
      <span id="NTM_BtnLoad" class="ntm_NewsNachladen">
        <div class="ntm_BTNNachladen" title="Elemente nachladen">
          <img src="/_layouts/15/images/blank.gif" alt="Nachladen" /><br/>
          Weitere anzeigen
        </div>
      </span>
	</xsl:if>
  </xsl:template>
  <xsl:template name="HistorischerKalender" match="Row[@Style='HistorischerKalender']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="Tag">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@Tag) ,1031 ,'dddd, dd. MMMM yyyy')" />
    </xsl:variable>
    <div class="ntm_HistorischerKalender">
		<div class="ntm_Kalenderblatt" title="{$DisplayTitle}">
			<div class="ntm_HKTitle">Kalenderblatt</div>
			<div class="ntm_DatumHeute">
				<xsl:value-of select="$Tag"/>
			</div>
		</div>
	</div>
	<div id="NTM_CtnKB" class="ntm_KalenderblattBig">
		<div class="ntm_CtnTitle">
			<div class="ntm_close">x</div>
			<div class="ntm_HKTitle">Kalenderblatt</div>
			<div class="ntm_DatumHistorisch">
				<xsl:value-of select="@HistorischerTag" disable-output-escaping="yes"/>
			</div>
		</div>
		<div class="ntm_CtnEreignis">
			<xsl:value-of select="@Ereignis" disable-output-escaping="yes"/>
		</div>
	</div>
  </xsl:template>
  <xsl:template name="Zitate" match="Row[@Style='Zitate']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <div class="ntm_Zitate {@Schriftgroesse}"><div>
        <div class="ntm_Zitattitel"><xsl:value-of select="$DisplayTitle" disable-output-escaping="yes"/></div>
		<div class="ntm_Quelle"><xsl:value-of select="@Quellenangabe" disable-output-escaping="yes"/></div>
    </div></div>
  </xsl:template>





  <xsl:template name="NoImage" match="Row[@Style='NoImage']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <div class="item link-item">
      <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
      <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
        <xsl:if test="$ItemsHaveStreams = 'True'">
          <xsl:attribute name="onclick">
            <xsl:value-of select="@OnClickForWebRendering"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
          <xsl:attribute name="onclick">
            <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:value-of select="$DisplayTitle"/>
      </a>
      <div class="description">
        <xsl:value-of select="@Description" />
      </div>
    </div>
  </xsl:template>
  <xsl:template name="TitleOnly" match="Row[@Style='TitleOnly']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <div class="item link-item">
      <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
      <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
        <xsl:if test="$ItemsHaveStreams = 'True'">
          <xsl:attribute name="onclick">
            <xsl:value-of select="@OnClickForWebRendering"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
          <xsl:attribute name="onclick">
            <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:value-of select="$DisplayTitle"/>
      </a>
    </div>
  </xsl:template>
  <xsl:template name="TitleWithBackground" match="Row[@Style='TitleWithBackground']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <div class="title-With-Background">
      <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
      <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
        <xsl:if test="$ItemsHaveStreams = 'True'">
          <xsl:attribute name="onclick">
            <xsl:value-of select="@OnClickForWebRendering"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
          <xsl:attribute name="onclick">
            <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:value-of select="$DisplayTitle"/>
      </a>
    </div>
  </xsl:template>
  <xsl:template name="Bullets" match="Row[@Style='Bullets']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <div class="item link-item bullet">
      <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
      <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
        <xsl:if test="$ItemsHaveStreams = 'True'">
          <xsl:attribute name="onclick">
            <xsl:value-of select="@OnClickForWebRendering"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
          <xsl:attribute name="onclick">
            <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:value-of select="$DisplayTitle"/>
      </a>
    </div>
  </xsl:template>
  <xsl:template name="ImageRight" match="Row[@Style='ImageRight']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <div class="item">
      <xsl:if test="string-length($SafeImageUrl) != 0">
        <div class="image-area-right">
          <a href="{$SafeLinkUrl}">
            <xsl:if test="$ItemsHaveStreams = 'True'">
              <xsl:attribute name="onclick">
                <xsl:value-of select="@OnClickForWebRendering"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
              <xsl:attribute name="onclick">
                <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
              </xsl:attribute>
            </xsl:if>
            <img class="image" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
              <xsl:if test="$ImageWidth != ''">
                <xsl:attribute name="width">
                  <xsl:value-of select="$ImageWidth" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="$ImageHeight != ''">
                <xsl:attribute name="height">
                  <xsl:value-of select="$ImageHeight" />
                </xsl:attribute>
              </xsl:if>
            </img>
          </a>
        </div>
      </xsl:if>
      <div class="link-item">
        <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
        <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
          <xsl:if test="$ItemsHaveStreams = 'True'">
            <xsl:attribute name="onclick">
              <xsl:value-of select="@OnClickForWebRendering"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
            <xsl:attribute name="onclick">
              <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:value-of select="$DisplayTitle"/>
        </a>
        <div class="description">
          <xsl:value-of select="@Description" />
        </div>
      </div>
    </div>
  </xsl:template>
  <xsl:template name="ImageTop" match="Row[@Style='ImageTop']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="Url" select="@LinkUrl"/>
      </xsl:call-template>
    </xsl:variable>
    <div class="item ntm_BildObenFloat"><div>
      <xsl:if test="string-length($SafeImageUrl) != 0">
        <div class="image-area-top">
          <a href="{$SafeLinkUrl}">
            <xsl:if test="$ItemsHaveStreams = 'True'">
              <xsl:attribute name="onclick">
                <xsl:value-of select="@OnClickForWebRendering"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
              <xsl:attribute name="onclick">
                <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
              </xsl:attribute>
            </xsl:if>
            <img class="image" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
              <xsl:if test="$ImageWidth != ''">
                <xsl:attribute name="width">
                  <xsl:value-of select="$ImageWidth" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="$ImageHeight != ''">
                <xsl:attribute name="height">
                  <xsl:value-of select="$ImageHeight" />
                </xsl:attribute>
              </xsl:if>
            </img>
          </a>
        </div>
      </xsl:if>
      <div class="link-item">
        <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
        <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
          <xsl:if test="$ItemsHaveStreams = 'True'">
            <xsl:attribute name="onclick">
              <xsl:value-of select="@OnClickForWebRendering"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
            <xsl:attribute name="onclick">
              <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:value-of select="$DisplayTitle"/>
        </a>
        <div class="description">
          <xsl:value-of select="@Description" />
        </div>
      </div>
    </div></div>
  </xsl:template>
  <xsl:template name="ImageTopCentered" match="Row[@Style='ImageTopCentered']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <div class="item centered">
      <xsl:if test="string-length($SafeImageUrl) != 0">
        <div class="image-area-top">
          <a href="{$SafeLinkUrl}" >
            <xsl:if test="$ItemsHaveStreams = 'True'">
              <xsl:attribute name="onclick">
                <xsl:value-of select="@OnClickForWebRendering"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
              <xsl:attribute name="onclick">
                <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
              </xsl:attribute>
            </xsl:if>
            <img class="image" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
              <xsl:if test="$ImageWidth != ''">
                <xsl:attribute name="width">
                  <xsl:value-of select="$ImageWidth" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="$ImageHeight != ''">
                <xsl:attribute name="height">
                  <xsl:value-of select="$ImageHeight" />
                </xsl:attribute>
              </xsl:if>
            </img>
          </a>
        </div>
      </xsl:if>
      <div class="link-item">
        <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
        <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
          <xsl:if test="$ItemsHaveStreams = 'True'">
            <xsl:attribute name="onclick">
              <xsl:value-of select="@OnClickForWebRendering"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
            <xsl:attribute name="onclick">
              <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:value-of select="$DisplayTitle"/>
        </a>
        <div class="description">
          <xsl:value-of select="@Description" />
        </div>
      </div>
    </div>
  </xsl:template>
  <xsl:template name="LargeTitle" match="Row[@Style='LargeTitle']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <div class="item">
      <xsl:if test="string-length($SafeImageUrl) != 0">
        <div class="image-area-left">
          <a href="{$SafeLinkUrl}">
            <xsl:if test="$ItemsHaveStreams = 'True'">
              <xsl:attribute name="onclick">
                <xsl:value-of select="@OnClickForWebRendering"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
              <xsl:attribute name="onclick">
                <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
              </xsl:attribute>
            </xsl:if>
            <img class="image" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
              <xsl:if test="$ImageWidth != ''">
                <xsl:attribute name="width">
                  <xsl:value-of select="$ImageWidth" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="$ImageHeight != ''">
                <xsl:attribute name="height">
                  <xsl:value-of select="$ImageHeight" />
                </xsl:attribute>
              </xsl:if>
            </img>
          </a>
        </div>
      </xsl:if>
      <div class="link-item-large">
        <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
        <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
          <xsl:if test="$ItemsHaveStreams = 'True'">
            <xsl:attribute name="onclick">
              <xsl:value-of select="@OnClickForWebRendering"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
            <xsl:attribute name="onclick">
              <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:value-of select="$DisplayTitle"/>
        </a>
        <div class="description">
          <xsl:value-of select="@Description" />
        </div>
      </div>
    </div>
  </xsl:template>
  <xsl:template name="ClickableImage" match="Row[@Style='ClickableImage']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <div class="item">
      <xsl:if test="string-length($SafeImageUrl) != 0">
        <div class="image-area-left">
          <a href="{$SafeLinkUrl}">
            <xsl:if test="$ItemsHaveStreams = 'True'">
              <xsl:attribute name="onclick">
                <xsl:value-of select="@OnClickForWebRendering"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
              <xsl:attribute name="onclick">
                <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
              </xsl:attribute>
            </xsl:if>
            <img class="image" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
              <xsl:if test="$ImageWidth != ''">
                <xsl:attribute name="width">
                  <xsl:value-of select="$ImageWidth" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="$ImageHeight != ''">
                <xsl:attribute name="height">
                  <xsl:value-of select="$ImageHeight" />
                </xsl:attribute>
              </xsl:if>
            </img>
          </a>
        </div>
      </xsl:if>
    </div>
  </xsl:template>
  <xsl:template name="NotClickableImage" match="Row[@Style='NotClickableImage']" mode="itemstyle">
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <div class="item">
      <xsl:if test="string-length($SafeImageUrl) != 0">
        <div class="image-area-left">
          <img class="image" src="{$SafeImageUrl}" title="{@ImageUrlAltText}">
            <xsl:if test="$ImageWidth != ''">
              <xsl:attribute name="width">
                <xsl:value-of select="$ImageWidth" />
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="$ImageHeight != ''">
              <xsl:attribute name="height">
                <xsl:value-of select="$ImageHeight" />
              </xsl:attribute>
            </xsl:if>
          </img>
        </div>
      </xsl:if>
    </div>
  </xsl:template>
  <xsl:template name="FixedImageSize" match="Row[@Style='FixedImageSize']" mode="itemstyle">
    <xsl:variable name="SafeImageUrl">
      <xsl:call-template name="OuterTemplate.GetSafeStaticUrl">
        <xsl:with-param name="UrlColumnName" select="'ImageUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="@Title"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <div class="item">
      <xsl:if test="string-length($SafeImageUrl) != 0">
        <div class="image-area-left">
          <a href="{$SafeLinkUrl}">
            <xsl:if test="$ItemsHaveStreams = 'True'">
              <xsl:attribute name="onclick">
                <xsl:value-of select="@OnClickForWebRendering"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
              <xsl:attribute name="onclick">
                <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
              </xsl:attribute>
            </xsl:if>
            <img class="image-fixed-width" src="{$SafeImageUrl}" title="{@ImageUrlAltText}"/>
          </a>
        </div>
      </xsl:if>
      <div class="link-item">
        <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
        <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
          <xsl:if test="$ItemsHaveStreams = 'True'">
            <xsl:attribute name="onclick">
              <xsl:value-of select="@OnClickForWebRendering"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
            <xsl:attribute name="onclick">
              <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:value-of select="$DisplayTitle"/>
        </a>
        <div class="description">
          <xsl:value-of select="@Description" />
        </div>
      </div>
    </div>
  </xsl:template>
  <xsl:template name="WithDocIcon" match="Row[@Style='WithDocIcon']" mode="itemstyle">
    <xsl:variable name="SafeLinkUrl">
      <xsl:call-template name="OuterTemplate.GetSafeLink">
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="DisplayTitle">
      <xsl:call-template name="OuterTemplate.GetTitle">
        <xsl:with-param name="Title" select="''"/>
        <xsl:with-param name="UrlColumnName" select="'LinkUrl'"/>
        <xsl:with-param name="UseFileName" select="1"/>
      </xsl:call-template>
    </xsl:variable>
    <div class="item link-item">
      <xsl:if test="string-length(@DocumentIconImageUrl) != 0">
        <div class="image-area-left">
          <img class="image" src="{@DocumentIconImageUrl}" title="" />
        </div>
      </xsl:if>
      <div class="link-item">
        <xsl:call-template name="OuterTemplate.CallPresenceStatusIconTemplate"/>
        <a href="{$SafeLinkUrl}" title="{@LinkToolTip}">
          <xsl:if test="$ItemsHaveStreams = 'True'">
            <xsl:attribute name="onclick">
              <xsl:value-of select="@OnClickForWebRendering"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="$ItemsHaveStreams != 'True' and @OpenInNewWindow = 'True'">
            <xsl:attribute name="onclick">
              <xsl:value-of disable-output-escaping="yes" select="$OnClickTargetAttribute"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:choose>
            <xsl:when test="string-length(@Title) != 0">
              <xsl:value-of select="@Title"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$DisplayTitle"/>
            </xsl:otherwise>
          </xsl:choose>
        </a>
        <div class="description">
          <xsl:value-of select="@Description" />
        </div>
      </div>
    </div>
  </xsl:template>
  <xsl:template name="ListAllFields" match="Row[@Style='ListAllFields']" mode="itemstyle">
    <xsl:for-each select="@*">
      <p>
        <xsl:value-of select="name()"/>
        <xsl:text> = </xsl:text>
        <xsl:value-of select="."/>
        <br/>
      </p>
    </xsl:for-each>
  </xsl:template>
  <xsl:template name="HiddenSlots" match="Row[@Style='HiddenSlots']" mode="itemstyle">
    <div class="SipAddress">
      <xsl:value-of select="@SipAddress" />
    </div>
    <div class="LinkToolTip">
      <xsl:value-of select="@LinkToolTip" />
    </div>
    <div class="OpenInNewWindow">
      <xsl:value-of select="@OpenInNewWindow" />
    </div>
    <div class="OnClickForWebRendering">
      <xsl:value-of select="@OnClickForWebRendering" />
    </div>
  </xsl:template>
</xsl:stylesheet>
