﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Taxonomy;
using System.Web;

namespace nteam.Layout.ZAP.Intranet.CONTROLTEMPLATES
{
    public partial class ZAP_globalnav : UserControl
    {
        public string html { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            html = "";
            //if (!Page.IsPostBack)
            //{
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                string url = SPContext.Current.Site.WebApplication.AlternateUrls[0].Uri.AbsoluteUri;
                var ctx = HttpContext.Current;
                try
                {
                    HttpContext.Current = null;
                    using (SPSite thisSite = new SPSite(url))
                    {

                        TaxonomySession session = new TaxonomySession(thisSite);
                        TermStoreCollection stores = session.TermStores;

                        try
                        {
                            foreach (TermStore termStore in stores)
                            {
                                Group navGroup = termStore.Groups["ZAP-Intranet-GlobalNav"];
                                foreach (TermSet topSet in navGroup.TermSets)
                                {
                                    html += writeTerms(topSet.Terms);
                                }
                            }
                        }
                        catch
                        {
                        }

                        finally
                        {
                            GlobalNavContainer.Text = "";
                            GlobalNavContainer.Text = html;
                        }
                    }
                }
                finally
                {
                    HttpContext.Current = ctx;
                }
            });
            // }
        }

        public string writeTerms(TermCollection terms)
        {
            if (terms.Count > 0)
            {
                html += "\n<ul class=\"GlobalNav\" id=\"NTM_GlobalNav\">\n";
                foreach (Term subTerm in terms)
                {
                    try
                    {
                        html += "<li><a " + string.Format("href=\"{0}\"", subTerm.LocalCustomProperties["_Sys_Nav_SimpleLinkUrl"]) + (subTerm.CustomProperties.ContainsKey("target") ? string.Format(" target=\"{0}\"", subTerm.CustomProperties["target"]) : string.Empty) + "><span>" + subTerm.Name + "</span></a>";
                        writeTerms(subTerm.Terms);
                        html += "</li>\n";
                    }

                    catch
                    {
                        html += "<li class=\"ntm_NoLink\"><a href=\"#\"><span>" + subTerm.Name + "</span></a>";
                        writeTerms(subTerm.Terms);
                        html += "</li>\n";
                    }
                }

                html += "</ul>\n";
            }
            return html;
        }
    }
}