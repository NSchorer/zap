﻿using System;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.Workflow;

namespace nteam.Layout.ZAP.Intranet.WebProvisionedEventReceiver
{
    /// <summary>
    /// Web Events
    /// </summary>
    public class WebProvisionedEventReceiver : SPWebEventReceiver
    {
        private const string InheritMasterFeatureId = "2581f24f-79dc-424b-98f8-3c055b4be61e";
        private const string SubSiteStaplerFeatureId = "0e35b1d1-b28c-4d0b-9525-91250dbcebed";
        /// <summary>
        /// A site was provisioned.
        /// </summary>
        public override void WebProvisioned(SPWebEventProperties properties)
        {
            base.WebProvisioned(properties);
            InheritMasterIfNeeded(properties);
        }

        private void InheritMasterIfNeeded(SPWebEventProperties properties)
        {
            if (IsStaplerFeatureEnabled(properties.Web.Site))
            {
                EnableInheritMasterFeature(properties.Web);
            }
        }

        private bool IsStaplerFeatureEnabled(SPSite sitecollection)
        {
            return sitecollection.Features[new Guid(SubSiteStaplerFeatureId)] != null;
        }

        private void EnableInheritMasterFeature(SPWeb targetWeb)
        {
            targetWeb.Features.Add(new Guid(InheritMasterFeatureId), true);
            targetWeb.Update();
        }
    }
}