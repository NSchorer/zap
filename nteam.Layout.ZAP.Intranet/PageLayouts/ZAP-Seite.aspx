﻿<%@ Page language="C#"   Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=16.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register tagprefix="WebControls" namespace="Microsoft.SharePoint.Publishing.WebControls" assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceholderID="PlaceHolderAdditionalPageHead" runat="server">
	<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %>" runat="server"/>
	<link rel="stylesheet" type="text/css" href="/_layouts/15/styles/ZAP/pl_seite.css" />
	<PublishingWebControls:EditModePanel runat="server">
		<!-- Styles for edit mode only-->
		<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/editmode15.css %>"
			After="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %>" runat="server"/>
        <link rel="stylesheet" type="text/css" href="/_layouts/15/styles/ZAP/pl_editmode.css" />
	</PublishingWebControls:EditModePanel>
	<SharePointWebControls:FieldValue id="PageStylesField" FieldName="HeaderStyleDefinitions" runat="server"/>
</asp:Content>

<asp:Content ContentPlaceHolderId="PlaceHolderPageTitle" runat="server">
	<SharePoint:ProjectProperty Property="Title" runat="server"/> - <SharePointWebControls:FieldValue ID="FieldValue1" FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderPageTitleInTitleArea" runat="server"></asp:Content>

<asp:Content ContentPlaceHolderId="PlaceHolderTitleBreadcrumb" runat="server"> <SharePointWebControls:ListSiteMapPath ID="ListSiteMapPath1" runat="server" SiteMapProviders="CurrentNavigationSwitchableProvider" RenderCurrentNodeAsLink="false" PathSeparator="" CssClass="s4-breadcrumb" NodeStyle-CssClass="s4-breadcrumbNode" CurrentNodeStyle-CssClass="s4-breadcrumbCurrentNode" RootNodeStyle-CssClass="s4-breadcrumbRootNode" NodeImageOffsetX=0 NodeImageOffsetY=289 NodeImageWidth=16 NodeImageHeight=16 NodeImageUrl="/_layouts/15/images/fgimg.png?rev=23" HideInteriorRootNodes="true" SkipLinkText="" /> </asp:Content>

<asp:Content ContentPlaceholderID="PlaceHolderMain" runat="server">

	<div class="ntm_PL ntm_PLSeite">
		<PublishingWebControls:EditModePanel ID="EditModePanel2" runat="server" CssClass="edit-mode-panel title-edit">
			<p>
                <div class="ntm_Wichtig">Änderung des Seitennamens<br />
                    Seiten sollten einen logischen Namen besitzen. Verwenden Sie bitte keine Sonderzeichen oder Umlaute im Seitennamen, sondern nur die Zeichen A-Z, a-z, 0-9. Statt Leerzeichen im Dokumentennamen bitte einen Bindestrich verwenden.<br />
                    Eine Seite mit dem Namen "default.aspx" bitte <u>nicht</u> umbenennen!
                </div>
                <SharePoint:FieldLabel ID="FieldLabelFileName" FieldName="FileLeafRef" runat="server"/><SharePoint:FileField id="PageNameInEditMode" FieldName="FileLeafRef" DisableInputFieldLabel="true" runat="server"/></p>
			<p><SharePointWebControls:TextField ID="TextFieldTitle" runat="server" FieldName="Title"/></p>
		</PublishingWebControls:EditModePanel>
		
		<div class="ntm_PLCont">

			<div class="ntm_PL_Main">
                <div class="ntm_WPZFloatRight">
					<WebPartPages:WebPartZone runat="server" Title="Rechte Marginalspalte" ID="WPZFloatRight" />
				</div>
				<h1><SharePointWebControls:FieldValue ID="FieldValueTitle" FieldName="Title" runat="server"/></h1>
				<PublishingWebControls:RichHtmlField ID="RichHtmlField2" FieldName="PublishingPageContent" HasInitialFocus="True" MinimumEditHeight="200px" runat="server"/>

				<div class="ntm_PLWPZ">
					<div class="ntm_PL_1_1">
						<WebPartPages:WebPartZone runat="server" Title="Top" ID="TopZone" />
					</div>
					<div class="ntm_Table">
						<div class="ntm_PL_ZweiDrittelL">
							<WebPartPages:WebPartZone runat="server" Title="Zwei Drittel links" ID="ZweiDrittel" />
						</div>
						<div class="ntm_PL_1_3R">
							<WebPartPages:WebPartZone runat="server" Title="Ein Drittel rechts" ID="EinDrittel" />
						</div>
					</div>
					<div class="ntm_Table">
						<div class="ntm_PL_1_2">
							<WebPartPages:WebPartZone runat="server" Title="<%$Resources:cms,WebPartZoneTitle_TopLeft%>" ID="TopLeftRow" />
						</div>
						<div class="ntm_PL_2_2">
							<WebPartPages:WebPartZone runat="server" Title="<%$Resources:cms,WebPartZoneTitle_TopRight%>" ID="TopRightRow" />
						</div>
					</div>
					<div class="ntm_Table">
						<div class="ntm_PL_1_3">
							<WebPartPages:WebPartZone runat="server" Title="<%$Resources:cms,WebPartZoneTitle_CenterLeft%>" ID="CenterLeftColumn" />
						</div>
						<div class="ntm_PL_2_3">
							<WebPartPages:WebPartZone runat="server" Title="<%$Resources:cms,WebPartZoneTitle_Center%>" ID="CenterColumn" />
						</div>
						<div class="ntm_PL_3_3">
							<WebPartPages:WebPartZone runat="server" Title="<%$Resources:cms,WebPartZoneTitle_CenterRight%>" ID="CenterRightColumn" />
						</div>
					</div>
					<div class="ntm_PL_Foot">
						<WebPartPages:WebPartZone runat="server" Title="<%$Resources:cms,WebPartZoneTitle_Footer%>" ID="Footer"/>
					</div>
				</div>

				<div style="visibility:hidden;height:0px;"> x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x </div>
				
			</div>
			
		</div>

		<div class="ntm_PLModified"><span style="display:none">Erstellt von <SharePointWebControls:UserField ID="CreatedBy" runat="server" FieldName="Author" ControlMode="Display" /> <div style="display:none!important;">am <SharePointWebControls:DateTimeField ID="Created" FieldName="Created" runat="server" ControlMode="Display" /> Uhr &nbsp;|&nbsp; </div>  &nbsp; &nbsp; &nbsp; </span>Zuletzt geändert von <SharePointWebControls:UserField ID="ModifiedBy" runat="server" FieldName="Editor" ControlMode="Display" /> am <SharePointWebControls:DateTimeField ID="Modified" FieldName="Modified" runat="server" ControlMode="Display" /> Uhr<div style="display:none!important;"> &nbsp;|&nbsp; <a href="#Pagetop" title="Nach oben scrollen" class="ntm_Anchor ntm_Oben">Nach oben</a> &nbsp;|&nbsp; <a href="#" onmousedown="printPage();" title="Drucken" class="ntm_Print">Drucken</a> </div></div>
		<PublishingWebControls:editmodepanel runat="server" id="editmodepanelPageInfos" CssClass="edit-mode-panel">
			<div>
				Created: <SharePointWebControls:FieldValue FieldName="Created" runat="server" /><br />
				Author: <SharePointWebControls:FieldValue FieldName="Author" runat="server" /><br />
				Last Modified: <SharePointWebControls:FieldValue FieldName="Modified" runat="server" /><br />
				Last Modified by: <SharePointWebControls:FieldValue FieldName="Editor" runat="server" /><br />
				Pagelayout: <SharePointWebControls:FieldValue FieldName="PublishingPageLayout" runat="server" /><br />
				Content Type: <SharePointWebControls:FieldValue FieldName="ContentType" runat="server" /><br />
				Version: <SharePointWebControls:FieldValue FieldName="Version" runat="server" /><br />
				LikesCount: <SharePointWebControls:FieldValue FieldName="LikesCount" runat="server" /><br />
			</div>
		</PublishingWebControls:EditModePanel>

	</div>

</asp:Content>
