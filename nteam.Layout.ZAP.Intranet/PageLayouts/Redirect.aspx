﻿<%@ Page language="C#"   Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=16.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content ContentPlaceHolderId="PlaceHolderAdditionalPageHead" runat="server">
	<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %>" runat="server"/>
    <link rel="stylesheet" type="text/css" href="/_layouts/15/styles/ZAP/pl_seite.css" /
	<PublishingWebControls:EditModePanel runat="server">
		<!-- Styles for edit mode only-->
		<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/editmode15.css %>"
			After="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %>" runat="server"/>
        <link rel="stylesheet" type="text/css" href="/_layouts/15/styles/ZAP/pl_editmode.css" />
	</PublishingWebControls:EditModePanel>
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderPageImage" runat="server">
	<SharePointWebControls:AlphaImage id="newshomepageimageid" Src="/_layouts/images/newshomepage.png" Width=49 Height=49 Alt="" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderMain" runat="server">
	<PublishingWebControls:RedirectControl SecondsBeforeRedirect="0" runat="server"/>
	<div class="redirect">
		<SharePointWebControls:FormField FieldName="RedirectURL" runat="server"/>
		<PublishingWebControls:EditModePanel runat="server" CssClass="edit-mode-panel title-edit">
			<p><SharePoint:FieldLabel ID="FieldLabelFileName" FieldName="FileLeafRef" runat="server"/><SharePoint:FileField id="PageNameInEditMode" FieldName="FileLeafRef" DisableInputFieldLabel="true" runat="server"/></p>
			<p><SharePointWebControls:TextField ID="TextField1" runat="server" FieldName="Title"/></p>
            <PublishingWebControls:RichImageField ID="RichImageField1" FieldName="PublishingRollupImage" runat="server"/>
		</PublishingWebControls:EditModePanel>
	</div>
</asp:Content>
