﻿<%@ Page language="C#"   Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=16.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register tagprefix="WebControls" namespace="Microsoft.SharePoint.Publishing.WebControls" assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SearchWC" Namespace="Microsoft.Office.Server.Search.WebControls" Assembly="Microsoft.Office.Server.Search, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePointPortalControls" Namespace="Microsoft.SharePoint.Portal.WebControls" Assembly="Microsoft.SharePoint.Portal, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Taxonomy" Namespace="Microsoft.SharePoint.Taxonomy" Assembly="Microsoft.SharePoint.Taxonomy, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceholderID="PlaceHolderAdditionalPageHead" runat="server">
	<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %>" runat="server"/>
	<link rel="stylesheet" type="text/css" href="/_layouts/15/styles/ZAP/pl_seite.css" />
	<link rel="stylesheet" type="text/css" href="/_layouts/15/styles/ZAP/pl_wikiseite.css" />
	<PublishingWebControls:EditModePanel runat="server">
		<!-- Styles for edit mode only-->
		<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/editmode15.css %>"
			After="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %>" runat="server"/>
        <link rel="stylesheet" type="text/css" href="/_layouts/15/styles/ZAP/pl_editmode.css" />
	</PublishingWebControls:EditModePanel>
	<SharePointWebControls:FieldValue id="PageStylesField" FieldName="HeaderStyleDefinitions" runat="server"/>
</asp:Content>

<asp:Content ContentPlaceHolderId="PlaceHolderPageTitle" runat="server">
	<SharePoint:ProjectProperty Property="Title" runat="server"/> - <SharePointWebControls:FieldValue ID="FieldValue1" FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderPageTitleInTitleArea" runat="server"></asp:Content>

<asp:Content ContentPlaceHolderId="PlaceHolderTitleBreadcrumb" runat="server">
	<SharePoint:ListSiteMapPath
		runat="server"
		SiteMapProviders="CurrentNavigation"
		RenderCurrentNodeAsLink="false"
		PathSeparator=""
		CssClass="s4-breadcrumb"
		NodeStyle-CssClass="s4-breadcrumbNode"
		CurrentNodeStyle-CssClass="s4-breadcrumbCurrentNode"
		RootNodeStyle-CssClass="s4-breadcrumbRootNode"
		NodeImageOffsetX=0
		NodeImageOffsetY=289
		NodeImageWidth=16
		NodeImageHeight=16
		NodeImageUrl="/_layouts/15/images/fgimg.png?rev=23"
		HideInteriorRootNodes="true"
		SkipLinkText="" />
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderMain" runat="server">
	<div>


				<PublishingWebControls:EditModePanel class="ewiki-margin" runat="server">
					<div class="ms-descriptiontext">
                        <div class="ntm_Wichtig">Änderung des Seitennamen<br />
                            Seiten sollten einen logischen Namen besitzen. Verwenden Sie bitte keine Sonderzeichen oder Umlaute im Seitennamen, sondern nur die Zeichen A-Z, a-z, 0-9. Statt Leerzeichen im Dokumentennamen bitte einen Bindestrich verwenden.<br />
                            Eine Seite mit dem Namen "default.aspx" bitte <u>nicht</u> umbenennen!
                        </div>
                        <SharePoint:FieldLabel ID="FieldLabelFileName" FieldName="FileLeafRef" runat="server"/><SharePoint:FileField id="PageNameInEditMode" CssClass="ewiki-pagename-align" FieldName="FileLeafRef" DisableInputFieldLabel="true" runat="server"/>
                        <br/>
						<SharePointWebControls:TextField ID="TextFieldTitle" runat="server" FieldName="Title"/>
					</div>
				</PublishingWebControls:EditModePanel>
				<div class="ntm_PLWPZ">
        		    <div class="ntm_PL_1_1">
					    <WebPartPages:WebPartZone runat="server" Title="Top" ID="TopZone" />
				    </div>
			    </div>
				<div class="edit-mode-border">
					<PublishingWebControls:RichHtmlField id="PageContent" FieldName="PublishingPageContent" MinimumEditHeight="400px" DisableInputFieldLabel="true" runat="server"/>
				</div>
				<div class="ntm_PLWPZ">
					<div class="ntm_Table">
						<div class="ntm_PL_1_2">
							<WebPartPages:WebPartZone runat="server" Title="<%$Resources:cms,WebPartZoneTitle_TopLeft%>" ID="TopLeftRow" />
						</div>
						<div class="ntm_PL_2_2">
							<WebPartPages:WebPartZone runat="server" Title="<%$Resources:cms,WebPartZoneTitle_TopRight%>" ID="TopRightRow" />
						</div>
					</div>
					<div class="ntm_PL_Foot">
						<WebPartPages:WebPartZone runat="server" Title="<%$Resources:cms,WebPartZoneTitle_Footer%>" ID="Footer"/>
					</div>
				</div>
				<div class="ms-descriptiontext ewiki-margin">
					 <SharePoint:FormattedString FormatText="<%$Resources:wss,wikidiff_lastmodifiedby%>" runat="server">
						<SharePoint:FormField id="modifiedFieldCtrl" ControlMode="Display" FieldName="Modified" runat="server"/>
						<SharePoint:UserField id="editorFieldCtrl" ControlMode="Display" FieldName="Editor" runat="server"/>
					 </SharePoint:FormattedString>
					 <PublishingWebControls:EditPageHyperLink id="editPageLink" Text="<%$Resources:cms,enterwiki_editpage%>" runat="server"/>
				</div>
	</div>
</asp:Content>
