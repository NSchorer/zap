﻿<%@ Page language="C#"   Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=16.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register tagprefix="WebControls" namespace="Microsoft.SharePoint.Publishing.WebControls" assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SearchWC" Namespace="Microsoft.Office.Server.Search.WebControls" Assembly="Microsoft.Office.Server.Search, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePointPortalControls" Namespace="Microsoft.SharePoint.Portal.WebControls" Assembly="Microsoft.SharePoint.Portal, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Taxonomy" Namespace="Microsoft.SharePoint.Taxonomy" Assembly="Microsoft.SharePoint.Taxonomy, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceholderID="PlaceHolderAdditionalPageHead" runat="server">
	<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %>" runat="server"/>
	<link rel="stylesheet" type="text/css" href="/_layouts/15/styles/ZAP/pl_seite.css" />
	<link rel="stylesheet" type="text/css" href="/_layouts/15/styles/ZAP/pl_homepage.css" />
	<PublishingWebControls:EditModePanel runat="server">
		<!-- Styles for edit mode only-->
		<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/editmode15.css %>"
			After="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %>" runat="server"/>
        <link rel="stylesheet" type="text/css" href="/_layouts/15/styles/ZAP/pl_editmode.css" />
	</PublishingWebControls:EditModePanel>
	<SharePointWebControls:FieldValue id="PageStylesField" FieldName="HeaderStyleDefinitions" runat="server"/>
</asp:Content>

<asp:Content ContentPlaceHolderId="PlaceHolderPageTitle" runat="server">
	<SharePoint:ProjectProperty Property="Title" runat="server"/> - <SharePointWebControls:FieldValue ID="FieldValue1" FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitleInTitleArea" runat="server">
	<span id="NTM_MyPageTitle"><SharePointWebControls:FieldValue ID="FieldValue2" FieldName="Title" runat="server" /></span>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderTitleBreadcrumb" runat="server"></asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderLeftNavBar" runat="server"></asp:Content>
<asp:Content ContentPlaceholderID="NTMPHHomeTop" runat="server">
	<table width="100%"><tr><td><div class="ntm_PHHomeTop">
			<WebPartPages:WebPartZone runat="server" Title="Zone oben - Zitate" ID="HomeTopZone" />
	</div></td></tr></table>
</asp:Content>

<asp:Content ContentPlaceholderID="PlaceHolderMain" runat="server">

	<div class="ntm_PL ntm_PLHomepage">
		<PublishingWebControls:EditModePanel ID="EditModePanel2" runat="server" CssClass="edit-mode-panel title-edit">
			<p><SharePointWebControls:TextField ID="TextFieldTitle" runat="server" FieldName="Title"/></p>
		</PublishingWebControls:EditModePanel>
		
		<div class="ntm_PLCont">

			<div class="ntm_PL_Main">

				<div class="ntm_RichTextCont"><PublishingWebControls:RichHtmlField ID="RichHtmlField2" FieldName="PublishingPageContent" HasInitialFocus="True" MinimumEditHeight="200px" runat="server"/></div>

				<div class="ntm_PLWPZ">
					<div class="ntm_Table ntm_HomeZeile1">
						<div class="ntm_PL_ZweiDrittelL">
							<div class="ntm_HomeZeile1">
								<WebPartPages:WebPartZone runat="server" Title="2/3 Links Oben" ID="TopLeftRow" />
							</div>
						</div>
						<div class="ntm_PL_1_3R">
							<div class="ntm_HomeZeile1">
								<WebPartPages:WebPartZone runat="server" Title="1/3 Rechts Oben" ID="TopRightRow" />
							</div>
						</div>
					</div>
					<div class="ntm_PL_1_1 ntm_HomeZeile2">
						<WebPartPages:WebPartZone runat="server" Title="Ganze Breite" ID="TopZone" />
					</div>
					<div class="ntm_Table ntm_HomeZeile3">
						<div class="ntm_PL_1_2">
							<div class="ntm_HomeZeile1">
								<WebPartPages:WebPartZone runat="server" Title="1/2 Links Unten" ID="LeftRow" />
							</div>
						</div>
						<div class="ntm_PL_2_2">
							<div class="ntm_HomeZeile1">
								<WebPartPages:WebPartZone runat="server" Title="2/2 Rechts Unten" ID="RightRow" />
							</div>
						</div>
					</div>

					<div class="ntm_PL_Foot">
						<WebPartPages:WebPartZone runat="server" Title="<%$Resources:cms,WebPartZoneTitle_Footer%>" ID="Footer"/>
					</div>
				</div>

			</div>
			
		</div>

		<PublishingWebControls:editmodepanel runat="server" id="editmodepanelPageInfos" CssClass="edit-mode-panel">
			<div>
				Created: <SharePointWebControls:FieldValue FieldName="Created" runat="server" /><br />
				Author: <SharePointWebControls:FieldValue FieldName="Author" runat="server" /><br />
				Last Modified: <SharePointWebControls:FieldValue FieldName="Modified" runat="server" /><br />
				Last Modified by: <SharePointWebControls:FieldValue FieldName="Editor" runat="server" /><br />
				Pagelayout: <SharePointWebControls:FieldValue FieldName="PublishingPageLayout" runat="server" /><br />
				Content Type: <SharePointWebControls:FieldValue FieldName="ContentType" runat="server" /><br />
				Version: <SharePointWebControls:FieldValue FieldName="Version" runat="server" /><br />
			</div>
		</PublishingWebControls:EditModePanel>

	</div>
	<script type="text/javascript">
	$(document).ready(function () {
		SetWPTitleIconsHome();
	});
	</script>
</asp:Content>
