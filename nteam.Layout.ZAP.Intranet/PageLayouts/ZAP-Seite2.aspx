﻿<%@ Page language="C#"   Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=16.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register tagprefix="WebControls" namespace="Microsoft.SharePoint.Publishing.WebControls" assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceholderID="PlaceHolderAdditionalPageHead" runat="server">
	<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %>" runat="server"/>
	<link rel="stylesheet" type="text/css" href="/_layouts/15/styles/ZAP/pl_seite.css" />
	<PublishingWebControls:EditModePanel runat="server">
		<!-- Styles for edit mode only-->
		<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/editmode15.css %>"
			After="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %>" runat="server"/>
        <link rel="stylesheet" type="text/css" href="/_layouts/15/styles/ZAP/pl_editmode.css" />
	</PublishingWebControls:EditModePanel>
	<SharePointWebControls:FieldValue id="PageStylesField" FieldName="HeaderStyleDefinitions" runat="server"/>
</asp:Content>

<asp:Content ContentPlaceHolderId="PlaceHolderPageTitle" runat="server">
	<SharePoint:ProjectProperty Property="Title" runat="server"/> - <SharePointWebControls:FieldValue ID="FieldValue1" FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderPageTitleInTitleArea" runat="server"></asp:Content>

<asp:Content ContentPlaceHolderId="PlaceHolderTitleBreadcrumb" runat="server"> <SharePointWebControls:ListSiteMapPath ID="ListSiteMapPath1" runat="server" SiteMapProviders="CurrentNavigationSwitchableProvider" RenderCurrentNodeAsLink="false" PathSeparator="" CssClass="s4-breadcrumb" NodeStyle-CssClass="s4-breadcrumbNode" CurrentNodeStyle-CssClass="s4-breadcrumbCurrentNode" RootNodeStyle-CssClass="s4-breadcrumbRootNode" NodeImageOffsetX=0 NodeImageOffsetY=289 NodeImageWidth=16 NodeImageHeight=16 NodeImageUrl="/_layouts/15/images/fgimg.png?rev=23" HideInteriorRootNodes="true" SkipLinkText="" /> </asp:Content>


<asp:Content ContentPlaceholderID="PlaceHolderMain" runat="server">

	<div class="ntm_PL ntm_PLSeite">
		<PublishingWebControls:EditModePanel ID="EditModePanel2" runat="server" CssClass="edit-mode-panel title-edit">
			<p>
                <div class="ntm_Wichtig">Änderung des Seitennamens<br />
                    Seiten sollten einen logischen Namen besitzen. Verwenden Sie bitte keine Sonderzeichen oder Umlaute im Seitennamen, sondern nur die Zeichen A-Z, a-z, 0-9. Statt Leerzeichen im Dokumentennamen bitte einen Bindestrich verwenden.<br />
                    Eine Seite mit dem Namen "default.aspx" bitte <u>nicht</u> umbenennen!
                </div>
                <SharePoint:FieldLabel ID="FieldLabelFileName" FieldName="FileLeafRef" runat="server"/><SharePoint:FileField id="PageNameInEditMode" FieldName="FileLeafRef" DisableInputFieldLabel="true" runat="server"/></p>
			<p><SharePointWebControls:TextField ID="TextFieldTitle" runat="server" FieldName="Title"/></p>
			<p><SharePointWebControls:NoteField ID="NFDescription" FieldName="Comments" InputFieldLabel="Einleitungstext/ Teaser für Übersichten (Startseite und Archiv), Tooltip in der Navigation" runat="server"></SharePointWebControls:NoteField></p>
		</PublishingWebControls:EditModePanel>
		
		<div class="ntm_PLCont">

			<div class="ntm_PL_Main">
				<h1><SharePointWebControls:FieldValue ID="FieldValue2" FieldName="Title" runat="server" /></h1>
				<div class="ntm_Table">
					<div class="ntm_PL_1_3L">
						<PublishingWebControls:RichHtmlField ID="RichHtmlFieldL1" FieldName="fullhtmlm" HasInitialFocus="False" MinimumEditHeight="200px" runat="server"/>
						<WebPartPages:WebPartZone runat="server" Title="Links Mitte" ID="CenterLeftColumn" />
						<PublishingWebControls:RichHtmlField ID="RichHtmlFieldL2" FieldName="fullhtmlr" HasInitialFocus="False" MinimumEditHeight="200px" runat="server"/>
						<WebPartPages:WebPartZone runat="server" Title="Links unten" ID="CenterRightColumn" />
					</div>
					<div class="ntm_PL_ZweiDrittelR">
						<PublishingWebControls:RichHtmlField ID="RichHtmlFieldMain" FieldName="PublishingPageContent" HasInitialFocus="True" MinimumEditHeight="200px" runat="server"/>
						<WebPartPages:WebPartZone runat="server" Title="Rechts unten" ID="CenterColumn" />
					</div>
				</div>
				<div class="ntm_PLWPZ">
					<div class="ntm_PL_1_1">
						<WebPartPages:WebPartZone runat="server" Title="Top" ID="TopZone" />
					</div>
					<div class="ntm_Table">
						<div class="ntm_PL_1_2">
							<WebPartPages:WebPartZone runat="server" Title="<%$Resources:cms,WebPartZoneTitle_TopLeft%>" ID="TopLeftRow" />
						</div>
						<div class="ntm_PL_2_2">
							<WebPartPages:WebPartZone runat="server" Title="<%$Resources:cms,WebPartZoneTitle_TopRight%>" ID="TopRightRow" />
						</div>
					</div>
					<div class="ntm_PL_Foot">
						<WebPartPages:WebPartZone runat="server" Title="<%$Resources:cms,WebPartZoneTitle_Footer%>" ID="Footer"/>
					</div>
				</div>

				<div style="visibility:hidden;height:0px;"> x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x </div>
				
			</div>
			
		</div>

		<PublishingWebControls:editmodepanel runat="server" id="editmodepanelPageInfos" CssClass="edit-mode-panel">
			<div>
				Created: <SharePointWebControls:FieldValue FieldName="Created" runat="server" /><br />
				Author: <SharePointWebControls:FieldValue FieldName="Author" runat="server" /><br />
				Last Modified: <SharePointWebControls:FieldValue FieldName="Modified" runat="server" /><br />
				Last Modified by: <SharePointWebControls:FieldValue FieldName="Editor" runat="server" /><br />
				Pagelayout: <SharePointWebControls:FieldValue FieldName="PublishingPageLayout" runat="server" /><br />
				Content Type: <SharePointWebControls:FieldValue FieldName="ContentType" runat="server" /><br />
				Version: <SharePointWebControls:FieldValue FieldName="Version" runat="server" /><br />
				LikesCount: <SharePointWebControls:FieldValue FieldName="LikesCount" runat="server" /><br />
			</div>
		</PublishingWebControls:EditModePanel>

	</div>

</asp:Content>
