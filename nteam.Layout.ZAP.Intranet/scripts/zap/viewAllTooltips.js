// einbinden mit: ~sitelayouts/scripts/bistum/viewAllTooltips.js

(function () {
    //(window.jQuery || document.write('<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-1.10.0.min.js"><\/script>'));
    //document.write('<link href="/_layouts/15/nteam/style.css" rel="stylesheet" />');
    document.write('<style type="text/css">table.ms-listviewtable {display:none;} .ntm_VCTooltipMehr{padding: 12px 0px;} .ntm_VCTooltipTitle.ntm_IBTitle {padding-bottom: 24px;} .ntm_MehrLink {font-size: 1.1em;display: inline-block; padding: 1px 9px 2px 22px; background: transparent url("/_layouts/15/images/bistum/svg/link.svg") no-repeat left 0px;}	</style>');

    var itemCtx = {};
    itemCtx.Templates = {};

//	itemCtx.Templates.Header = "";
	itemCtx.Templates.Item = MyItemOverride;
//	itemCtx.Templates.Footer = "";

    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(itemCtx);

})();


function MyItemOverride(ctx) {
    var itemtitle = ctx.CurrentItem["Title"];
    var myUrl = ctx.CurrentItem["URL"];
	//var myClearUrl = myUrl.split(";")[0];
    var myID = ctx.CurrentItem["ID"];
    var myBeschreibung = ctx.CurrentItem.Beschreibung;
    var myHilfreich = ctx.CurrentItem.Hilfreich;
	var myTextHilfreich = "";
	if(myHilfreich != "" && myHilfreich >= 1) {
		myTextHilfreich = "(" + myHilfreich + " Personen fanden das hilfreich)";
	}

    return "<div class='ntm_ViewContTooltip ms-rteElement-Box1'>" + 
			"<div class='ntm_VCTooltipTitle ntm_IBTitle'>" +
			"<a href='" + myUrl + "?showId=" + myID +
			"' title='Aufruf der Seite mit dem Tooltip'>" + itemtitle +
			"</a></div>" +
            "<div class='ntm_VCTooltipText'>" + myBeschreibung +
			"</div>" + 
			"<div class='ntm_VCTooltipMehr'><a href='" + myUrl + "?showId=" + myID +
			"' title='Aufruf der Seite mit dem Tooltip' class='ntm_MehrLink'>Ansehen</a> " + myTextHilfreich + "</div>" +
            "</div>";
}
