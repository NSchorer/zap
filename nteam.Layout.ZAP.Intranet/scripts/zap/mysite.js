$(document).ready(function () {
	//$("#sideNavBox a:contains('Personen')").closest("li").hide();
    GlobalNavItemFly();
    SetGlobalNavIcons();
	NavItemToggle();
	GlobalNavItemToggle();
	HideMySiteLinks();
	
	
	
    // mobile Navigation ein-/ ausblenden
    $("#NTM_BtnMobilNav").click(function () {
        $('#NTM_MainNavCont').slideToggle(); //DeltaTopNavigation  NTM_MobileNavCont
        $('#s4-workspace').animate({ scrollTop: 0 }, duration);
        $(this).toggleClass("ntm_BtnMobilNavActive");
    });
    // mobile aktuelle linke Navigation ein-/ ausblenden
    $("#NTM_BtnMobilQuicklaunch").click(function () {
        $('#sideNavBox').slideToggle();
        $('#s4-workspace').animate({ scrollTop: 0 }, duration);
        $(this).toggleClass("ntm_BtnMobilQuicklaunchActive");
    });
});



// mobile Navigation Item Toggle
function NavItemToggle() {
    var WindowWidth = $(window).width(); // Returns width of browser viewport
    //alert(NavContent);	
    if ((WindowWidth <= 1024)) {
        $('#DeltaTopNavigation ul.root.ms-core-listMenu-root.static ul li:has(ul) > a').append('<div class="ntm_OpenIco"><img src="/_layouts/15/images/blank.gif" alt=""></div>');
        $('#DeltaTopNavigation ul.root.ms-core-listMenu-root.static ul.dynamic').attr('style', 'display:none;');
        // Find li has ul and add Toggle Function
        $('#DeltaTopNavigation .ntm_OpenIco')
            .on(
                'click',
                function (e) {
                    e.preventDefault();

                    if ($(this).parent().parent().attr('id') != 'NTM_NavItemTools') {

                        $(this).parent().parent()
                            .toggleClass('ntm_Collapsed ntm_Expanded')
                            .children('ul')
                            .slideToggle();

                    }

                }
            )
            .parent().parent().addClass('ntm_Collapsed')
            ;
    }
    else {
        //
    }
}

// mobile Navigation GlobalNav Item Toggle
function GlobalNavItemToggle() {
    var WindowWidth = $(window).width(); // Returns width of browser viewport
    //alert(NavContent);	
    if ((WindowWidth <= 1024)) {
        $('#DeltaTopNavigation ul.GlobalNav li:has(ul) > a').append('<div class="ntm_OpenIco"><img src="/_layouts/15/images/blank.gif" alt=""></div>');
        $('#DeltaTopNavigation ul.GlobalNav ul.GlobalNav').attr('style', 'display:none;');
        // Find li has ul and add Toggle Function
        $('#DeltaTopNavigation .ntm_OpenIco')
            .on(
                'click',
                function (e) {
                    e.preventDefault();

                    if ($(this).parent().parent().attr('id') != 'NTM_NavItemTools') {

                        $(this).parent().parent()
                            .toggleClass('ntm_Collapsed ntm_Expanded')
                            .children('ul')
                            .slideToggle();

                    }

                }
            )
            .parent().parent().addClass('ntm_Collapsed')
            ;
    }
    else {
        //
    }
}
function GlobalNavItemFly() {
    $('#DeltaTopNavigation ul.GlobalNav ul.GlobalNav li:has(ul) > a').addClass('dynamic-children');
}
// Globale Navigation Navigationselemente in Icons wandeln
function SetGlobalNavIcons() {
    try {
		$("#NTM_MainNavCont span.GlobalNavContainer > ul.GlobalNav > li > a span:contains('Blauer')").parent().parent().addClass("ntm_IcoBlauerPlan");
        $("#NTM_MainNavCont ul.GlobalNav > li > a span:contains('Meine Seite')").parent().parent().addClass("ntm_IcoMyPage");
        $("#NTM_MainNavCont ul.GlobalNav > li > a span:contains('Persönliche')").parent().parent().addClass("ntm_IcoMyPage");

    } catch (Err) { }
}
function HideMySiteLinks() {
    try {
		$("#DeltaPlaceHolderLeftNavBar a span:contains('Blog')").parent().parent().addClass("ntm_Hide");
		$("#DeltaPlaceHolderLeftNavBar a[title*='Blog']").parent().addClass("ntm_Hide");
		$("#DeltaPlaceHolderLeftNavBar a[title*='Apps']").parent().addClass("ntm_Hide");
    } catch (Err) { }
}
