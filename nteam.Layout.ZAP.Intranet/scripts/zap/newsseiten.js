﻿// ~sitecollectionlayouts/scripts/ZAP/newsseiten.js
(function () {
    (window.jQuery || document.write('<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-1.10.0.min.js"><\/script>'));
    //document.write('<link href="/_layouts/15/nteam/style.css" rel="stylesheet" />');

    document.write('<style type="text/css"></style>');

    var itemCtx = {};
    itemCtx.Templates = {};

    itemCtx.Templates.Header = "&nbsp;";
    itemCtx.Templates.Item = MyItemOverride;
    itemCtx.Templates.Footer = "";

    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(itemCtx);

})();


function MyItemOverride(ctx) {
    var itemtitle = ctx.CurrentItem["Title"];
    var myrollupimage = ctx.CurrentItem.PublishingRollupImage;
    var pagecontent = ctx.CurrentItem.PublishingPageContent;
    var articledate = ctx.CurrentItem.ArticleStartDate;
    var modified = ctx.CurrentItem.Modified;
    var newsort = ctx.CurrentItem.Inhaltsthema;
    var teaser = ctx.CurrentItem.Comments;
    var target = "_self";
    var mehrlink = "";
    var newsclass = "ntm_News";
    var myimage = "";
    var trenner = "";

    if (newsort != "") {
		if(newsort.indexOf(';#') > -1){
			newsort = newsort.split(";#").join(" ");
		}
    }
    else { }

	
    if (articledate != "" && articledate !== "undefined") {
        articledate = articledate;
    }
    else if (modified != "" && modified !== "undefined") {
		articledate = modified + "&#160;Uhr";
	}
	else {
		articledate = "";
	}
    if (articledate != "" && newsort != "") {
        trenner = "&#160; - &#160;";
    }
    else { }

	itemurl = ctx.CurrentItem.FileRef;
	if (pagecontent.length >= 58) {
		mehrlink = "<a href=\"" + itemurl + "\" target=\"" + target + "\" title=\"" + itemtitle + "\" class=\"ntm_MehrLink\">Artikel lesen</a>";
	}
	else { }

	if (typeof myrollupimage !== "undefined") {
        try {
            myrollupimage = myrollupimage.toLowerCase();
            myrollupimage = myrollupimage.replace(/&quot;/g, "\"");
        } catch (Err) { }
        var myrollupimageArr = myrollupimage.split("\"");
        if (myrollupimageArr.length >= 8) {
            var myrollupimageUrl = myrollupimageArr[7];

            myimage = "<div class=\"ntm_newsImg\"><a href=\"" + itemurl + "\" target=\"" + target + "\" title=\"" + itemtitle + "\"><img src=\"" + myrollupimageUrl + "\"/></a></div>";
        }
        else { }
    }
    else { }

    return "<div class=\"" + newsclass + "\">" + myimage +
            "<div class=\"ntm_NewsDate\">" + articledate + trenner + newsort + "</div>" +
            "<div class=\"ntm_NewsTitle\"><a href=\"" + itemurl + "\" target=\"" + target + "\" title=\"" + itemtitle + "\">" + itemtitle + "</a></div>" +
            "<div class=\"ntm_NewsText\">" + teaser + mehrlink +
            "</div>" +
            "<div class=\"clearfix\"></div>" +
        "</div>";
}
