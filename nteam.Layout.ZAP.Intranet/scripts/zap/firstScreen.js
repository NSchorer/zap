﻿(function () {
    //(window.jQuery || document.write('<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-1.10.0.min.js"><\/script>'));
	
	// script firstScreen.js
	// SP List FirstScreen: Title (single line of text), sequence (Number), elem (Multiline of text - Rich text)
	// Einbindung: <script type="text/javascript" src="http://www.nteam.de/Style%20Library/scripts/firstScreen.js"></script>
	// Einbindung: <script type="text/javascript" src="/_layouts/15/scripts/ZAP/firstScreen.js"></script><link rel="stylesheet" type="text/css" href="/_layouts/15/styles/ZAP/FirstScreen.css">
	// EDIT MODE: ?EditFirstScreen=true
	
	

    //Determine display mode of sharepoint page
    var form = document.forms[MSOWebPartPageFormName];
    var inDesignMode = form && form.MSOLayout_InDesignMode && form.MSOLayout_InDesignMode.value;
    var wikiInEditMode = form && form._wikiPageMode && form._wikiPageMode.value;
    var inEditMode = inDesignMode || wikiInEditMode;
    if (!inEditMode) {
		SP.SOD.executeFunc("SP.js", "SP.ClientContext", function() {
			window.nteam = window.nteam || {};
			var nteam = window.nteam;
			
			
			// URL zum Web mit der Liste "FirstScreen"
			nteam.siteURL = 'http://zap.afd-dbtg.frak';
			//nteam.siteURL = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + ''; 
			nteam.listURL = 'FirstScreen';
			nteam.defaultTitle = 'There is a place for the tooltipp content';
			nteam.isOverlayActive = false;
			nteam.currSequenceNumber = 1;
			nteam.currElem = 0;
			
			getCurrentSequenceNumber();
			
			console.log('firstScreen activated');
			if(GetUrlKeyValue('EditFirstScreen') == 'true'){
				
				var prevElement = null;
				document.addEventListener('mousemove',
					function(e){
						var elem = $(e.target) || $(e.srcElement);
						if (prevElement!= null) 
						{
							prevElement.removeClass('mouseOnItem');
							prevElement.off('click');
						}
						elem.addClass('mouseOnItem');
						elem.on('click',getElemInformation);
						prevElement = elem;
					},true);
			} else {
				var itemsArr = [];
				var currItem = 0;
				
				$('body').append('<div class="ntm_showFirstScreen ntm_grayGradient">?</div>')
				
				$(".ntm_showFirstScreen").click(function(){
					setFirstScreenItems(itemsArr, currItem);
					$(this).hide();
				});	
				
				getListItems(itemsArr);
			}	
		});
		// Display Mode   
		(window.jQuery || document.write('<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-1.10.0.min.js"><\/script>'));
		document.write('<link rel="stylesheet" type="text/css" href="/_layouts/15/styles/ZAP/FirstScreen.css">');
	}
})();



function addOverlay(){
	var overlayHeight = $('body').innerHeight();
	var overlayWidth = $('body').innerWidth();
	$('body').append('<div class="ntm_FirstScreenLayer" style="width:' + overlayWidth + 'px; height:' + overlayHeight + 'px; opacity: 0.6; position: absolute; top: 0; left: 0; z-index: 7777;"></div>');
	nteam.isOverlayActive = true;
}

function getListItems(itemsArr){
	var siteUrl = nteam.siteURL;
    $.ajax({
        url: siteUrl + "/_api/lists/getbytitle('" + nteam.listURL + "')/items?orderby=sequence asc",
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {  
            if (data.d.results.length > 0 ) {
                jQuery.each(data.d.results, function( i, val ) {
					itemsArr.push({'sequence': val.sequence, 'elem': val.elem, 'Title': val.Title});
				});				
				itemsArr.sort(function(a, b){
					var a1= a.sequence, b1= b.sequence;
					if(a1== b1) return 0;
					return a1> b1? 1: -1;
				});
            } 
        },
        error: function (data) {
           console.log("Error: "+ data);
		}
    });
}

function setFirstScreenItems(itemsArr, currItem){
	var elem = $(itemsArr[currItem].elem);
	// scroll window to current element
	if(elem.length > 0){
		$('#s4-workspace').animate({ scrollTop: $(elem).position().top - 300 }, 500);
	} else {
		$('#s4-workspace').animate({ scrollTop: $("body").position().top - 300 }, 500);
	}
	

	
	setTimeout(function(){ 
		if(nteam.isOverlayActive == false){
			addOverlay();
		}
		
		// var elem = $('#NTM_MyPageTitle');
		if(elem.length > 0){
			var elemOffset = elem.offset();
			// Tooltip above
			var offsetTop = elemOffset.top - 10;
			var offsetLeft = elemOffset.left - 10;
			var itemHeight = elem.innerHeight() + 20;
			var itemWidth = elem.innerWidth() + 20;
			if($('.ntm_itemOverlay').length == 0){
				$('body').append('<div class="ntm_itemOverlay" style="position:absolute; top:' + offsetTop + 'px; left:' + offsetLeft  + 'px; height:' + itemHeight + 'px; width:' + itemWidth + 'px; z-index: 7778; background-color: rgba(255, 255, 255, 0.9); border-radius: 6px;"></div>');
			} else {
				$('.ntm_itemOverlay').css('top', offsetTop).css('left', offsetLeft).css('height', itemHeight).css('width', itemWidth).show();
			}
			
		}
		
		var hintBoxOffsetTop = '30%';
		var hintBoxOffsetLeft = '50%';
		var hintBoxMarginLeft = -125; //half on itemBoc height
		
		if(elem.length > 0){
			var triangleClass = 'ntm_itemHintTriangle';
			if(offsetTop > 200){
				hintBoxOffsetTop = offsetTop - 170 + 'px';
				hintBoxOffsetLeft = offsetLeft + 10  + 'px';
			} else {
				hintBoxOffsetTop = offsetTop + elem.innerHeight() + 50 + 'px';
				hintBoxOffsetLeft = offsetLeft - 20  + 'px';
				triangleClass = 'ntm_itemHintTriangle bottom';
			}
			
			hintBoxMarginLeft = 0;
		}
		
		var hintBoxHeight = 150;
		var hintBoxWidth = 275;
		
		$('.ntm_itemHintBox').remove();
		
		$('body').append('<div class="ntm_itemHintBox" style="position:absolute; top:' + hintBoxOffsetTop + '; left:' + hintBoxOffsetLeft  + '; height:' + hintBoxHeight + 'px; width:' + hintBoxWidth + 'px; z-index: 7778; background-color: rgba(255, 255, 255, 1); border-radius: 5px; margin-left:' + hintBoxMarginLeft + 'px;"><div class="ntm_itemHintHeader"><div class="ntm_itemHintCounter ntm_grayGradient">' + itemsArr[currItem].sequence + '/' + itemsArr[itemsArr.length-1].sequence + '</div><div class="ntm_itemHintClose ntm_grayGradient">X</div></div><div class="ntm_itemHintText"><div class="ntm_itemHintTextContent">' + itemsArr[currItem].Title + '</div></div><div class="ntm_itemHintButtons"><div class="ntm_btnPrev ntm_grayGradient"><</div><div class="ntm_btnNext ntm_grayGradient">></div></div><div class="' + triangleClass + '"></div></div>');
		
		if(itemsArr[currItem].sequence == 1){
			$(".ntm_btnPrev").addClass('disabled');
		}
		if(itemsArr[currItem].sequence == itemsArr[itemsArr.length-1].sequence){
			$(".ntm_btnNext").addClass('disabled');
		}
		if(elem.length > 0){
			if(elem.css('position') != "absolute") {
				$(elem).addClass('highlightedItemPosition');
			} 
			$(elem).addClass('highlightedItem');
			$(".ntm_itemHintTriangle").css('display', 'block');
		}
		
		
		$(".ntm_itemHintClose").click(function(){
			$(".ntm_FirstScreenLayer").hide();
			$(".ntm_itemOverlay").hide();
			$(".ntm_itemHintBox").hide();
			$(".highlightedItem").removeClass("highlightedItem").removeClass("highlightedItemPosition");
			$(".ntm_showFirstScreen").show();
			nteam.isOverlayActive = false;
		});	
		
		$(".ntm_btnNext:not(.disabled)").click(function(){
			showNextItem(itemsArr, currItem);
		});
		$(".ntm_btnPrev:not(.disabled)").click(function(){
			showPrevItem(itemsArr, currItem);
		});		
		
	}, 600);

}

function showNextItem(itemsArr, currItem) {
	$(".ntm_itemOverlay").hide();
	$(".ntm_itemHintBox").hide();
	$(".highlightedItem").removeClass("highlightedItem").removeClass("highlightedItemPosition");
	
	currItem++;
	setFirstScreenItems(itemsArr, currItem);
}

function showPrevItem(itemsArr, currItem) {
	$(".ntm_itemOverlay").hide();
	$(".ntm_itemHintBox").hide();
	$(".highlightedItem").removeClass("highlightedItem").removeClass("highlightedItemPosition");
	
	currItem--;
	setFirstScreenItems(itemsArr, currItem);
}

function getElemInformation(e) {
    // Event Workaround für Internet Explorer
	var evt = window.event || e;
	if (!evt.target) { //if event obj doesn't support e.target, presume it does e.srcElement
		evt.target = evt.srcElement; //extend obj with custom e.target prop
	}
	
	var elementId = null;
	if(evt.target.id !== "") {
		elementId = '#' + evt.target.id;
		var path = elementId;
	} else {
		var closestId = $(evt.target).closest('[id]')[0];
		elementId = closestId.id;
		// show full element path
		var rightArrowParents = [];
		$(this).parents().addBack().not('html').each(function() {
		   	var entry = this.tagName.toLowerCase();
		    if (this.id) {
		        entry += "#" + this.id;
		    }
		    if (this.className) {
		        entry += "." + this.className.replace(/ /g, '.');
		    }
		   	rightArrowParents.push(entry);
		});
		
		// show full element path in one string
		var fullPath = rightArrowParents.toString();
		// n - show position of last element with id in full path, m - show position to cut substring
		var n = fullPath.search(closestId.tagName.toLowerCase() + "#" + closestId.id);
		var m = fullPath.search(".mouseOnItem");
	    var path = fullPath.substring(n, m).replace(/,/g, ' ');
		// -- all exceptions -- 
	    if(path.substr(path.length - 4) == ' img'){
	    	path = path.substr(0, (path.length - 4));
	    }
		// new
		if($(path).length > 1){
			if($(path).parent().length > 1){
				jQuery.each($(path), function( i, val ) {
					if(val.className.indexOf('mouseOnItem') > -1){
						var childNumber = i + 1;
						path = path.substring(0, path.lastIndexOf(' ')) + ':nth-child(' + childNumber + ')' + path.substring(path.lastIndexOf(' '), path.length);
					}
				});
			} else {
				jQuery.each($(path), function( i, val ) {
					if(val.className.indexOf('mouseOnItem') > -1){
						var childNumber = i + 1;
						path = path + ':nth-child(' + childNumber + ')';
					}
				});
			}	
		}
		
		if(path.substr(path.length - 9) == '.clearfix'){
	    	path = path.substr(0, (path.length - 9));
	    }
	}

	createListItem(elementId, path);

	return false;
}

function createListItem(elementId, path) {
    var clientContext = new SP.ClientContext(nteam.siteURL);
    var oList = clientContext.get_web().get_lists().getByTitle(nteam.listURL);   
    var itemCreateInfo = new SP.ListItemCreationInformation();
    var oListItem;
    var data = {};
    
    data.oListItem = oList.addItem(itemCreateInfo);
        
	data.oListItem.set_item('Title', nteam.defaultTitle);
	data.oListItem.set_item('elem', path);
	data.oListItem.set_item('sequence', nteam.currSequenceNumber);
	        
    data.oListItem.update();
    clientContext.load(data.oListItem);
    clientContext.executeQueryAsync(Function.createDelegate(data, onQuerySucceeded), Function.createDelegate(data, onQueryFailed)); 
}

/* ==================================================================
 * onQuerySucceeded
 * ------------------------------------------------------------------
 * Redirects to edit saved record on a SP list
 * ------------------------------------------------------------------
 * ================================================================== */
function onQuerySucceeded() {
	var data = this;
    window.location = nteam.siteURL +'/Lists/' + nteam.listURL + '/EditForm.aspx?ID=' + data.oListItem.get_id();   
}

function onQueryFailed(sender, args) {
    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
}

function getCurrentSequenceNumber() {
	var siteUrl = nteam.siteURL;
    $.ajax({
        url: siteUrl + "/_api/lists/getbytitle('" + nteam.listURL + "')/items",
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {  
            if (data.d.results.length > 0 ) {
                jQuery.each(data.d.results, function( i, val ) {
					if(val.sequence > nteam.currSequenceNumber) {
						nteam.currSequenceNumber = val.sequence;
					} 
				});
				nteam.currSequenceNumber++;
            } 
        },
        error: function (data) {
           console.log("Error: "+ data);
		}
    });
	
	return true;
}