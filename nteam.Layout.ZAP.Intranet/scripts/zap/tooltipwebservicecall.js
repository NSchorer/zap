function callService()
{
    var data = { 'listName': 'Tooltips', 'listURL':'http://zap.afd-dbtg.frak/Lists', 'hilfreich':false, 'itemID':51 };

	// /_layouts/15/services/nteam/Tooltip.WS/
	var webserviceURL = '/_vti_bin/nteam/Tooltip.WS/Tooltip.asmx';
	var method = 'countHelpful';
		
	$.ajax({
		url: webserviceURL + '/' + method,
		type: "POST",
		dataType: "json",
		contentType: "application/json; charset=utf-8",
		data: data ? JSON.stringify(data) : undefined,
		success: function (data) {
			var data = data ? data.d : data;
			console.log('success\n data: ' + data);
		},
		error: function (err) {
			console.log('Bei der Verarbeitung ist ein Fehler aufgetreten\n\n' + err.status + " " + err.statusText);
		}
	});
}