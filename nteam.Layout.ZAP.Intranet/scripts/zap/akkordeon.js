﻿// ~sitecollectionlayouts/scripts/ZAP/akkordeon.js
/*
für eine bestimmte Listenansicht oder abweichende Spalten konfigurieren
<script>
var nteam = nteam || {};
nteam.viewIdents = nteam.viewIdents || [];

nteam.viewIdents.push( { 
  name:'akkordeon', 
  view:'{7F234B66-1AD5-4177-9921-D3C312B1AFC4}', 
  akkordeonField_Title: 'Title',
  akkordeonField_Description:'Antwort',
} );
</script>

*/


(function () {
	var viewIdIdentifier = 'akkordeon';
	
	window.nteam = window.nteam || {};
	var nteam = window.nteam;
	
	//(window.jQuery || document.write('<script src="//ajax.aspnetcdn.com/ajax/jquery/jquery-1.10.0.min.js"><\/script>'));
	//document.write('<link href="/_layouts/15/nteam/style.css" rel="stylesheet" />');
	//document.write('<style type="text/css"></style>');

	
	// JSLink-ListView-Context überschrieben
	var itemCtx = {};
	itemCtx.Templates = {
		Header: emptyHeaderOverride1,
		Item: AkkordeonItemOverride/*,
		Footer: emptyFooterOverride1*/
	};
	SPClientTemplates.TemplateManager.RegisterTemplateOverrides(itemCtx);
	
	
	// Globale Variablen
	//var lastRow = -1;
	//var curpos = 0;
	
	// Header Rendering
	function emptyHeaderOverride1(ctx) {
		// Überprüfen, ob das überhaupt durch uns gerendert werden soll.. wenn nicht, dann über RenderItemTemplate das Default-Rendering anschmeissen
		if(!getViewSettings(ctx).shouldOverride) {
			return RenderHeaderTemplate(ctx);
		}
		return "";
	}
	
	// Footer Rendering
	function emptyFooterOverride1(ctx) {
		// Überprüfen, ob das überhaupt durch uns gerendert werden soll.. wenn nicht, dann über RenderItemTemplate das Default-Rendering anschmeissen
		if(!getViewSettings(ctx).shouldOverride) {
			return RenderFooterTemplate(ctx);
		}
		return "";
	}
	
	// Item Rendering
	function AkkordeonItemOverride(ctx) {
		// Überprüfen, ob das Item überhaupt durch uns gerendert werden soll.. wenn nicht, dann über RenderItemTemplate das Default-Rendering anschmeissen
		var viewSettings = getViewSettings(ctx);
		if(!viewSettings.shouldOverride) {
			return RenderItemTemplate(ctx);
		}
		
		// lokale Variablen
		var viewID = ctx.view; // ID der View 
		var wpID = ctx.wpq // ID des Webparts zB WPQ2
		
		var id = ctx.CurrentItem.ID;
		var itemtitle = ctx.CurrentItem[viewSettings.akkordeonField_Title];
		var mydescription = ctx.CurrentItem[viewSettings.akkordeonField_Description];		
		
		var result = "";
	
		// Den Inhalt rendern
			result += 	"<h3 class='ntm_AkkordeonTitle ntm_AkkordeonList'>"
							+"<div class='ntm_OpenIco'><img src='/_layouts/15/images/blank.gif' /></div>"
							+ itemtitle
						+"</h3>"
						+"<div class='ntm_AkkordeonText ntm_AkkordeonList'>"
							+  mydescription
						+"</div>";
		
		
		return result;
	}

	function shouldOverride(ctx) {
		if( nteam && nteam.viewIdents ) {
			for(var i=0; i<nteam.viewIdents.length; i++) {
				if( nteam.viewIdents[i].name == viewIdIdentifier ) {
					if(nteam.viewIdents[i].view == ctx.view) {
						return true;
					}
				}
			}
			return false;
		} else {
			return true;
		}
	}
	
	function getViewSettings(ctx) {
		var defaultViewSettings = {
			shouldOverride: true,
			akkordeonField_Title: 'Title',
			akkordeonField_Description: 'Antwort',
		};
		
		if( nteam && nteam.viewIdents ) {
			for(var i=0; i<nteam.viewIdents.length; i++) {
				var curr = nteam.viewIdents[i];
				if( curr.name == viewIdIdentifier ) {
					if(curr.view == ctx.view) {
						return {
							shouldOverride: true,
							akkordeonField_Title: curr.akkordeonField_Title || defaultViewSettings.akkordeonField_Title,
							akkordeonField_Description: curr.akkordeonField_Description || defaultViewSettings.akkordeonField_Description
						}
					}
				}
			}
			defaultViewSettings.shouldOverride = false;
		}
		return defaultViewSettings;
	}
})();