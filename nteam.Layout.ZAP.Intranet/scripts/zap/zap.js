/*################################################################*/
/*################## Copyright 2017 nteam GmbH ###################*/
/*################################################################*/

var WindowWidth = $(window).width(); // Returns width of browser viewport

$(window).load(function () {
});

$(document).ready(function () {
    var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
    // URL �nderung nach # Bind the event.
    $(window).on("hashchange", hashchanged);
    // Trigger the event (useful on page load).

    // init simple Lightbox
    //Determine display mode of sharepoint page
    var form = document.forms[MSOWebPartPageFormName];
    var inDesignMode = form && form.MSOLayout_InDesignMode && form.MSOLayout_InDesignMode.value;
    var wikiInEditMode = form && form._wikiPageMode && form._wikiPageMode.value;
    var inEditMode = inDesignMode || wikiInEditMode;
    if (!inEditMode) {
        // Display Mode

        // Flexslider init
        $('.ntm_mainSlider').flexslider({
            controlNav: false,
            smoothHeight: true,
            touch: true,
            animation: "fade",
            pauseOnHover: false,
            slideshowSpeed: 22000
        });
        $('.ntm_IconSlider').flexslider({
            animation: "slide",
            animationLoop: false,
            itemWidth: 120,
            itemMargin: 5,
            minItems: 4,
            maxItems: 9

        });
        /*
                $('.ntm_newsSlider').flexslider({
                    directionNav: true,
                    pauseOnHover: false,
                    touch: true,
                    itemWidth: 360,
                    itemMargin: 8,
                    minItems: 1,
                    maxItems: 3,
                    slideshowSpeed: 9000,
                    animationSpeed: 500
                });*/
        $('.ntm_SliderOne').flexslider({
            directionNav: true,
            pauseOnHover: false,
            controlNav: true,
            touch: true,
            slideshowSpeed: 10000,
            smoothHeight: true
        });

        // News Slider Anzahl abh�ngig von Aufl�sung berechnen
        // store the slider in a local variable
        var $window = $(window),
            flexslider = { vars: {} };
        // check grid size on resize event

        $window.resize(function () {
            var gridSize = getGridSize();
            flexslider.vars.minItems = gridSize;
            flexslider.vars.maxItems = gridSize;
        });
        // tiny helper function to add breakpoints
        function getGridSize() {
            return (window.innerWidth < 600) ? 1 :
                (window.innerWidth < 900) ? 2 : 3;
        }
        $('.ntm_SliderMore').flexslider({
            animation: "slide",
            animationLoop: false,
            itemWidth: 360,
            itemMargin: 3,
            minItems: getGridSize(), // use function to pull in initial value
            maxItems: getGridSize() // use function to pull in initial value
        });


        // End News Slider




        //window.lightbox.init();
        $(function () {
            var $gallery = $('a.image-link').simpleLightbox();
            var $gallery2 = $('img.ms-rteStyle-LightBox, span.ms-rteStyle-LightBox').parent().simpleLightbox({
                //SaveToDisk: true
            });
        });

        try {
            $("#NTM_Gruppierung div.ntm_NewsText, div.ntm_AkkordeonText").attr("style", "display: none;");
        } catch (Err) { }


    }


    var url = window.location.href;
    try { url = url.toLowerCase(); } catch (Err) { }
    var urlArr = url.split("/");


    //Websiteinhalte();
    Recent();
    //SafeRegion();
    activeNavItem();
    KachelnNorm();
    KachelnBig();
    cycleDivs();
    cycleDivSetHeight();
    NavItemToggle();
    GlobalNavItemToggle();
    GlobalNavItemFly();
    SetGlobalNavIcons();



    // Seite drucken
    try {
        $('.ntm_Print').click(function (event) {
            event.preventDefault();
            printPage();
        });
    } catch (Err) { }

    // Seite per Mail empfehlen
    try {
        $('#site_share_buttonPL').click(function () {
            var myURL = window.location.href.split('?')[0];
            myURL = window.location.href.split('#')[0];
            var myTitle = document.title;
            $(location).attr('href', 'mailto:?subject='
                + encodeURIComponent("Empfehlenswert")
                + "&body="
                + encodeURIComponent("Hallo! \r\n\r\nFolgende Seite lohnt es sich anzuschauen:\r\n")
                + encodeURIComponent(myTitle)
                + encodeURIComponent(" (")
                + encodeURIComponent(myURL)
                + encodeURIComponent(") \r\n\r\nViel Spass!")
            );
        });
    } catch (Err) { }

    // mobile Navigation ein-/ ausblenden
    $("#NTM_BtnMobilNav").click(function () {
        $('#NTM_MainNavCont').slideToggle(); //DeltaTopNavigation  NTM_MobileNavCont
        $('#s4-workspace').animate({ scrollTop: 0 }, duration);
        $(this).toggleClass("ntm_BtnMobilNavActive");
    });
    // mobile aktuelle linke Navigation ein-/ ausblenden
    $("#NTM_BtnMobilQuicklaunch").click(function () {
        $('#sideNavBox').slideToggle();
        $('#s4-workspace').animate({ scrollTop: 0 }, duration);
        $(this).toggleClass("ntm_BtnMobilQuicklaunchActive");
    });
    // mobile Suche ein-/ ausblenden
    $("#NTM_BtnMobilSearch").click(function () {
        $('#NTM_SearchCont').slideToggle();
        $('#s4-workspace').animate({ scrollTop: 0 }, duration);
        $(this).toggleClass("ntm_BtnMobilSearchActive");
    });
    // Sucheingabe ein-/ ausblenden
    $("#NTM_SearchCloseBtn, #NTM_SearchNavItem").click(function () {
        $('#NTM_SearchCont').slideToggle();
    });
    // Suche ein-/ ausblenden
    $("#NTM_BtnSearch").click(function () {
        $('#NTM_SearchCont').slideToggle();
        $('#s4-workspace').animate({ scrollTop: 0 }, duration);
        $(this).toggleClass("ntm_BtnSearchActive");
    });
    // Historischen Kalender ein-/ ausblenden
    $(".ntm_HistorischerKalender").click(function () {
        $('#NTM_CtnKB').slideToggle();
    });
    $("#NTM_CtnKB .ntm_close").click(function () {
        $('#NTM_CtnKB').slideToggle();
    });



    // Accordion
    // JS-Link Variante
    try {
        $('#NTM_GroupLevelHold').append($('#NTM_GroupLevel1'));
    } catch (Err) { }

    // Reiter aktiv setzen
    try {
        $('#NTM_GroupLevelHold li a').bind('click', function (e) {
            var contID = $(this).attr("Title");;
            $('#NTM_GroupLevelHold li a').each(function (i) {
                $(this).removeClass("ntm_Active");
                $.each($('.ntm_ReiterCont'), function (index, value) { $(this).attr("class", "ntm_hide ntm_ReiterCont"); });
                //alert(contID);
            });
            $(this).addClass("ntm_Active");
            $("#" + contID).attr("class", "ntm_show ntm_ReiterCont");
        });
    } catch (Err) { }
    try {
        $('#NTM_Gruppierung').find('h2').click(function () {
            var MyElement = $(this);
            if (MyElement.hasClass("ntm_NewsClose")) {
                $(this).next().slideUp();
                $(this).attr("class", "ntm_ReiterLbl");
            }
            else {
                $(this).next().slideToggle();
                $("#NTM_Gruppierung h2").attr("class", "ntm_ReiterLbl");
                $(this).attr("class", "ntm_ReiterLbl ntm_NewsClose");
                $("#NTM_Gruppierung div.ntm_Body").not($(this).next()).slideUp();
            }
            return false;
        });
    } catch (Err) { }
    try {
        $('#NTM_Gruppierung').find('h3').click(function () {
            var MyElement = $(this);
            if (MyElement.hasClass("ntm_NewsClose")) {
                $(this).next().slideUp();
                $(this).attr("class", "ntm_NewsTitle");
            }
            else {
                $(this).next().slideToggle();
                $("#NTM_Gruppierung h3").attr("class", "ntm_NewsTitle");
                $(this).attr("class", "ntm_NewsTitle ntm_NewsClose");
                $("#NTM_Gruppierung div.ntm_NewsText").not($(this).next()).slideUp();
            }
            return false;
        });
    } catch (Err) { }
    try {
        $('h3.ntm_AkkordeonTitle').click(function () {
            var MyElement = $(this);
            if (MyElement.hasClass("ntm_NewsClose")) {
                MyElement.next('div.ntm_AkkordeonText').slideUp();
                MyElement.attr("class", "ntm_AkkordeonTitle");
            }
            else {
                MyElement.next('div.ntm_AkkordeonText').slideDown();
                $('h3.ntm_AkkordeonTitle').attr("class", "ntm_AkkordeonTitle");  // optional, falls immer nur ein Akkordeon aufgeklappt sein soll
                MyElement.attr("class", "ntm_AkkordeonTitle ntm_NewsClose");
                $('div.ntm_AkkordeonText').not(MyElement.next('div.ntm_AkkordeonText')).slideUp();  // optional, falls immer nur ein Akkordeon aufgeklappt sein soll
            }
            return false;
        });
    } catch (Err) { }
    $(".ms-rteStyle-MAnzeigen").click(function () {
        $(this).parent().next(".ms-rteElement-Ausblenden").slideToggle();
    });


    // Referrer an Link zur Anzeige h�ngen
    try {
        $.each($('.ntm_Anzeigenlink'), function () {
            var value = $(this).attr("href");
            if (value.indexOf('Source=') == -1) {
                var Referrer = window.location.href;
                Referrer = Referrer.replace(/\:/g, "%3A");
                Referrer = Referrer.replace(/\./g, "%2E");
                Referrer = Referrer.replace(/\//g, "%2F");
                Referrer = Referrer.replace(/\_/g, "%5F");
                Referrer = Referrer.replace(/\ /g, "%20");
                $(this).attr("href", value + "&Source=" + Referrer);
            }
        });
    } catch (Err) { }
    // Referrer an Link zu Terminen h�ngen
    try {
        $.each($('.ntm_TermineTitleLink'), function () {
            var value = $(this).attr("href");
            if (value.indexOf('Source=') == -1) {
                var Referrer = window.location.href;
                Referrer = Referrer.replace(/\:/g, "%3A");
                Referrer = Referrer.replace(/\./g, "%2E");
                Referrer = Referrer.replace(/\//g, "%2F");
                Referrer = Referrer.replace(/\_/g, "%5F");
                Referrer = Referrer.replace(/\ /g, "%20");
                $(this).attr("href", value + "&Source=" + Referrer);
            }
        });
    } catch (Err) { }

    // �nderungen nach Scrollen
    try {
        var offset = 250;
        var duration = 500;
        $("#s4-workspace").scroll(function () {
            // Link "Nach oben" nach Scrollen einblenden
            if ($(this).scrollTop() > offset) {
                $('#NTM_TopLink, #NTM_goToTop').fadeIn(duration);
            } else {
                $('#NTM_TopLink, #NTM_goToTop').fadeOut(duration);
            }
        });
    } catch (Err) { }
    // Top Button Funktion
    try {
        $('#NTM_TopLink, #NTM_TopLink2, #NTM_goToTop').click(function (event) {
            event.preventDefault();
            $('#s4-workspace').animate({ scrollTop: 0 }, duration);
            return false;
        })
    } catch (Err) { }



    // Akkordeon �ffnen, wenn Parameter mit Titel in URL
    try { var Akkordeon = decodeURIComponent(getUrlParameter('Akk')); } catch (Err) { }
    if (Akkordeon) {
        var item = $("#DeltaPlaceHolderMain H3.ntm_AkkordeonTitle:contains('" + Akkordeon + "')");
        // Wenn das Item gefunden wurde
        if (item.length == 1) {
            item.click();
            // Heruntersrollen zum Item
            var animationTime = 500; // Dauer f�r das Herunterscrollen in Millisekunden
            //var itemTop = Math.round(item.position().top, 0) + 280; // Y-Position ermitteln
            var itemTop = Math.round(item.position().top, 0); // Y-Position ermitteln
            $('#s4-workspace').animate({ scrollTop: itemTop }, animationTime);
        }
    }

    // Direktes �ffnen eines Akkordeon-Eintrags �ber URL-Parameter
    try { var openAccParam = getUrlParameter('Akknum'); } catch (Err) { }
    if (openAccParam && $.isNumeric(openAccParam)) {
        var index = parseInt(openAccParam) - 1;
        var elems = $('#DeltaPlaceHolderMain .ntm_AkkordeonTitle');
        if (index > -1 && index < elems.length) {
            elems.eq(index).click();
            // Heruntersrollen zum Item
            var animationTime = 500; // Dauer f�r das Herunterscrollen in Millisekunden
            var itemTop = Math.round(elems.eq(index).position().top, 0); // Y-Position ermitteln
            $('#s4-workspace').animate({ scrollTop: itemTop }, animationTime);
        }
    }


}); // end document.ready

if (typeof asyncDeltaManager != 'undefined') {
    asyncDeltaManager.add_endRequest(hashchanged);
}
function hashchanged() {
    var url = window.location.href;
    try { url = url.toLowerCase(); } catch (Err) { }
}

// Navigation f�r mobile Ansicht
function SetMobileNav() {
    var NavContent = $("#NTM_MainNavCont").html();
    $("#NTM_MobileNavCont").html('' + NavContent);
    alert(NavContent);
    if ((WindowWidth <= 1024)) {
        $("#NTM_MainNavCont").attr('style', 'display:none;');
    }
    else {
        $("#NTM_MobileNavCont").attr('style', 'display:none;');
    }
}

// mobile Navigation Item Toggle
function NavItemToggle() {
    var WindowWidth = $(window).width(); // Returns width of browser viewport
    //alert(NavContent);	
    if ((WindowWidth <= 1024)) {
        $('#DeltaTopNavigation ul.root.ms-core-listMenu-root.static ul li:has(ul) > a').append('<div class="ntm_OpenIco"><img src="/_layouts/15/images/blank.gif" alt=""></div>');
        $('#DeltaTopNavigation ul.root.ms-core-listMenu-root.static ul.dynamic').attr('style', 'display:none;');
        // Find li has ul and add Toggle Function
        $('#DeltaTopNavigation .ntm_OpenIco')
            .on(
                'click',
                function (e) {
                    e.preventDefault();

                    if ($(this).parent().parent().attr('id') != 'NTM_NavItemTools') {

                        $(this).parent().parent()
                            .toggleClass('ntm_Collapsed ntm_Expanded')
                            .children('ul')
                            .slideToggle();

                    }

                }
            )
            .parent().parent().addClass('ntm_Collapsed')
            ;
    }
    else {
        //
    }
}

// mobile Navigation GlobalNav Item Toggle
function GlobalNavItemToggle() {
    var WindowWidth = $(window).width(); // Returns width of browser viewport
    //alert(NavContent);	
    if ((WindowWidth <= 1024)) {
        $('#DeltaTopNavigation ul.GlobalNav li:has(ul) > a').append('<div class="ntm_OpenIco"><img src="/_layouts/15/images/blank.gif" alt=""></div>');
        $('#DeltaTopNavigation ul.GlobalNav ul.GlobalNav').attr('style', 'display:none;');
        // Find li has ul and add Toggle Function
        $('#DeltaTopNavigation .ntm_OpenIco')
            .on(
                'click',
                function (e) {
                    e.preventDefault();

                    if ($(this).parent().parent().attr('id') != 'NTM_NavItemTools') {

                        $(this).parent().parent()
                            .toggleClass('ntm_Collapsed ntm_Expanded')
                            .children('ul')
                            .slideToggle();

                    }

                }
            )
            .parent().parent().addClass('ntm_Collapsed')
            ;
    }
    else {
        //
    }
}
function GlobalNavItemFly() {
    $('#DeltaTopNavigation ul.GlobalNav ul.GlobalNav li:has(ul) > a').addClass('dynamic-children');
}
// Globale Navigation Navigationselemente in Icons wandeln
function SetGlobalNavIcons() {
    try {
		$("#NTM_MainNavCont span.GlobalNavContainer > ul.GlobalNav > li > a span:contains('Blauer')").parent().parent().addClass("ntm_IcoBlauerPlan");
        $("#NTM_MainNavCont ul.GlobalNav > li > a span:contains('Meine Seite')").parent().parent().addClass("ntm_IcoMyPage");
        $("#NTM_MainNavCont ul.GlobalNav > li > a span:contains('Pers�nliche')").parent().parent().addClass("ntm_IcoMyPage");

    } catch (Err) { }
}
// Icons in Webparttitel einf�gen Startseite
function SetWPTitleIconsHome() {
    try {
        $("h2.ms-webpart-titleText span:contains('Termine')").parent().parent().addClass("ntm_IcoTermine");
        $("h2.ms-webpart-titleText span:contains('Veranstaltungen')").parent().parent().addClass("ntm_IcoEvents");
        $("h2.ms-webpart-titleText span:contains('Nachrichten')").parent().parent().addClass("ntm_IcoNews");
        $("h2.ms-webpart-titleText span:contains('News')").parent().parent().addClass("ntm_IcoNews");
        $("h2.ms-webpart-titleText span:contains('Schnelleinstiege')").parent().parent().addClass("ntm_IcoFav");
        $("h2.ms-webpart-titleText span:contains('Favoriten')").parent().parent().addClass("ntm_IcoFav");
        $("h2.ms-webpart-titleText span:contains('Quicklinks')").parent().parent().addClass("ntm_IcoLinks");
        $("h2.ms-webpart-titleText span:contains('Links')").parent().parent().addClass("ntm_IcoLinks");
        $("h2.ms-webpart-titleText span:contains('Dokumente')").parent().parent().addClass("ntm_IcoDokumente");

    } catch (Err) { }
    try {
        $("h2.ms-webpart-titleText span:contains('Bilder')").closest("div.ms-webpart-chrome-title").attr('style', 'display:block;');
        $("h2.ms-webpart-titleText span:contains('Bilder')").closest("div.s4-wpcell-plain.ms-webpartzone-cell").attr('style', 'padding-left:8px;');
    } catch (Err) { }
}
function KachelnNorm() {
    var bg;
    $('.ntm_KachelnNorm.boxgrid.caption').hover(function () {
        $(".cover", this).stop().animate({ top: '-90px' }, { queue: false, duration: 160 });
        bg = $(".ntm_CtIcon", this).attr('style');
        var bghover = $(".ntm_CtIcon", this).attr('hoverbg');
        $(".ntm_CtIcon", this).attr("style", "background-image: url('" + bghover + "');");
    }, function () {
        $(".cover", this).stop().animate({ top: '0px' }, { queue: false, duration: 160 });
        $(".ntm_CtIcon", this).attr("style", bg);
    });
    $('.ntm_KachelnNorm.boxgrid.caption').click(function () {
        var url = $(".cover", this).attr('link');
        var rec = $(".cover", this).attr('target');
        window.open(url, rec);
        return false;
    });
}
function KachelnBig() {
    var bg;
    $('.ntm_KachelnBig.boxgrid.caption, .ntm_KachelnEinfach.boxgrid.caption, .ntm_KachelnDocs.boxgrid.caption').hover(function () {
        $(".coverA", this).stop().animate({ top: '-32px' }, { queue: false, duration: 160 });
        $(".coverB", this).stop().animate({ top: '-65px' }, { queue: false, duration: 160 });
        bg = $(".ntm_CtIcon", this).attr('style');
        var bghover = $(".ntm_CtIcon", this).attr('hoverbg');
        $(".ntm_CtIcon", this).attr("style", "background-image: url('" + bghover + "');");
    }, function () {
        $(".coverA", this).stop().animate({ top: '0px' }, { queue: false, duration: 160 });
        $(".coverB", this).stop().animate({ top: '0px' }, { queue: false, duration: 160 });
        $(".ntm_CtIcon", this).attr("style", bg);
    });
    $('.ntm_KachelnBig.boxgrid.caption, .ntm_KachelnEinfach.boxgrid.caption, .ntm_KachelnDocs.boxgrid.caption').click(function () {
        var url = $(".cover", this).attr('link');
        var rec = $(".cover", this).attr('target');
        window.open(url, rec);
        return false;
    });
}


// active NavItem Parent
function activeNavItem() {
    var container = $('#DeltaPlaceHolderLeftNavBar ul.root.ms-core-listMenu-root.static > li > ul > li.selected');
    container.attr("title", "active");
    //var containerParent = container.parent();
    var ParentLi = container.parent("ul").parent("li");
    ParentLi.addClass('ntm_Expanded ntm_HasSelectedItem').attr("title", "HasSelectedItem");
}

// Link Websiteinhalte ausblenden
function Websiteinhalte() {
    try {
        $('#sideNavBox .ms-core-listMenu-verticalBox').find('a').each(function () {
            var value = $(this).attr('href').toLowerCase();
            if (value.indexOf("/15/viewlsts.aspx") >= 0) {
                $(this).closest("li").hide();
            }
            else { }
        });
    } catch (Err) { }
}
// Zuletzt verwendet/ Recent aus linker Navigation entfernen
function Recent() {
    try {
        $(".ms-core-sideNavBox-removeLeftMargin span:contains('Zuletzt verwendet')").closest("li").attr('style', 'display:none;');
    } catch (Err) { }
    try {
        $(".ms-core-sideNavBox-removeLeftMargin span:contains('Recent')").closest("li").attr('style', 'display:none;');
    } catch (Err) { }
}
// Gesch�tzter Bereich	
function SafeRegion() {
    try {
        $("#sideNavBox .ms-core-listMenu-verticalBox a:contains('tzter Bereich'), #sideNavBox .ms-core-listMenu-verticalBox a:contains('um gesch'), #sideNavBox .ms-core-listMenu-verticalBox a:contains('internen Bereich')").parent("li.static").addClass('ntm_GeschuetzterBereich').attr("title", "interner Bereich");
    } catch (Err) { }

}



// Bilder Slideshow Bilder Fade
function cycleDivs() {
    try {
        $('.ntm_DivCycler').each(function () {
            // scope everything for each slideshow
            var $this = this;
            $('> :gt(0)', $this).hide();
            setInterval(function () { $('> :first-child', $this).fadeOut(1500).next().fadeIn(1500).end().appendTo($this); cycleDivSetHeight(); }, 5000);
        })
    } catch (Err) { }
}
function cycleDivSetHeight() {
    try {
        $('#DeltaPlaceHolderMain').find('.ntm_DivCycler').each(function () {
            var divHeight = $(this).find('img').height() + 70;
            $(this).css('height', divHeight + 'px');
        })
    } catch (Err) { }
}

function openSPDialog(pageUrl, pageTitle) {
    SP.UI.ModalDialog.showModalDialog(
        {
            url: pageUrl,
            //width: 500,  
            //height: 500,  
            title: pageTitle
        }
    );
}

function doFollowPage() {
    SP.SOD.executeFunc('followingcommon.js', 'FollowSite', function () {
        SetFollowStatus(window.location.href, true, false);
    });
    return false;
}

function OpenInNewTab(url) {
    var win = window.open(url, '_blank');
    win.focus();
}



/* Zu den Favoriten hinzuf�gen */
function CreateBookmarkLink() {
    url = location.href;
    title = "Intranet ZAP - " + document.title;

    if (window.sidebar) { // Mozilla Firefox Bookmark		
        window.sidebar.addPanel(title, url, "");
    }
    else if (window.external) { // IE Favorite		
        window.external.AddFavorite(url, title);
    }
    else if (window.opera && window.print) { // Opera Hotlist	
        alert("Sorry, aber Ihr Browser unterst�tzt diese Funktion leider nicht! Dr�cken Sie stattdessen Strg+D um die Seite als Lesezeichen zu speichern.");
    }
}
function fav() {
    if ((navigator.appName == 'Microsoft Internet Explorer') && (parseInt(navigator.appVersion) >= 4) && (navigator.platform != 'MacPPC')) {
        Titel = "Intranet ZAP - " + document.title;
        window.external.AddFavorite(location.href, Titel);
    }
    else {
        alert('Dies funktioniert leider nur mit Internet Explorer ab Version 4.x unter Windows!');
    }
}
/* Seite drucken */
function printPage() {
    if (window.print) { window.print(); return false; }
    else { alert("Sorry, aber Ihr Browser unterst�tzt nicht das direkte Drucken von HTML-Seiten!"); }
}
/* Seite speichern */
function savePage() {
    if (document.execCommand) document.execCommand('saveAs');
    else alert("Sorry, aber Ihr Browser unterst�tzt nicht das direkte Speichern von HTML-Seiten!");
}


/* Allen Suchfeldern die sich in #DeltaPlaceHolderMain befinden einen ResetSearch-Button anf�gen */
function addSearchResetButton() {
    var box = $('#DeltaPlaceHolderMain #SearchBox');
    if (box.find('.ntm_ResetSearch').length === 0) {
        $('#DeltaPlaceHolderMain #SearchBox').append($('<div class="ntm_ResetSearch"><a href="#" title="Sucheingabe zur&uuml;cksetzen" onclick="resetSearchEntry();"><img src="/_layouts/15/images/blank.gif" alt=""></a></div>'));
    }
}
function resetSearchEntry() {
    // Wenn ein k-Url-Parameter gesetzt ist verh�lt sich SharePoint wieder ganz anders
    var urlParam = getUrlParameter('k');
    if (urlParam) {
        $('#DeltaPlaceHolderMain #SearchBox INPUT').val('');
    }

    setTimeout(function () { window.location.hash = '#k='; }, 1);

    // da beim Setzen des Hash die SearchBox auch neu komplett neu gerendert wird m�ssen wir den Reset-Button wieder hinzuf�gen (mit timeout, sonst ist die Box ggf. noch nicht da)
    setTimeout(addSearchResetButton, 200);
}
// manchmal greift der document.ready, manchmal das _spBodyOnLoadFunctionNames
//_spBodyOnLoadFunctionNames.push("addSearchResetButton");
//$(document).ready(addSearchResetButton);



// Ermitteln eines URL-Parameter-Wertes
function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

var nteam = nteam || {};

/* Inhaltsabfrage Elemente dynamisch nachladen */
(function () {
    "use strict"

    window.nteam.NachladeListe = window.nteam.NachladeListe || {}
    window.nteam.NachladeListe.Items = [];
    window.nteam.NachladeListe.Konfiguration = {};

    var config = {
        defaultInitialItems: 10, // Initiale Anzahl der angezeigten Items
        defaultItemsToShow: 10, // Die per "Page" aufzuklappenden Items
        inactiveClass: 'ntm_Inactive', // Klasse f�r den Nachladen-Button, wenn schon alle vorhandenen Items angezeigt werden
        inactiveTooltipp: 'alle Elemente werden bereits angezeigt' // Tooltipp, der f�r den Nachladen-Button angezeigt wird, wenn schon alle vorhandenen Items angezeigt werden
    }
    window.nteam.NachladeListe.config = config;


    function getMyConfig(webpartId) {
        // Default-Wert
        var initialItems = config.defaultInitialItems;
        var itemsToShow = config.defaultItemsToShow;

        var konfiguration = window.nteam.NachladeListe.Konfiguration; // custom Konfiguration		
        if (konfiguration) {
            if (konfiguration && konfiguration.hasOwnProperty(webpartId)) {
                var customConfig = konfiguration[webpartId];
                initialItems = customConfig.initial || config.defaultInitialItems;
                itemsToShow = customConfig.nachladen || config.defaultItemsToShow;
            }
        }

        return { initialItems: initialItems, itemsToShow: itemsToShow };
    }

    function NachladeList(webpartId) {
        var that = this;

        var myConfig = getMyConfig(webpartId);

        that.shownItems = myConfig.initialItems; // Anzahl der aktuell angezeigten Elemente
        that.itemsToShow = myConfig.itemsToShow; // Anzahl der per "Page" aufzuklappenden Items
        that.list = $('[webpartid="' + webpartId + '"] .dfwp-list'); // Selector der Liste
        that.items = that.list.find('.ntm_Nachladen'); // Selektor der Elemente der Liste
        that.nachladeBtn = that.list.find('.ntm_BTNNachladen'); // Selektor des Nachladebuttons

        // "�berz�hlige" ausblenden
        $.each(that.items, function (index, item) {
            if (index >= that.shownItems) { $(item).hide(); }
        });

        if (that.items.length <= that.shownItems) {
            // Nachladebutton "ausblenden" (weil Maximalanzahl schon angezeigt wird)
            //that.nachladeBtn.hide();
            that.nachladeBtn.addClass(config.inactiveClass);
            that.nachladeBtn.attr('title', config.inactiveTooltipp);
        } else {
            // Klickhandler f�r Nachladebutton
            that.nachladeBtn.click(function () {
                // Zus�tzliche Items anzeigen
                $.each(that.items, function (index, item) {
                    if (index >= that.shownItems && index < that.shownItems + that.itemsToShow) {
                        // slideDown() auf das Item geht nicht so wirklich.. 
                        // wenn man das slideDown() auf das li setzt schaut es nicht so wirklich gut aus
                        $(item).fadeIn();
                    }
                });

                // Anzahl der angezeigten Items um itemsToShow erh�hen
                that.shownItems += that.itemsToShow;

                // bei Bedarf den Nachladebutton "ausblenden"
                if (that.items.length <= that.shownItems) {
                    //that.nachladeBtn.hide();
                    that.nachladeBtn.addClass(config.inactiveClass);
                    that.nachladeBtn.attr('title', config.inactiveTooltipp);
                }
            });
        }
    }

    $(document).ready(function (list) {
        try {
            // Webparts mit Content-Editor-Webparts rausfiltern und NachladeListe initialisieren
            var webparts = $('[webpartid]').has('.dfwp-list');
            $.each(webparts, function (index, webpart) {
                var newItem = new NachladeList($(webpart).attr('webpartid'));
                window.nteam.NachladeListe.Items.push(newItem);
            });
            console.log("Nachladewebpart gefunden");
        } catch (e) {
            console.error('Fehler in der Verarbeitung der Inhaltsabfrage - ' + e.stack);
        }
    });
})();
