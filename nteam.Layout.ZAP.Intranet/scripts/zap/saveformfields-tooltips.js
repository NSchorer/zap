﻿/* ================================================================================
 * Version 0.1, 31.07.2017, Sascha von König
 * ================================================================================ */
// ~sitelayouts/scripts/bvg/saveformfields-tooltips.js

// Zusätzliche Stylesheets einbinden
//$('head').append('<link href="/style%20library/css/Projekte.css" type="text/css" rel="stylesheet"/>'); 

/* -------------------------------------------------------------------------------- */

(function(){

	// Initialisieren der Variablen zum Überschreiben der Objekte
	var ctx = {};
	ctx.Templates = {};

	// Rendering der Felder überschreiben. In erster Linie werden die Felder auf Readonly gesetzt.
	ctx.Templates.Fields = {
		'ElementID' : {
			'EditForm' : SPField_FormDisplay_Default,
			'NewForm' : SPField_FormDisplay_Default
		},
		'Path' : {
			'EditForm' : SPField_FormDisplay_Default,
			'NewForm' : SPField_FormDisplay_Default
		},
		'URL' : {
			'EditForm' : SPField_FormDisplay_Default,
			'NewForm' : SPField_FormDisplay_Default
		},
		'Hilfreich' : {
			'EditForm' : SPField_FormDisplay_Default,
			'NewForm' : SPField_FormDisplay_Default
		},
		'NichtHilfreich' : {
			'EditForm' : SPField_FormDisplay_Default,
			'NewForm' : SPField_FormDisplay_Default
		}
	};
	
	// Registrieren der Änderung am Feld
	SPClientTemplates.TemplateManager.RegisterTemplateOverrides(ctx);

})();


/* -------------------------------------------------------------------------------- */

$(document).ready(function(){
	
	// --- Debug Mode ------------------------------------------------------------- 
	debugmode = (GetUrlKeyValue("debug") == "1") ? true : false;


	
});

