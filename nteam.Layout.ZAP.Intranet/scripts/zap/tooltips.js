﻿(function () {

    "use strict";

    // Globale Variable
	var siteUrl = 'http://zap.afd-dbtg.frak';
	//var siteUrl = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + ''; 
	var myListUrl = siteUrl + '/Lists/Tooltips';
    var listElementToEditUrl = myListUrl + '/EditForm.aspx?ID=';
    var listTitle = 'Tooltips';
	//console.log("Tooltip Liste: " + myListUrl);

    /* Variablen zum zusammenpassen für Kunde */
    var defaultTitleValue = 'Tooltip Titel';
    var defaultDescriptionValue = 'Tooltip Beschreibung';
    var userProfilePropertyName = 'TooltipInfo';
    var showAllText = 'Alle Tooltips anzeigen';
    var hilfreichText = 'War das für Sie hilfreich?';
    var hilfreichJaTitel = 'Das war hilfreich';
    var hilfreichNeinTitel = 'Das war nicht hilfreich';
    var schliessen = 'Schließen';
    var weiterempfehlen = 'Tooltip weiter empfehlen';
    var RueckmeldungText = 'Vielen Dank für Ihre Bewertung.'
    var timeout = 3000; // setzt Zeitraum für Rückmeldung einblenden

    var weiterempfehlenBetreff = 'Empfehlenswert';
    var weiterempfehlenInhalt = 'Hallo! \r\n\r\nDen Tooltip auf der folgenden Seite lohnt es sich anzuschauen:\r\n';
    var weiterempfehlenFooter = 'Viel Spass!';

    /* Variablen für GET Parameter und für benötigte URLs */
    var setTooltip = GetURLParameter('setTooltip');
	//console.log('setTooltip: ' + setTooltip);
    var showAll = GetURLParameter('showAll');
    var showId = GetURLParameter('showId');
    var pathname = window.location.pathname;
    var host = window.location.host;
    var nowDate = Date.now();
    var allTooltipData;


    window.nteam = window.nteam || {};
    var nteam = window.nteam

    nteam.tooltip = nteam.tooltip || {};
    var tooltip = nteam.tooltip;


    /* ==================================================================
     * getTooltipData
     * ------------------------------------------------------------------
     * This function get data to save tooltips information on SP list
     * ------------------------------------------------------------------
     * ================================================================== */
    function getTooltipData(e) {
        // Event Workaround für Internet Explorer
        var evt = window.event || e;
        if (!evt.target) { //if event obj doesn't support e.target, presume it does e.srcElement
            evt.target = evt.srcElement; //extend obj with custom e.target prop
        }

        var elementId = null;
        if (evt.target.id !== "") {
            elementId = evt.target.id;
        } else {
            var closestId = $(evt.target).closest('[id]')[0];
            elementId = closestId.id;
            // show full element path
            var rightArrowParents = [];
            $(this).parents().addBack().not('html').each(function () {
                var entry = this.tagName.toLowerCase();
                if (this.id) {
                    entry += "#" + this.id;
                }
                if (this.className) {
                    entry += "." + this.className.replace(/ /g, '.');
                }
                rightArrowParents.push(entry);
            });

            // show full element path in one string
            var fullPath = rightArrowParents.toString();
            // n - show position of last element with id in full path, m - show position to cut substring
            var n = fullPath.search(closestId.tagName.toLowerCase() + "#" + closestId.id);
            var m = fullPath.search(".mouseOn");
            var path = fullPath.substring(n, m).replace(/,/g, ' ');
            if (path.substr(path.length - 4) == ' img') {
                path = path.substr(0, (path.length - 4));
            }
        }

        createListItem(elementId, path);

        return false;
    }


    /* ==================================================================
     * createListItem
     * ------------------------------------------------------------------
     * Saves data on a SP list. Data such as title, description, color 
     * and url are declared by default values.
     * ------------------------------------------------------------------
     * String elementId - id of current element or of closest element which will be saved
     * String path - path between saved current element and closest element with id, saved when current element has no id 
     * ================================================================== */

    //tooltip.createListItem= function(elementId, path){



    function createListItem(elementId, path) {
        var clientContext = new SP.ClientContext(siteUrl);
        var oList = clientContext.get_web().get_lists().getByTitle(listTitle);
        var itemCreateInfo = new SP.ListItemCreationInformation();
        var oListItem;
        var data = {};

        data.oListItem = oList.addItem(itemCreateInfo);

        data.oListItem.set_item('Title', defaultTitleValue);
        data.oListItem.set_item('Beschreibung', defaultDescriptionValue);
        data.oListItem.set_item('ElementID', elementId);
        data.oListItem.set_item('Path', path);
        data.oListItem.set_item('URL', pathname);

        data.oListItem.update();
        clientContext.load(data.oListItem);
        clientContext.executeQueryAsync(Function.createDelegate(data, onQuerySucceeded), Function.createDelegate(data, onQueryFailed));
    }


    /* ==================================================================
     * onQuerySucceeded
     * ------------------------------------------------------------------
     * Redirects to edit saved record on a SP list
     * ------------------------------------------------------------------
     * ================================================================== */
    function onQuerySucceeded() {
        var data = this;
        window.location = listElementToEditUrl + data.oListItem.get_id();
    }

    function onQueryFailed(sender, args) {
        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    /* ==================================================================
     * showTooltips
     * ------------------------------------------------------------------
     * Gets data from a SP list
     * ------------------------------------------------------------------
     * String listTitle - global parameter, which shows specific list 
     * ================================================================== */
    function showTooltips() {
        $.ajax({
            url: siteUrl + "/_api/web/lists/getbytitle('" + listTitle + "')/items",
            method: "GET",
            headers: { "Accept": "application/json; odata=verbose" },
            success: function (data) {
                if (data.d.results.length > 0) {
                    getDataForTooltip(data.d.results);
                }
            },
            error: function (data) {
                console.log("Error: " + data);
            }
        });
    }

    /* ==================================================================
     * getDataForTooltip
     * ------------------------------------------------------------------
     * Gets all data from cookies
     * Each line from results array is searching in cookies.
     * Shows tooltip with data for all elements which are not in User 
     * Profile or current date is between start and end date.
     * Shows Tooltips only for this Website by pathname or
     * show one Tooltip by given ID
     * ------------------------------------------------------------------
     * Array[] results - array with data for all tooltips
     * ================================================================== */
    function getDataForTooltip(results) {
        var cookies = document.cookie.split('; ');
        var cookieData = [];
        var userProfileData = [];
        var aktivTooltips = 0;

        if (!showAll == true) {
            if (allTooltipData) {
                $.each(allTooltipData, function (index, value) {
                    userProfileData.push(value.replace(/&&/g, ' ').replace('-tooltip', ''));
                });
            }
        }
        //	console.log(results);
        $.each(results, function (index, value) {
            // stringToSearch - das ist derselbe Struktur wie im User Profil gespeichert
            var stringToSearch = value.ElementID + '=' + value.Path + '||' + value.Id;
            if (value.URL && value.URL.Description == pathname) {
                aktivTooltips++;
                if (showId === undefined) {
                    var oneDayInMiliseconds = 86400000;
                    //				console.log(stringToSearch);			
                    if ((nowDate >= Date.parse(value.Beginn) || value.Beginn == null) && (nowDate < (Date.parse(value.Ende) + oneDayInMiliseconds) || value.Ende == null) && jQuery.inArray(stringToSearch, userProfileData) == -1) {
                        addTooltipToView(value);
                    }
                } else if (value.ID.toString() === showId.toString()) {
                    addTooltipToView(value);
                }
            }

        });

        //console.log(userProfileData);
        if (aktivTooltips === 0) {
            $('#NTM_TooltipsViewAll').css('display', 'none');
        }
    }

    function addTooltipToView(value) {
        var dataPath = null;
        if (value.Path !== null) {
            dataPath = value.Path.replace(/ /g, '&&');
        }
        var tooltipContainer = '<div id="' + value.ElementID + '-tooltip" data-path="' + dataPath + '" data-url="' + value.URL.Url + '?showId=' + value.Id + '" data-id="' + value.Id + '" class="ntm_TooltipCont tooltip tooltipShow ' + value.Ausrichtung + '"><div class="ntm_tooltipBody ntm_Farbe ' + value.Farbe + '"><div class="ntm_Close"><a href="javascript:void(0)" class="close thLink" title="Schließen"><span><img src="/_layouts/15/images/blank.gif" alt="' + schliessen + '" /></span></a></div><div class="ntm_Share"><a href="javascript:void(0)" title="' + weiterempfehlen + '" class="thLink site_share_button"><span><img src="/_layouts/15/images/blank.gif" alt="' + weiterempfehlen + '" /></span></a></div><div class="tooltipTitle">' + value.Title + '</div><div class="ntm_TooltipHTML">' + value.Beschreibung + '</div><div class="ntm_TooltipFooter">' +
		//<div class="ntm_Hilfreich">' + hilfreichText + ' <a href="javascript:void(0);" class="helpfulYes" title="' + hilfreichJaTitel + '">Ja</a>  <a href="javascript:void(0);" class="helpfulNo" title="' + hilfreichNeinTitel + '">Nein</a></div><div class="ntm_Rueckmeldung">' + RueckmeldungText + '</div>' +
		'</div></div><div class="ntm_Triangle ' + value.Farbe + '" ></div></div>';
        if (value.Path !== null) {
            $(value.Path).first().css({ "position": "relative" }).append(tooltipContainer);
        } else {
            $('#' + value.ElementID).css({ "position": "relative" }).append(tooltipContainer);
        }

    }


    /* ==================================================================
     * GetURLParameter
     * ------------------------------------------------------------------
     * Gets value of given parameter from URL
     * ------------------------------------------------------------------
     * String sParam - name of given parameter
     * ================================================================== */
    function GetURLParameter(sParam) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    // Liest den Wert aus dem UserProfile, wird über REST erledigt, weil JSOM offensichtlich keine Custom-Properties liefert
    var readUserProfile = function (trueCallback) {
        //var siteUrl = _spPageContextInfo.siteAbsoluteUrl; // "http://athena/bistum/";
        //console.log("siteUrl= " + siteUrl);
        $.ajax({
            url: siteUrl + "/_api/SP.UserProfiles.PeopleManager/GetMyProperties",
            method: "GET",
            headers: { "Accept": "application/json; odata=verbose" },
            success: function (data) {
                data = data.d;
                for (var i = 0; i < data.UserProfileProperties.results.length; i++) {
                    if (data.UserProfileProperties.results[i].Key == userProfilePropertyName) {
                        trueCallback(data.UserProfileProperties.results[i].Value);
                        return;
                    }
                }
            },
            error: function (err) {
                console.error('Fehler beim Zugriff auf die Properties des Users: ' + err.responseText);
            }
        });
    }


    var xml_special_to_escaped_one_map = {
        '&': '&amp;',
        '"': '&quot;',
        '<': '&lt;',
        '>': '&gt;'
    };

    var escaped_one_to_xml_special_map = {
        '&amp;': '&',
        '&quot;': '"',
        '&lt;': '<',
        '&gt;': '>'
    };

    function encodeXml(string) {
        return string.replace(/([\&"<>])/g, function (str, item) {
            return xml_special_to_escaped_one_map[item];
        });
    };

    function decodeXml(string) {
        return string.replace(/(&quot;|&lt;|&gt;|&amp;)/g,
            function (str, item) {
                return escaped_one_to_xml_special_map[item];
            });
    }

    // User Profil lesen
    readUserProfile(function (val) {
        if (!val) {
            allTooltipData = [];
        } else {
            val = decodeXml(val);
            // check if JSON from UserProperty is correct
            if (/^[\],:{}\s]*$/.test(val.replace(/\\["\\\/bfnrtu]/g, '@').
            replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
            replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                allTooltipData = JSON.parse(val);
            } else {
                console.log('Check the value of UserProfileProperty: TooltipInfo');
            }
        }
    });

    // Alle Tooltips einblenden
    tooltip.showAllTooltip = function () {
        var href = window.location.href;
        if (href.substr(href.length - 1) == '#') {
            href = href.substr(0, (href.length - 1));
        }
        window.location.replace(href + '?showAll=true');
    }


    $(document).on("click", "a.close", function () {
        var parentWithLink = $(this).parents('a');
        var parentHref = parentWithLink.attr('href');
        parentWithLink.removeAttr('href');

        $(this).closest('.tooltipShow').addClass('tooltipHide').removeClass('tooltipShow');
        var div = $(this).closest('.tooltipHide').get(0);
        var divData = div.id + '=' + div.getAttribute('data-path') + '||' + div.getAttribute('data-id');
        parentWithLink.attr('href', parentHref);

        // User Profile
        if (jQuery.inArray(divData, allTooltipData) < 0) {
            allTooltipData.push(divData);
            allTooltipData = allTooltipData.map(function (obj) {
                return encodeXml(obj);
            });
            console.log(JSON.stringify(allTooltipData));
            writeUserProfile(userProfilePropertyName, JSON.stringify(allTooltipData));
        }
    });

    // Hilfreich
    $(document).on("click", ".helpfulYes", function () {

        setRueckmeldung($(this));
        var tooltipId = $(this).closest('.tooltipShow').attr('data-id');

        callService(true, tooltipId);
        // no page reload
        return false;
    });

    // Nicht Hilfreich
    $(document).on("click", ".helpfulNo", function () {

        setRueckmeldung($(this));
        var tooltipId = $(this).closest('.tooltipShow').attr('data-id');

        callService(false, tooltipId);
        // no page reload
        return false;
    });

    function setRueckmeldung(elem) {
        var $div = elem.closest('div.ntm_TooltipFooter').find("div.ntm_Rueckmeldung");
        if ($div.is(":visible")) { return; }
        $div.show();
        setTimeout(function () {
            $div.hide();
        }, timeout);

        return;
    }

    function callService(hilfreich, tooltipId) {
        var data = { 'listName': listTitle, 'listURL': myListUrl, 'hilfreich': hilfreich, 'itemID': tooltipId };
        var webserviceURL = '/_vti_bin/nteam/Tooltip.WS/Tooltip.asmx';
        var method = 'countHelpful';

        //console.log("List Title = " + listTitle);
        //console.log("List URL = " + myListUrl);
        //console.log("Hilfreich = " + hilfreich);
        //console.log("Tooltip-ID = " + tooltipId);

        $.ajax({
            url: webserviceURL + '/' + method,
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: data ? JSON.stringify(data) : undefined,
            success: function (data) {
                var data = data ? data.d : data;
                console.log('success\n data: ' + data);
            },
            error: function (err) {
                console.log('Bei der Verarbeitung ist ein Fehler aufgetreten\n\n' + err.status + " " + err.statusText);
            }
        });
    }

    // Anzeige per Mail empfehlen
    $(document).on("click", "a.site_share_button", function () {
        try {
            var myURL = $(this).closest('div.tooltip').attr('data-url');
            var myTitle = document.title;
            $(location).attr('href', 'mailto:?subject='
                 + encodeURIComponent(weiterempfehlenBetreff)
                 + "&body="
                 + encodeURIComponent(weiterempfehlenInhalt)
                 + encodeURIComponent(myTitle)
                 + encodeURIComponent(" (")
                 + encodeURIComponent(myURL)
                 + encodeURIComponent(") \r\n\r\n" + weiterempfehlenFooter)
            );
        } catch (Err) { }
    });


    $(document).ready(function () {
		//console.log("document.ready");
        /*	$("<link/>", {
               rel: "stylesheet",
               type: "text/css",
               href: "/bistum/Style Library/CSS/Tooltips.css"
            }).appendTo("head");
        */
        $("a.close").trigger("click");
        $("a.site_share_button").trigger("click");
        $("button.ntm_tooltipsViewAll").trigger("click");

        if (setTooltip == 1 || setTooltip == "true") {
			//console.log("setTooltip=1");
            var prevElement = null;
            document.addEventListener('mousemove',
                function (e) {
                    var elem = $(e.target) || $(e.srcElement);
                    if (prevElement != null) {
                        prevElement.removeClass('mouseOn');
                        prevElement.off('click');
                    }
                    elem.addClass('mouseOn');
                    elem.on('click', getTooltipData);
                    prevElement = elem;
                }, true);
        } else {
			//console.log("else setTooltip");
            // Button for show all tooltips
            if (!showAll == true && showId === undefined) {
                $('#NTM_TooltipsViewAll').css('float', 'right').append('<div><button class="ntm_BtnTooltipsViewAll" type="button" onclick="window.nteam.tooltip.showAllTooltip()">' + showAllText + '</button></div>');
            }

            showTooltips();
        }
    });

    // Daten in User Profil speichern
    var writeUserProfile = function (propertyName, propertyValue) {

        var reallyWriteProfile = function () {
            var clientContext = new SP.ClientContext.get_current();
            var peopleManager = new SP.UserProfiles.PeopleManager(clientContext);
            var userProfileProperties = peopleManager.getMyProperties();
            clientContext.load(userProfileProperties, "AccountName");
            clientContext.executeQueryAsync(function () {
                var currentUserAccountName = userProfileProperties.get_accountName();
                // das ist leider erst ab mit dem neuesten CSOM-Package möglich (3rd of Sep 2014 or newer)
                //peopleManager.setSingleValueProfileProperty(currentUserAccountName, config.userProfilePropertyName, value);
                //clientContext.executeQueryAsync(function(){    			
                //	console.log("Kontrast-Property im Benutzerprofil gespeichert");
                //},  function(sender,args){
                //	console.log("Fehler beim Speichern des Kontrast-Property im Benutzerprofil: "+ args.get_message());
                //});
                SetUserProperty(currentUserAccountName, propertyName, propertyValue);
            });
        }

        SP.SOD.executeFunc("sp.js", "SP.ClientContext", function () {
            SP.SOD.registerSod("sp.userprofiles.js", SP.Utilities.Utility.getLayoutsPageUrl("sp.userprofiles.js"));
            SP.SOD.executeFunc("sp.userprofiles.js", "SP.UserProfiles.PeopleManager", reallyWriteProfile);
        });
    }


    // Many thanks to Yaroslav Pentsarskyy for his post http://www.sharemuch.com/2012/05/13/dynamically-modify-sharepoint-user-profile-properties-with-jquery/
    function SetUserProperty(accountName, propertyName, propertyValue) {
        // this variable contains the XML that is passed to the SOAP web service
        //  about which account will have which property modified
        var newData = '<?xml version="1.0" encoding="utf-8"?>'
            + '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'
            + '<soap:Body>'
            + ' <ModifyUserPropertyByAccountName xmlns="http://microsoft.com/webservices/SharePointPortalServer/UserProfileService">'
            + '  <accountName>' + accountName + '</accountName>'
            + '  <newData>'
                + '  <PropertyData>'
                + '  <IsPrivacyChanged>false</IsPrivacyChanged>'
                + '  <IsValueChanged>true</IsValueChanged>'
                + '  <Name>' + propertyName + '</Name>'
                + '  <Privacy>Public</Privacy>'
                + '  <Values><ValueData><Value xsi:type="xsd:string">' + propertyValue + '</Value></ValueData></Values>'
                + '  </PropertyData>'
            + '  </newData>'
            + ' </ModifyUserPropertyByAccountName>'
            + ' </soap:Body>'
            + '</soap:Envelope>'
        // this is the actual call to the web service and method responsible for modifying user profile property
        $.ajax({
            url: '/_vti_bin/UserProfileService.asmx',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("SOAPAction", "http://microsoft.com/webservices/SharePointPortalServer/UserProfileService/ModifyUserPropertyByAccountName");
            },
            type: "POST",
            dataType: "xml",
            data: newData,
            contentType: "text/xml; charset=\"utf-8\""
        }).done(function (data) {
            console.log("TooltipInfo-Property im Benutzerprofil gespeichert");
        }).fail(function (err) {
            console.log("Fehler beim Speichern des TooltipInfo-Property im Benutzerprofil: " + err.responseText);
        });
    }

})();