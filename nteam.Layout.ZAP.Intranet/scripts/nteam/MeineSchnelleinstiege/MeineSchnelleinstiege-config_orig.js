﻿(function () {
    "use strict"

    var meineSchnelleinstiege = window.nteam.MeineSchnelleinstiege;
    meineSchnelleinstiege.config = {
        MAX_SORTINGVALUE: 1073741823,
        REORDER_THRESHOLD: 100,
        maxToolsOnHomepage: 6,
        maxLinksOnHomepage: 12,
        rootUrl: '/'
    }

    // Ein bisschen fake um das Deployment in unterschiedlichen Umgebungen nicht konfigurieren zu müssen
    if (window.location.hostname == "venus") {
        meineSchnelleinstiege.config.rootUrl = '/bvg/';
    } else if (window.location.hostname == "talisker") {
        meineSchnelleinstiege.config.rootUrl = '/sites/bvg/';
    } else if (window.location.hostname == "athena") {
        meineSchnelleinstiege.config.rootUrl = '/nteam/';
    }
})();