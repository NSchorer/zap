﻿(function () {
    "use strict"

    var meineSchnelleinstiege = window.nteam.MeineSchnelleinstiege;
    meineSchnelleinstiege.config = {
        MAX_SORTINGVALUE: 1073741823,
        REORDER_THRESHOLD: 100,
        maxToolsOnHomepage: 3,
        maxLinksOnHomepage: 10,
        rootUrl: '/'
    }

    // Ein bisschen fake um das Deployment in unterschiedlichen Umgebungen nicht konfigurieren zu müssen
    if (window.location.toString().indexOf("/nteamnet/") > -1) {
        meineSchnelleinstiege.config.rootUrl = '/nteamnet/';
    } else if (window.location.toString().indexOf("/hypo/") > -1) {
        meineSchnelleinstiege.config.rootUrl = '/hypo/';
    } else if (window.location.toString().indexOf("/zap/") > -1) {
        meineSchnelleinstiege.config.rootUrl = '/zap/';


})();



