﻿(function () {
    "use strict"

    var meineSchnelleinstiege = window.nteam.MeineSchnelleinstiege;
    meineSchnelleinstiege.server = {};
    var server = meineSchnelleinstiege.server;
    var util = meineSchnelleinstiege.util;
    var config = meineSchnelleinstiege.config;


    server.addTool = function (toolId, successCallback) {
        callService('AddTool', { toolId: toolId }, successCallback, 'Fehler bei der Verarbeitung');
    }

    server.deleteLink = function (linkId, successCallback) {
        callService('DeleteLink', { linkId: linkId }, successCallback, 'Beim Löschen ist ein Fehler aufgetreten.');
    }

    server.updateSortierung = function (item, successCallback) {
        SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
            var clientContext = getRootClientContext();
            var web = clientContext.get_web();
            var listSchnelleinstiege = web.get_lists().getByTitle('Schnelleinstiege');

            var myItem = listSchnelleinstiege.getItemById(item.id);
            // hier "dürfen" wir das Update noch nicht direkt machen, weil wir sonst später einen Version Conflict vom SharePoint um die Ohren bekommen
            //myItem.set_item('Sortierung', item.sortierung);
            //myItem.update();
            clientContext.load(myItem);

            clientContext.executeQueryAsync(Function.createDelegate(this, function (p1, p2) {
                var clientContext = getRootClientContext();
                var web = clientContext.get_web();
                var listSchnelleinstiege = web.get_lists().getByTitle('Schnelleinstiege');

                var myItem = listSchnelleinstiege.getItemById(item.id);
                myItem.set_item('Sortierung', item.sortierung);
                myItem.update();

                clientContext.executeQueryAsync(Function.createDelegate(this, successCallback), Function.createDelegate(this, function (e1, e2) {
                    util.ShowError('Beim Sortieren ist ein Fehler aufgetreten. Bitte laden Sie die Seite neu.');
                }));

            }), Function.createDelegate(this, function (e1, e2) {
                util.ShowError('Beim Sortieren ist ein Fehler aufgetreten. Bitte laden Sie die Seite neu.');
            }));
        });
    }

    server.reorderList = function (successCallback) {
        callService('Reorder', undefined, successCallback, 'Beim Neusortieren ist ein Fehler aufgetreten. Bitte laden Sie die Seite neu.')
    }

    server.getStandardLinks = function (successCallback) {
        callService('SetStandardLinksForUser', undefined, undefined, 'Meine Schnelleinstiege: Beim Setzen der Standard-Links ist ein Fehler aufgetreten.');

        SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
            var data = { successCallback: successCallback, }

            var clientContext = getRootClientContext();
            var lists = clientContext.get_web().get_lists();
            var listStandardLinks = lists.getByTitle('Standard-Links');
            data.standardLinks = listStandardLinks.getItems(SP.CamlQuery.createAllItemsQuery(100));

            clientContext.load(data.standardLinks);
            clientContext.executeQueryAsync(Function.createDelegate(data, function (sender, args) {
                var data = this;

                function Schnelleinstieg(item) {
                    this.Id = item.get_id();
                    this.Anzeigename = item.get_item('Title');
                    this.InNewTab = item.get_item('InNewTab');
                    this.Link = item.get_item('Link'); if (this.Link && this.Link.get_url) { this.Link = this.Link.get_url(); }
                    this.LinkDescription = item.get_item('Link'); if (this.LinkDescription && this.LinkDescription.get_description) { this.LinkDescription = this.LinkDescription.get_description(); }
                    this.Tool = item.get_item('Tool'); if (this.Tool && this.Tool.get_lookupId) { this.Tool = this.Tool.get_lookupId(); }
                    this.Sortierung = item.get_item('Sortierung');
                }
                var schnelleinstiege = [];
                var schnelleinstiegeEnum = data.standardLinks.getEnumerator();
                while (schnelleinstiegeEnum.moveNext()) {
                    schnelleinstiege.push(new Schnelleinstieg(schnelleinstiegeEnum.get_current()));
                }

                data.successCallback(schnelleinstiege);

            }), Function.createDelegate(data, function (sender, args) {
                //util.ShowError('Meine Schnelleinstiege: Fehler beim Laden der initialen Daten');
				console.log('Meine Schnelleinstiege: Fehler beim Laden der initialen Daten');
            }));
        });
    }

    server.getUsersEinsteigeAndTools = function (callback) {
        SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
            var clientContext = getRootClientContext();
            var web = clientContext.get_web();
            var data = {
                callback: callback,
                currentUser: web.get_currentUser(),
                clientContext: clientContext
            }
            clientContext.load(data.currentUser);
            clientContext.executeQueryAsync(Function.createDelegate(data, function () {
                var data = this;
                var currentUser = data.currentUser;
                var userId = currentUser.get_id();

                var clientContext = data.clientContext;
                var lists = clientContext.get_web().get_lists();
                var listSchnelleinstiege = lists.getByTitle('Schnelleinstiege');
                var listTools = lists.getByTitle('Systeme & Tools');

                var camlSchnelleinstiege = new SP.CamlQuery();
                camlSchnelleinstiege.set_viewXml('<View><Query><Where> <Eq><FieldRef Name="Benutzer" LookupId="True" /><Value Type="Int">' + userId + '</Value></Eq></Where></Query><RowLimit>30</RowLimit></View>');
                var camlTools = new SP.CamlQuery();

                data.schnelleinstiegeList = listSchnelleinstiege;
                data.schnelleinstiege = listSchnelleinstiege.getItems(camlSchnelleinstiege);
                data.tools = listTools.getItems(camlTools);

                clientContext.load(data.schnelleinstiege);
                clientContext.load(listSchnelleinstiege, 'DefaultNewFormUrl');
                clientContext.load(listSchnelleinstiege, 'DefaultEditFormUrl');
                clientContext.load(data.tools);
                clientContext.executeQueryAsync(Function.createDelegate(data, function (sender, args) {
                    var data = this;

                    var urlNewForm = data.schnelleinstiegeList.get_defaultNewFormUrl();
                    var urlEditForm = data.schnelleinstiegeList.get_defaultEditFormUrl();

                    function Schnelleinstieg(item) {
                        this.Id = item.get_id();
                        this.Anzeigename = item.get_item('Title');
                        this.InNewTab = item.get_item('InNewTab');
                        this.Link = item.get_item('Link'); if (this.Link && this.Link.get_url) { this.Link = this.Link.get_url(); }
                        this.LinkDescription = item.get_item('Link'); if (this.LinkDescription && this.LinkDescription.get_description) { this.LinkDescription = this.LinkDescription.get_description(); }
                        this.Tool = item.get_item('Tool'); if (this.Tool && this.Tool.get_lookupId) { this.Tool = this.Tool.get_lookupId(); }
                        this.Sortierung = item.get_item('Sortierung');
                    }
                    var schnelleinstiege = [];
                    var schnelleinstiegeEnum = data.schnelleinstiege.getEnumerator();
                    while (schnelleinstiegeEnum.moveNext()) {
                        //var curr = schnelleinstiegeEnum.get_current();
                        //var benutzer = curr.get_item('Benutzer');
                        schnelleinstiege.push(new Schnelleinstieg(schnelleinstiegeEnum.get_current()));
                    }

                    function Tool(item) {
                        this.Id = item.get_id();
                        this.Anzeigename = item.get_item('Title');
                        this.Link = item.get_item('Link'); if (this.Link && this.Link.get_url) { this.Link = this.Link.get_url(); }
                        this.LinkDescription = item.get_item('Link'); if (this.LinkDescription && this.LinkDescription.get_description) { this.LinkDescription = this.LinkDescription.get_description(); }
                        this.Icon = item.get_item('Icon'); if (this.Icon && this.Icon.get_url) { this.Icon = this.Icon.get_url(); }
                        this.HoverIcon = item.get_item('HoverIcon'); if (this.HoverIcon && this.HoverIcon.get_url) { this.HoverIcon = this.HoverIcon.get_url(); }
                        this.Sortierung = item.get_item('Sortierung');
                        this.InNewTab = item.get_fieldValues().InNewTab ? item.get_item('InNewTab') : false;
                        this.Gruppe = item.get_item('Gruppe'); if (!this.Gruppe) { this.Gruppe = undefined; };
                    }
                    var tools = [];
                    var toolsEnum = data.tools.getEnumerator();
                    while (toolsEnum.moveNext()) {
                        tools.push(new Tool(toolsEnum.get_current()));
                    }

                    data.callback(schnelleinstiege, tools, urlNewForm, urlEditForm);

                }), Function.createDelegate(data, function (sender, args) {
                    //util.ShowError('Meine Schnelleinstiege: Fehler beim Laden der Daten');
					console.log('Meine Schnelleinstiege: Fehler beim Laden der Daten');
                }));

            }), Function.createDelegate(data, function (e1, e2) {
                //util.ShowError('Meine Schnelleinstiege: Fehler bei der Ermittlung des Benutzers');
				console.log('Meine Schnelleinstiege: Fehler bei der Ermittlung des Benutzers');
            }));
        });
    }

    server.getTools = function (callback) {
        SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function () {
            var clientContext = getRootClientContext();
            var web = clientContext.get_web();
            var lists = clientContext.get_web().get_lists();

            var listTools = lists.getByTitle('Systeme & Tools');
            var camlTools = new SP.CamlQuery();

            var data = {
                callback: callback
            };
            data.tools = listTools.getItems(camlTools);
            clientContext.load(data.tools);

            clientContext.executeQueryAsync(Function.createDelegate(data, function (sender, args) {
                var data = this;

                function Tool(item) {
                    this.Id = item.get_id();
                    this.Anzeigename = item.get_item('Title');
                    this.Link = item.get_item('Link'); if (this.Link && this.Link.get_url) { this.Link = this.Link.get_url(); }
                    this.Icon = item.get_item('Icon'); if (this.Icon && this.Icon.get_url) { this.Icon = this.Icon.get_url(); }
                    this.HoverIcon = item.get_item('HoverIcon'); if (this.HoverIcon && this.HoverIcon.get_url) { this.HoverIcon = this.HoverIcon.get_url(); }
                }
                var tools = [];
                var toolsEnum = data.tools.getEnumerator();
                while (toolsEnum.moveNext()) {
                    tools.push(new Tool(toolsEnum.get_current()));
                }

                data.callback(tools);

            }), Function.createDelegate(data, function (sender, args) {
                //util.ShowError('Fehler beim Laden der Daten');
				console.log('Fehler beim Laden der Daten');
            }));
        });
    }

    function getRootClientContext() {
        var clientContext = new SP.ClientContext.get_current();
        if (clientContext.get_url() != config.rootUrl) {
            clientContext = new SP.ClientContext(config.rootUrl);
        }
        return clientContext;
    }

    function getServiceUrl(action) {
        return util.getSlashedRootUrl() + "_layouts/15/services/nteam/MeineSchnelleinstiege/MeineSchnelleinstiegeService.asmx/" + action
    }

    function callService(action, data, successCallback, errorMessage)
    {
        $.ajax({
            url: getServiceUrl(action),
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: data ? JSON.stringify(data) : undefined,
            success: function (data) {
                var data = data ? data.d : data;
                if (successCallback) {
                    successCallback(data);
                }
            },
            error: function (err) {
                if (errorMessage) {
                    util.ShowError(errorMessage);
                } else {
                    util.ShowError('Bei der Verarbeitung ist ein Fehler aufgetreten');
                }
            }
        });
    }
})();