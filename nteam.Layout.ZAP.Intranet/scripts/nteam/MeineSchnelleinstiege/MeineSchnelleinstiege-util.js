﻿(function () {
    "use strict"

    var meineSchnelleinstiege = window.nteam.MeineSchnelleinstiege;
    meineSchnelleinstiege.util = {};
    var util = meineSchnelleinstiege.util;
    var config = meineSchnelleinstiege.config;


    util.isEmptyOrSpaces = function(str) {
        return !str || str.match(/^ *$/) !== null;
    }

    util.ShowSuccess = function (message) {
        if(message != "") { SP.UI.Notify.addNotification($('#notification_Success').text().format(message), false); }
    }

    util.ShowError = function (message) {
		if(message != "") { SP.UI.Notify.addNotification($('#notification_Error').text().format(message), true); }
    }


    util.addParameterStringToUrl = function (url, parameterString) {
        if (url.indexOf('?') > -1) {
            return url + '&' + parameterString;
        } else {
            return url + '?' + parameterString;
        }
    }

    util.getSlashedUrl = function (url) {
        return url.charAt(url.length - 1) == '/' ? url : url + '/';
    }

    util.getSlashedWebServerRelativeUrl = function() {
        var url = _spPageContextInfo.webServerRelativeUrl;
        if (!url || url.length == 0) {
            return '/';
        }
        return util.getSlashedUrl(url);
    }

    util.getSlashedRootUrl = function () {
        return window.location.protocol + '//' + window.location.host + util.getSlashedUrl(config.rootUrl);
    }


    // -- knockoutJs custom Extensions -- \\
    window.ko.bindingHandlers.hover = {
        init: function (element, valueAccessor) {
            var value = valueAccessor();
            ko.applyBindingsToNode(element, {
                event: {
                    mouseenter: function () { value(true) },
                    mouseleave: function () { value(false) }
                }
            });
        }
    }


    // -- Custom extensions -- \\
    // String.format
    if (!String.prototype.format) {
        String.prototype.format = function () {
            var args = arguments;
            return this.replace(/{(\d+)}/g, function (match, number) {
                return typeof args[number] != 'undefined'
                  ? args[number]
                  : match
                ;
            });
        };
    }
    // String.startsWith
    if (!String.prototype.startsWith) {
        String.prototype.startsWith = function (searchString, position) {
            position = position || 0;
            return this.indexOf(searchString, position) === position;
        };
    }
})();