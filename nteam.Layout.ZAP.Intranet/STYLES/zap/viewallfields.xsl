﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2004/07/xpath-functions" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime">

  <xsl:output method="html" indent="yes"/>

  <xsl:template match="/">
    <xsl:apply-templates mode="content" select="/dsQueryResponse/Rows/Row"/>
  </xsl:template>
  <xsl:template match="/dsQueryResponse/Rows/Row" mode="content">

    <p>
      <xsl:for-each select="@*">
        <xsl:value-of select="name()"/>
        <xsl:text> = </xsl:text>
        <xsl:value-of select="."/>
        <br/>
      </xsl:for-each>
    </p>

  </xsl:template>

</xsl:stylesheet>