﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2004/07/xpath-functions" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime"

  xmlns:x="http://www.w3.org/2001/XMLSchema"
  xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
  xmlns:cmswrt="http://schemas.microsoft.com/WebParts/v3/Publishing/runtime"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt">

  <xsl:output method="html" indent="yes"/>

  <xsl:template match="/">
    <style type="text/css">

    </style>

    <xsl:apply-templates mode="content" select="/dsQueryResponse/Rows/Row"/>
  </xsl:template>
  <xsl:template match="/dsQueryResponse/Rows/Row" mode="content">

    <xsl:variable name="ListURL">
      <xsl:value-of select="substring-before(@FileRef,@FileLeafRef)"/>
    </xsl:variable>
    <xsl:variable name="PageURL">
      FileRef <xsl:value-of select="@FileRef"/> FileLeafRef <xsl:value-of select="@FileLeafRef"/> FileLeafRef.Name <xsl:value-of select="@FileLeafRef.Name"/> ID <xsl:value-of select="@ID"/>
    </xsl:variable>

    <xsl:variable name="EventDateHelper">
      <xsl:value-of select="number(ddwrt:FormatDateTime(ddwrt:FormatDate(string(@EndDate),1033,1),1033,'yyyyMMdd'))"/> - <xsl:value-of select="ddwrt:FormatDateTime(ddwrt:FormatDate(string(@EventDate),1031,1),1031,'d MMMM yyyy')"/>
    </xsl:variable>
    <xsl:variable name="EventDay">
      <xsl:value-of select="ddwrt:FormatDateTime(ddwrt:FormatDate(string(@EventDate),1031,1),1031,'dd')"/>
    </xsl:variable>
    <xsl:variable name="EventMonth">
      <xsl:value-of select="ddwrt:FormatDateTime(ddwrt:FormatDate(string(@EventDate),1031,1),1031,'MMM')"/>
    </xsl:variable>
    <xsl:variable name="EventYear">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@EventDate) ,1031 ,'yyyy')" />
    </xsl:variable>
    <xsl:variable name="Beginn">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@EventDate) ,1031 ,'HH:mm')" />
    </xsl:variable>
    <xsl:variable name="Ende">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@EndDate) ,1031 ,'HH:mm')" />
    </xsl:variable>
    <xsl:variable name="TerminBeschreibung">
      <xsl:value-of select="substring-after(substring-before(@Description,'&#60;&#47;'),'&#62;')" disable-output-escaping="yes" />
    </xsl:variable>
    <xsl:variable name="URLTitle">
      <xsl:value-of select="substring-after(@URL,', ')"/>
    </xsl:variable>
    <xsl:variable name="URLHref">
      <xsl:value-of select="substring-before(@URL,', ')"/>
    </xsl:variable>


    <!-- This markup is used for each item in the list -->

    <div class="ntm_Termin">
      <div class="ntm_DateBox">
        <div class="ntm_DateBoxDay">
          <xsl:value-of select="$EventDay"/>
        </div>
        <div class="ntm_DateBoxMonth">
          <div>
            <xsl:value-of select="$EventMonth"/>
          </div>
        </div>
      </div>
      <div class="ntm_TitelAuthorHold">
        <div class="ntm_Zeitraum">
          <xsl:choose>
            <xsl:when test="@fAllDayEvent = 'Ja'" >ganzt&#228;gig</xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$Beginn"/> - <xsl:value-of select="$Ende"/> Uhr
            </xsl:otherwise>
          </xsl:choose>
          <xsl:choose>
            <xsl:when test="@fRecurrence = 1">
              <img src="/_layouts/15/images/recur.gif" style="vertical-align: bottom;" />
            </xsl:when>
            <xsl:otherwise>
            </xsl:otherwise>
          </xsl:choose>
        </div>
        <div class="ntm_TerminTitle">
          <a href="{$ListURL}DispForm.aspx?ID={@ID}" title="{$TerminBeschreibung}" class="ntm_TermineTitleLink">
            <xsl:value-of select="@Title"/>
          </a>
			<xsl:if test="string-length(@Category) != 0"><span class="ntm_Kategorie">&#160;(<xsl:value-of select="@Category"/>)</span></xsl:if>
		</div>
        <xsl:if test="string-length(@Location) != 0">
          <div class="ntm_Description">
			<a href="{$ListURL}DispForm.aspx?ID={@ID}" title="{$TerminBeschreibung}" class="ntm_TermineTitleLink">
				<xsl:value-of select="@Location"/>
			</a>
          </div>
        </xsl:if>
        <xsl:if test="string-length(@URL) != 0">
          <div class="ntm_MehrLink">
			<a href="{@URL}" title="{@Title}" class="ntm_MehrLink ntm_TermineTitleLink" target="_blank">
				Weitere Informationen
			</a>
          </div>
        </xsl:if>

        <div style="display:none;">
          fRecurrence: <xsl:value-of select="@fRecurrence"/><br/>
          fAllDayEvent: <xsl:value-of select="@fAllDayEvent"/><br/>
        </div>
      </div>
    </div>

  </xsl:template>


</xsl:stylesheet>