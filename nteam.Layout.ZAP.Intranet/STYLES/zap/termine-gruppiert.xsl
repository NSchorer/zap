﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2004/07/xpath-functions" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime">

  <xsl:output method="html" indent="yes"/>

  <xsl:key name="group-by-level-1" match="Row" use="@Category"/>

  <xsl:template match="/">

    <div class="ntm_TermineGruppe ntm_ReiterCont">
      <ul id="GruppierungEvents" class="ntm_ReiterUL">
        <xsl:apply-templates mode="headerlevel1" select="//Row[count(. | key('group-by-level-1', @Category)[1]) = 1]">
          <xsl:sort select="@Category"/>
        </xsl:apply-templates>
      </ul>
    </div>

    <style type="text/css">


    </style>
    <script type="text/javascript">
      $(document).ready(function(){
      // Reiter aktiv setzen
      $('#GruppierungEvents li a').bind('click', function(e) {
      $('#GruppierungEvents li a').each(function(i) {
      $(this).removeClass("ntm_Active");
      $(this).next().attr("class", "ntm_hide ntm_Absolut");
      //$(this).css("background-color","orange");
      //$(this).delay(i*200).fadeOut(1500);
      });
      $(this).addClass("ntm_Active");
      $(this).next().attr("class", "ntm_show ntm_Absolut");
      });
      });
    </script>
  </xsl:template>

  <xsl:template match="Row" mode="headerlevel1">
    <li class="ntm_Reiter kategorie-{@Category}">
      <a href="#" class="ntm_ReiterALbl">
        <xsl:if test="position() = 1">
          <xsl:attribute name="class">ntm_ReiterALbl ntm_Active</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="@Category"/>
      </a>
      <div id="{@Category}" class="ntm_hide ntm_Absolut">
        <xsl:if test="position() = 1">
          <xsl:attribute name="target">
            <xsl:value-of select="@Category"/>
          </xsl:attribute>
          <xsl:attribute name="class">ntm_Absolut</xsl:attribute>
        </xsl:if>
        <xsl:apply-templates mode="content" select="key('group-by-level-1',@Category)">

        </xsl:apply-templates>
      </div>
    </li>
  </xsl:template>


  <xsl:template match="Row" mode="content">

    <xsl:variable name="ListURL">
      <xsl:value-of select="substring-before(@FileRef,@FileLeafRef)"/>
    </xsl:variable>
    <xsl:variable name="PageURL">
      FileRef <xsl:value-of select="@FileRef"/> FileLeafRef <xsl:value-of select="@FileLeafRef"/> FileLeafRef.Name <xsl:value-of select="@FileLeafRef.Name"/> ID <xsl:value-of select="@ID"/>
    </xsl:variable>

    <xsl:variable name="EventDateHelper">
      <xsl:value-of select="number(ddwrt:FormatDateTime(ddwrt:FormatDate(string(@EndDate),1033,1),1033,'yyyyMMdd'))"/> - <xsl:value-of select="ddwrt:FormatDateTime(ddwrt:FormatDate(string(@EventDate),1031,1),1031,'d MMMM yyyy')"/>
    </xsl:variable>
    <xsl:variable name="EventDay">
      <xsl:value-of select="ddwrt:FormatDateTime(ddwrt:FormatDate(string(@EventDate),1031,1),1031,'dd')"/>
    </xsl:variable>
    <xsl:variable name="EventMonth">
      <xsl:value-of select="ddwrt:FormatDateTime(ddwrt:FormatDate(string(@EventDate),1031,1),1031,'MMM')"/>
    </xsl:variable>
    <xsl:variable name="EventYear">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@EventDate) ,1031 ,'yyyy')" />
    </xsl:variable>
    <xsl:variable name="Beginn">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@EventDate) ,1031 ,'HH:mm')" />
    </xsl:variable>
    <xsl:variable name="Ende">
      <xsl:value-of select="ddwrt:FormatDateTime(string(@EndDate) ,1031 ,'HH:mm')" />
    </xsl:variable>
    <xsl:variable name="TerminBeschreibung">
      <xsl:value-of select="substring-after(substring-before(@Description,'&#60;&#47;'),'&#62;')" disable-output-escaping="yes" />
    </xsl:variable>


    <!-- This markup is used for each item in the list -->

    <div class="ntm_Termin">
      <div class="ntm_DateBox">
        <div class="ntm_DateBoxMonth">
          <div>
            <xsl:value-of select="$EventMonth"/>
          </div>
        </div>
        <div class="ntm_DateBoxDay">
          <xsl:value-of select="$EventDay"/>
        </div>
        <div class="ntm_DateBoxYear">
          <xsl:value-of select="$EventYear"/>
        </div>
      </div>
      <div class="ntm_TitelAuthorHold">
        <div class="ntm_Zeitraum">
          <xsl:choose>
            <xsl:when test="@fAllDayEvent = 'Ja'" >ganzt&#228;gig</xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$Beginn"/> - <xsl:value-of select="$Ende"/> Uhr
            </xsl:otherwise>
          </xsl:choose>
          <xsl:choose>
            <xsl:when test="@fRecurrence = 1">
              <img src="/_layouts/15/images/recur.gif" alt="Serientermin" style="vertical-align: bottom;" />
            </xsl:when>
            <xsl:otherwise>
            </xsl:otherwise>
          </xsl:choose>
        </div>
        <div class="ntm_TerminTitle">
          <a href="{$ListURL}DispForm.aspx?ID={@ID}&#38;Source=/strato/" title="{$TerminBeschreibung}" class="ntm_TermineTitleLink">
            <xsl:value-of select="@Title"/>
          </a>
        </div>
        <xsl:if test="string-length(@Location) != 0">
          <div class="ntm_Description">
            <div class="ntm_Label">Ort: </div>
            <xsl:value-of select="@Location"/>
          </div>
        </xsl:if>
        <xsl:if test="string-length(@Art) != 0">
          <div class="ntm_Description">
            <div class="ntm_Label">Art: </div>
            <xsl:value-of select="@Art"/>
          </div>
        </xsl:if>
        <div style="display:none;">
          fRecurrence: <xsl:value-of select="@fRecurrence"/><br/>
          fAllDayEvent: <xsl:value-of select="@fAllDayEvent"/><br/>
        </div>
      </div>
    </div>

  </xsl:template>


</xsl:stylesheet>