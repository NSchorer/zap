﻿<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2004/07/xpath-functions" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime"
	xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" exclude-result-prefixes="xsl msxsl ddwrt" xmlns:msxsl="urn:schemas-microsoft-com:xslt" >
  <xsl:include href="/_layouts/15/xsl/main.xsl" />
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" />
  <xsl:output method='html' indent='yes' />

  <xsl:template match="/" >
    <style type="text/css">
      .ms-webpartzone-cell {
      margin-bottom	: 0px;
      }
    </style>

    <script type="text/javascript">

    </script>
    <xsl:variable name="Rows" select="/dsQueryResponse/Rows/Row[$EntityName = '' or (position() &gt;= $FirstRow and position() &lt;= $LastRow)]" />
    <div class="ntm_GruppeFAQ">
      <xsl:for-each select="$Rows">
        <xsl:call-template name="RowView"></xsl:call-template>
      </xsl:for-each>
    </div>

    <xsl:call-template name="pagingButtons" />
  </xsl:template>

  <xsl:template name="RowView">
    <xsl:variable name="thisNode" select="." />
    <h3 class="ntm_AkkordeonTitle">
      <div class="ntm_OpenIco">
        <img src="/_layouts/15/images/blank.gif" />
      </div>
      <xsl:value-of select="@Title"/>
    </h3>
    <div class="ntm_AkkordeonText">
      <div class="ntm_Beschreibung">
        <xsl:value-of select="@Antwort" disable-output-escaping="yes"/>
      </div>
    </div>
  </xsl:template>

  <xsl:template name="pagingButtons">
    <xsl:choose>
      <xsl:when test="$XmlDefinition/List/@TemplateType = 106 and $XmlDefinition/@RecurrenceRowset='TRUE'">
        <xsl:if test="$dvt_nextpagedata or $dvt_prevpagedata">
          <xsl:call-template name="CalendarExpandedRecurrenceFooter"/>
        </xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="$XmlDefinition/RowLimit[@Paged='TRUE']">
          <xsl:call-template name="CommandFooter">
            <xsl:with-param name="FirstRow" select="$FirstRow" />
            <xsl:with-param name="LastRow" select="$LastRow" />
            <xsl:with-param name="dvt_RowCount" select="$dvt_RowCount" />
          </xsl:call-template>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="CommandFooter">
    <xsl:param name="FirstRow" select="1"/>
    <xsl:param name="LastRow" select="1"/>
    <xsl:param name="dvt_RowCount" select="1"/>
    <xsl:if test="$FirstRow &gt; 1 or $dvt_nextpagedata">
      <xsl:call-template name="Navigation">
        <xsl:with-param name="FirstRow" select="$FirstRow" />
        <xsl:with-param name="LastRow" select="$LastRow" />
        <xsl:with-param name="dvt_RowCount" select="$dvt_RowCount" />
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="Navigation">
    <xsl:param name="FirstRow" select="1"/>
    <xsl:param name="LastRow" select="1"/>
    <xsl:param name="dvt_RowCount" select="1"/>
    <xsl:variable name="LastRowValue">
      <xsl:choose>
        <xsl:when test="$EntityName = '' or $LastRow &lt; $RowTotalCount">
          <xsl:value-of select="$LastRow"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$RowTotalCount"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="NextRow">
      <xsl:value-of select="$LastRowValue + 1"/>
    </xsl:variable>

    <div id="bottomPagingCell{$WPQ}" class="ntm_PagingCont">
      <div class="ntm_PagingLeft">
        <xsl:if test="$dvt_firstrow &gt; 1">
          <a>
            <xsl:choose>
              <xsl:when test="$dvt_RowCount = 0 and not($NoAJAX)">
                <xsl:attribute name="onclick">
                  javascript:RefreshPageTo(event, "<xsl:value-of select="$PagePath"/>?<xsl:value-of select="$ShowWebPart"/>\u0026<xsl:value-of select='$FieldSortParam'/><xsl:value-of select='$SortQueryString'/>\u0026View=<xsl:value-of select="$View"/>");javascript:return false;
                </xsl:attribute>
                <xsl:attribute name="href">javascript:</xsl:attribute>
              </xsl:when>
              <xsl:otherwise>
                <xsl:variable name="RealRowLimit">
                  <xsl:choose>
                    <xsl:when test="$XmlDefinition/Query/GroupBy[@Collapse='TRUE']/@GroupLimit">
                      <xsl:value-of select ="$XmlDefinition/Query/GroupBy[@Collapse='TRUE']/@GroupLimit"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select = "$XmlDefinition/RowLimit"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:variable>
                <xsl:choose>
                  <xsl:when test="not($NoAJAX)">
                    <xsl:attribute name="onclick">
                      javascript:RefreshPageTo(event, "<xsl:value-of select="$PagePath"/>?<xsl:value-of select="$dvt_prevpagedata"/><xsl:value-of select="$ShowWebPart"/>\u0026PageFirstRow=<xsl:value-of select="$FirstRow - $RealRowLimit"/>\u0026<xsl:value-of select='$FieldSortParam'/><xsl:value-of select='$SortQueryString'/>\u0026View=<xsl:value-of select="$View"/>");javascript:return false;
                    </xsl:attribute>
                    <xsl:attribute name="href">javascript:</xsl:attribute>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:attribute name="href">
                      javascript: <xsl:call-template name="GenFireServerEvent">
                        <xsl:with-param name="param" select="concat('dvt_firstrow={',$FirstRow - $XmlDefinition/RowLimit,'};dvt_startposition={',$dvt_prevpagedata,'}')"/>
                      </xsl:call-template>
                    </xsl:attribute>
                  </xsl:otherwise>
                </xsl:choose>
                <span class="ms-rteStyle-Back">zurück</span>
              </xsl:otherwise>
            </xsl:choose>
          </a>
        </xsl:if>
      </div>

      <div class="ntm_PagingRight">

        <xsl:if test="$LastRowValue &lt; $dvt_RowCount or string-length($dvt_nextpagedata)!=0">
          <a>
            <xsl:choose>
              <xsl:when test="not($NoAJAX)">
                <xsl:attribute name="onclick">
                  javascript:RefreshPageTo(event, "<xsl:value-of select="$PagePath"/>?<xsl:value-of select="$dvt_nextpagedata"/><xsl:value-of select="$ShowWebPart"/>\u0026PageFirstRow=<xsl:value-of select="$NextRow"/>\u0026<xsl:value-of select='$FieldSortParam'/><xsl:value-of select='$SortQueryString'/>\u0026View=<xsl:value-of select="$View"/>");javascript:return false;
                </xsl:attribute>
                <xsl:attribute name="href">javascript:</xsl:attribute>
              </xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="href">
                  javascript: <xsl:call-template name="GenFireServerEvent">
                    <xsl:with-param name="param" select="concat('dvt_firstrow={',$NextRow,'};dvt_startposition={',$dvt_nextpagedata,'}')"/>
                  </xsl:call-template>
                </xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>
            <span class="ms-rteStyle-Mehr">weiter</span>
          </a>
        </xsl:if>
      </div>
    </div>

    <xsl:if test="not($GroupingRender)">
      <script>
          var topPagingCell = document.getElementById(&quot;topPagingCell<xsl:value-of select="$WPQ" />&quot;);
          var bottomPagingCell = document.getElementById(&quot;bottomPagingCell<xsl:value-of select="$WPQ" />&quot;);
          if (topPagingCell != null &amp;&amp; bottomPagingCell != null)
          {
              topPagingCell.innerHTML = bottomPagingCell.innerHTML;
          }
      </script>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>


