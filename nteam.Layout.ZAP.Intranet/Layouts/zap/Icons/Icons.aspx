<%@ Assembly Name="Microsoft.SharePoint.ApplicationPages, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@ Page Language="C#" DynamicMasterPageFile="~masterurl/default.master" Inherits="Microsoft.SharePoint.WebControls.LayoutsPageBase" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="wssawc" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content ID="Content1" ContentPlaceHolderId="PlaceHolderPageTitle" runat="server">
	<SharePoint:EncodedLiteral ID="EncodedLiteral1" runat="server" text="Icons" EncodeMethod='HtmlEncode'/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderId="PlaceHolderPageTitleInTitleArea" runat="server">Icons</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderId="PlaceHolderAdditionalPageHead" runat="server">
	<link rel="shortcut icon" href="/_layouts/15/images/ZAP/favicon.ico" id="favicon" />
	<link rel="stylesheet" type="text/css" href="/_layouts/15/styles/ZAP/general.css" />
<style type="text/css">
	.ntm-Box {
		float: left;
		height: 100px;	
		margin-bottom: 1em;
		margin-right: 1em;
		text-align: center;	
		width: 100px;
	}
	.ntm-Box div {
		margin-top: 41px;	
	}
	.ntm-Farbe .xxx {
		background-color: rgba(178,178,178,.5);
	}
	.ntm-padd {
		padding-left: 20px;
	}
	.ntm_KachelnNorm.boxgrid,
	.ntm_KachelnNorm.boxgrid img, .ntm_KachelnNorm.boxgrid .ntm_CtIcon {
		width: 410px !important;
	}
	.ntm_KachelnNorm .boxcaption, .ntm_KachelnNorm .ntm_BoxCT,
	.ntm_KachelnNorm .ntm_BoxTitle	{
		width: 350px !important;
	}
	.ms-rteStyle-BoxClearing {
		border-top		: 0px solid transparent;
		background-color: transparent;
		height			: 0px;
		clear			: both;
	}
	table.ntm_FloatingTable {}
	table.ntm_FloatingTable tr {
		display			: inline-block;
		width			: 410px;
		height			: 90px;
		float			: left;
		margin-right	: 30px;
		margin-bottom	: 20px;
	}
	table.ntm_FloatingTable td {
		width			: 350px;
		height			: 90px;
		background-color: rgba(178,178,178,.5);
	}
	table.ntm_FloatingTable tr td:first-child {
		width			: 60px;
	}

</style>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderId="PlaceHolderPageDescription" runat="server">
	<asp:Label id="LabelPageDescription" runat="server"/>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderId="PlaceHolderMain" runat="server">
<a name="oben"></a>


	<h1><a name="iconsn"></a>Office SVG Icons</h1>

<div class="ntm_KachelnNorm boxgrid " title="AGG">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/AGG.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/AGG.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/AGG.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/AGG.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="AGG_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/AGG_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/AGG_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/AGG_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/AGG_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Azubis">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Azubis.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Azubis.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Azubis.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Azubis.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Azubis_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Azubis_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Azubis_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Azubis_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Azubis_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Buerostuhl">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Buerostuhl.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Buerostuhl.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Buerostuhl.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Buerostuhl..svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Buerostuhl_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Buerostuhl_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Buerostuhl_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Buerostuhl_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Buerostuhl_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Compliance">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/compliance.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/compliance.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/compliance.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/compliance..svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Compliance_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/compliance_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/compliance_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/compliance_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/compliance_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Drucker">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Drucker.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Drucker.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Drucker.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Drucker..svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Drucker_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Drucker_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Drucker_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Drucker_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Drucker_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Gluehbirne">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Gluehbirne.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Gluehbirne.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Gluehbirne.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Gluehbirne.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Gluehbirne_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Gluehbirne_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Gluehbirne_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Gluehbirne_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Gluehbirne_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="idee">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/idee.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/idee.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/idee.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/idee.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="idee_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/idee_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/idee_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/idee_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/idee_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Menu">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/menu.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/menu.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/menu.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/menu.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Menu_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/menu_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/menu_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/menu_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/menu_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Netzwerk">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Netzwerk.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Netzwerk.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Netzwerk.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Netzwerk.svg</div></div>
		</div>
	</div>
</div><div class="ntm_KachelnNorm boxgrid " title="Netzwerk_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Netzwerk_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Netzwerk_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Netzwerk_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Netzwerk_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Personal">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/personal.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/personal.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/personal.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/personal.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Personal_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/personal_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/personal_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/personal_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/personal_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Stuhl">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Stuhl.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Stuhl.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Stuhl.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Stuhl..svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Stuhl_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Stuhl_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Stuhl_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Stuhl_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Stuhl_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="VisionMission">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/VisionMission.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/VisionMission.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/VisionMission.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/VisionMission.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="VisionMission_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/VisionMission_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/VisionMission_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/VisionMission_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/VisionMission_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Zusammenarbeit">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Zusammenarbeit.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Zusammenarbeit.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Zusammenarbeit.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Zusammenarbeit.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Zusammenarbeit_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Zusammenarbeit_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Zusammenarbeit_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Zusammenarbeit_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Zusammenarbeit_w.svg</div></div>
		</div>
	</div>
</div>

	<span class="ms-rteStyle-BoxClearing"> </span>
	<p>&nbsp;</p><p><a href="#oben">nach oben</a></p>

    <h1><a name="iconsn"></a>Allgemeine SVG Icons</h1>
	
<div class="ntm_KachelnNorm boxgrid " title="add-users">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/add-users.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/add-users.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/add-users.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/add-users.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="add-users_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/add-users_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/add-users_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/add-users_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/add-users_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="alphabetical-order">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/alphabetical-order.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/alphabetical-order.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/alphabetical-order.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/alphabetical-order.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="alphabetical-order_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/alphabetical-order_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/alphabetical-order_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/alphabetical-order_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/alphabetical-order_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="picture">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/picture.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/picture.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/picture.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/picture.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="picture_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/picture_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/picture_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/picture_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/picture_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Bildarchiv">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/bildarchiv.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/bildarchiv.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/bildarchiv.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/bildarchiv.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Bildarchiv_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/bildarchiv_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/bildarchiv_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/bildarchiv_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/bildarchiv_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Media">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/media.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/media.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/media.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/media.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Media_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/media_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/media_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/media_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/media_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="bookmark">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/bookmark.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/bookmark.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/bookmark.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/bookmark.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="bookmark_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/bookmark_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/bookmark_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/bookmark_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/bookmark_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Check">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/check.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/check.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/check.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/check.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Check_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/check_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/check_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/check_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/check_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="check1">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/check1.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/check1.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/check1.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/check1.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="check1_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/check1_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/check1_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/check1_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/check1_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="check2">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/check2.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/check2.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/check2.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/check2.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="check2_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/check2_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/check2_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/check2_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/check2_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Diagramm">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/diagramm.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/diagramm.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/diagramm.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/diagramm.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Diagramm_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/diagramm_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/diagramm_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/diagramm_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/diagramm_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Graph">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/graph.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/graph.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/graph.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/graph.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Graph_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/graph_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/graph_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/graph_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/graph_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="graph2">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/graph2.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/graph2.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/graph2.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/graph2.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="graph2_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/graph2_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/graph2_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/graph2_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/graph2_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Kunst">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/kunst.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/kunst.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/kunst.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/kunst.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Kunst_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/kunst_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/kunst_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/kunst_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/kunst_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="lupe">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/lupe.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/lupe.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/lupe.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/lupe.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="lupe_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/lupe_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/lupe_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/lupe_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/lupe_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="pfeil-rechts">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/pfeil-rechts.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/pfeil-rechts.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/pfeil-rechts.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/pfeil-rechts.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="pfeil-rechts_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/pfeil-rechts_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/pfeil-rechts_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/pfeil-rechts_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/pfeil-rechts_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="pfeil-rechts-doppelt">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/pfeil-rechts-doppelt.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/pfeil-rechts-doppelt.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/pfeil-rechts-doppelt.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/pfeil-rechts-doppelt.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="pfeil-rechts-doppelt_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/pfeil-rechts-doppelt_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/pfeil-rechts-doppelt_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/pfeil-rechts-doppelt_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/pfeil-rechts-doppelt_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Raumplaene">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Raumplaene.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Raumplaene.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Raumplaene.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Raumplaene.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Raumplaene_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Raumplaene_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Raumplaene_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Raumplaene_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Raumplaene_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="service">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/service.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/service.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/service.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/service.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="service_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/service_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/service_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/service_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/service_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Speiseplan">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/speiseplan.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/speiseplan.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/speiseplan.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/speiseplan.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Speiseplan_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/speiseplan_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/speiseplan_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/speiseplan_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/speiseplan_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="stern">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/stern.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/stern.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/stern.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/stern.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="stern_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/stern_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/stern_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/stern_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/stern_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="suche">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/suche.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/suche.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/suche.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/suche.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="suche_gruen">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/suche_gruen.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/suche_gruen.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/suche_gruen.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/suche_gruen.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="suche_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/suche_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/suche_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/suche_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/suche_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Wegweiser">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/wegweiser.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/wegweiser.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/wegweiser.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/wegweiser.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Wegweiser_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/wegweiser_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/wegweiser_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/wegweiser_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/wegweiser_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Zahnrad">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Zahnrad.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Zahnrad.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Zahnrad.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Zahnrad.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Zahnrad_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Zahnrad_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Zahnrad_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Zahnrad_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Zahnrad_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="prozesse">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/prozesse.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/prozesse.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/prozesse.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/prozesse.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="prozesse_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/prozesse_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/prozesse_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/prozesse_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/prozesse_w.svg</div></div>
		</div>
	</div>
</div>

	<span class="ms-rteStyle-BoxClearing"> </span>
	<p>&nbsp;</p><p><a href="#oben">nach oben</a></p>

	
	<h1><a name="iconsn"></a>Dokumente SVG Icons</h1>

<div class="ntm_KachelnNorm boxgrid " title="accept-file">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/accept-file.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/accept-file.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/accept-file.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/accept-file.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="accept-file_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/accept-file_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/accept-file_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/accept-file_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/accept-file_w.svg</div></div>
		</div>
	</div>
</div>	
<div class="ntm_KachelnNorm boxgrid " title="Bearbeiten">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/bearbeiten.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/bearbeiten.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/bearbeiten.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/bearbeiten.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Bearbeiten_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/bearbeiten_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/bearbeiten_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/bearbeiten_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/bearbeiten_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Bewerbung">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Bewerbung.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Bewerbung.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Bewerbung.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Bewerbung.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Bewerbung_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Bewerbung_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Bewerbung_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Bewerbung_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Bewerbung_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Brief">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Brief.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Brief.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Brief.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Brief.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Brief2">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Brief2.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Brief2.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Brief2.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Brief2.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Brief2_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Brief2_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Brief2_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Brief2_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Brief2_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Briefmarke">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Briefmarke.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Briefmarke.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Briefmarke.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Briefmarke.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Briefmarke_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Briefmarke_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Briefmarke_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Briefmarke_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Briefmarke_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Broschuere">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/broschuere.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/broschuere.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/broschuere.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/broschuere.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Broschuere_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/broschuere_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/broschuere_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/broschuere_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/broschuere_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Buch">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/buch.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/buch.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/buch.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/buch.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Buch_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/buch_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/buch_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/buch_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/buch_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Buch_offen">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/buch_offen.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/buch_offen.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/buch_offen.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/buch_offen.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Buch_offen_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/buch_offen_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/buch_offen_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/buch_offen_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/buch_offen_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Buecher">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Buecher.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Buecher.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Buecher.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Buecher.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Buecher_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Buecher_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Buecher_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Buecher_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Buecher_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="checkliste">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/checkliste.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/checkliste.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/checkliste.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/checkliste.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="checkliste_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/checkliste_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/checkliste_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/checkliste_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/checkliste_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Design">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/design.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/design.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/design.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/design.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Design_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/design_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/design_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/design_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/design_w.svg</div></div>
		</div>
	</div>
</div>

<div class="ntm_KachelnNorm boxgrid " title="Dokument">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/dokument.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/dokument.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/dokument.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/dokument.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Dokument_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/dokument_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/dokument_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/dokument_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/dokument_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Dokument_leer">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/dokument_leer.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/dokument_leer.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/dokument_leer.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/dokument_leer.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Dokument_leer_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/dokument_leer_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/dokument_leer_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/dokument_leer_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/dokument_leer_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Dokument_sicher">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/dokument_sicher.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/dokument_sicher.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/dokument_sicher.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/dokument_sicher.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Dokument_sicher_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/dokument_sicher_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/dokument_sicher_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/dokument_sicher_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/dokument_sicher_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Eintrag">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/eintrag.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/eintrag.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/eintrag.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/eintrag.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Eintrag_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/eintrag_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/eintrag_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/eintrag_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/eintrag_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="floppy-disk">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/floppy-disk.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/floppy-disk.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/floppy-disk.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/floppy-disk.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="floppy-disk_gruen">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/floppy-disk_gruen.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/floppy-disk_gruen.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/floppy-disk_gruen.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/floppy-disk_gruen.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="floppy-disk_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/floppy-disk_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/floppy-disk_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/floppy-disk_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/floppy-disk_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Gesetz">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/gesetz.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/gesetz.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/gesetz.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/gesetz.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Gesetz_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/gesetz_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/gesetz_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/gesetz_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/gesetz_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Glossar">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/glossar.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/glossar.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/glossar.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/glossar.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Glossar_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/glossar_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/glossar_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/glossar_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/glossar_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Ordner">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/ordner.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/ordner.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/ordnert.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/ordner.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Ordner_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/ordner_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/ordner_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/ordner_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/ordner_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Neuer_Ordner">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/neuer_ordner.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/neuer_ordner.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/neuer_ordnert.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/neuer_ordner.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Neuer_Ordner_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/neuer_ordner_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/neuer_ordner_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/neuer_ordner_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/neuer_ordner_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="news">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/news.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/news.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/news.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/news.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="news_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/news_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/news_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/news_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/news_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="newsletter">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/newsletter.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/newsletter.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/newsletter.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/newsletter.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="newsletter_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/newsletter_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/newsletter_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/newsletter_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/newsletter_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Postkarte">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Postkarte.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Postkarte.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Postkarte.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Postkarte.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Postkarte_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Postkarte_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Postkarte_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Postkarte_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Postkarte_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="saving-disk">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/saving-disk.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/saving-disk.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/saving-disk.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/saving-disk.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="saving-disk_gruen">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/saving-disk_gruen.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/saving-disk_gruen.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/saving-disk_gruen.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/saving-disk_gruen.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="saving-disk_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/saving-disk_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/saving-disk_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/saving-disk_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/saving-disk_w.svg</div></div>
		</div>
	</div>
</div>

	<span class="ms-rteStyle-BoxClearing"> </span>
	<p>&nbsp;</p><p><a href="#oben">nach oben</a></p>
	
	<h1><a name="iconsn"></a>Internet SVG Icons</h1>
	
<div class="ntm_KachelnNorm boxgrid " title="at">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/at.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/at.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/at.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/at.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="at_rot">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/at_rot.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/at_rot.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/at_rot.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/at_rot.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="at_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/at_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/at_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/at_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/at_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="at-sign">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/at-sign.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/at-sign.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/at-sign.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/at-sign.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="cloud">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/cloud.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/cloud.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/cloud.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/cloud.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="cloud_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/cloud_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/cloud_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/cloud_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/cloud_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="download">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/download.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/download.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/download.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/download.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="download_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/download_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/download_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/download_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/download_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Hyperlink">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/hyperlink.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/hyperlink.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/hyperlink.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/hyperlink.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Hyperlink_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/hyperlink_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/hyperlink_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/hyperlink_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/hyperlink_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/mail.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/mail.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/mail.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/mail.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="mail_rot">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/mail_rot.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/mail_rot.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/mail_rot.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/mail_rot.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="mail_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/mail_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/mail_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/mail_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/mail_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="upload">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/upload.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/upload.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/upload.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/upload.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="upload_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/upload_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/upload_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/upload_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/upload_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="worldwide">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/worldwide.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/worldwide.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/worldwide.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/worldwide.svg</div></div>
		</div>
	</div>
</div>

	<span class="ms-rteStyle-BoxClearing"> </span>
	<p>&nbsp;</p><p><a href="#oben">nach oben</a></p>
	
	<h1><a name="iconsn"></a>Sicherheit SVG Icons</h1>
	
<div class="ntm_KachelnNorm boxgrid " title="Arbeitssicherheit">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Arbeitssicherheit.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Arbeitssicherheit.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Arbeitssicherheit.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Arbeitssicherheit.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Arbeitssicherheit_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Arbeitssicherheit_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Arbeitssicherheit_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Arbeitssicherheit_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Arbeitssicherheit_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Aufsicht">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Aufsicht.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Aufsicht.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Aufsicht.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Aufsicht.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Aufsicht_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Aufsicht_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Aufsicht_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Aufsicht_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Aufsicht_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Auge">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Auge.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Auge.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Auge.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Auge.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Auge_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Auge_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Auge_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Auge_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Auge_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Schloss">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/schloss.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/schloss.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/schloss.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/schloss.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Schloss_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/schloss_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/schloss_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/schloss_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/schloss_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Schloss_Kreis">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/schloss_kreis.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/schloss_kreis.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/schloss_kreis.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/schloss_kreis.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Schloss_Kreis_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/schloss_kreis_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/schloss_kreis_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/schloss_kreis_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/schloss_kreis_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="stop">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/stop.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/stop.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/stop.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/stop.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="stop_rot">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/stop_rot.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/stop_rot.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/stop_rot.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/stop_rot.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="stop_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/stop_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/stop_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/stop_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/stop_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Versicherung">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/versicherung.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/versicherung.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/versicherung.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/versicherung.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Versicherung_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/versicherung_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/versicherung_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/versicherung_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/versicherung_w.svg</div></div>
		</div>
	</div>
</div>

	<span class="ms-rteStyle-BoxClearing"> </span>
	<p>&nbsp;</p><p><a href="#oben">nach oben</a></p>
	
	<h1><a name="iconsn"></a>Kommunikation SVG Icons</h1>

<div class="ntm_KachelnNorm boxgrid " title="Chat">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/chat.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/chat.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/chat.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/chat.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Chat_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/chat_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/chat_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/chat_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/chat_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Dealteams">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Dealteams.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Dealteams.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Dealteams.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Dealteams.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Dealteams_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Dealteams_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Dealteams_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Dealteams_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Dealteams_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Frage">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/frage.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/frage.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/frage.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/frage.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Frage_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/frage_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/frage_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/frage_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/frage_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="fragezeichen">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/fragezeichen.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/fragezeichen.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/fragezeichen.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/fragezeichen.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="fragezeichen_gruen">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/fragezeichen_gruen.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/fragezeichen_gruen.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/fragezeichen_gruen.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/fragezeichen_gruen.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="fragezeichen_rot">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/fragezeichen_rot.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/fragezeichen_rot.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/fragezeichen_rot.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/fragezeichen_rot.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="fragezeichen_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/fragezeichen_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/fragezeichen_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/fragezeichen_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/fragezeichen_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="group1">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/group1.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/group1.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/group1.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/group1.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="info">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/info.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/info.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/info.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/info.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="info_rot">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/info_rot.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/info_rot.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/info_rot.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/info_rot.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="info_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/info_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/info_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/info_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/info_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="info1">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/info1.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/info1.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/info1.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/info1.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Kommunikation">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/kommunikation.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/kommunikation.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/kommunikation.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/kommunikation.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Kommunikation_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/kommunikation_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/kommunikation_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/kommunikation_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/kommunikation_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Kopf">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/kopf.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/kopf.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/kopf.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/kopf.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Kopf_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/kopf_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/kopf_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/kopf_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/kopf_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Leitsaetze">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Leitsaetze.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Leitsaetze.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Leitsaetze.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Leitsaetze.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Leitsaetze_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Leitsaetze_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Leitsaetze_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Leitsaetze_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Leitsaetze_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="LessonLearned">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/LessonLearned.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/LessonLearned.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/LessonLearned.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/LessonLearned.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="LessonLearned_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/LessonLearned_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/LessonLearned_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/LessonLearned_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/LessonLearned_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="meeting">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/meeting.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/meeting.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/meeting.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/meeting.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="meeting_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/meeting_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/meeting_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/meeting_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/meeting_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="meeting-room">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/meeting-room.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/meeting-room.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/meeting-room.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/meeting-room.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="meeting-room_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/meeting-room_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/meeting-room_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/meeting-room_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/meeting-room_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Pressemitteilungen">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Pressemitteilungen.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Pressemitteilungen.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Pressemitteilungen.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Pressemitteilungen.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Pressemitteilungen_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Pressemitteilungen_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Pressemitteilungen_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Pressemitteilungen_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Pressemitteilungen_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Pressespiegel">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Pressespiegel.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Pressespiegel.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Pressespiegel.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Pressespiegel.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Pressespiegel_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Pressespiegel_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Pressespiegel_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Pressespiegel_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Pressespiegel_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Sitzungsraum">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Sitzungsraum.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Sitzungsraum.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Sitzungsraum.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Sitzungsraum..svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Sitzungsraum_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Sitzungsraum_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Sitzungsraum_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Sitzungsraum_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Sitzungsraum_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="support">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/support.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/support.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/support.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/support.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="support_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/support_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/support_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/support_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/support_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="teamwork">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/teamwork.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/teamwork.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/teamwork.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/teamwork.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="telefon">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/telefon.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/telefon.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/telefon.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/telefon.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="telefon_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/telefon_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/telefon_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/telefon_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/telefon_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="users">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/users.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/users.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/users.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/users.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="users">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/users_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/users_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/users_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/users_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="users1">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/users1.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/users1.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/users1.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/users1.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="users2">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/users2.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/users2.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/users2.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/users2.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="users2">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/users2_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/users2_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/users2_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/users2_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="users3">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/users3.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/users3.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/users3.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/users3.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Veranstaltung">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/veranstaltung.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/veranstaltung.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/veranstaltung.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/veranstaltung.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Veranstaltung_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/veranstaltung_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/veranstaltung_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/veranstaltung_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/veranstaltung_w.svg</div></div>
		</div>
	</div>
</div>

	<span class="ms-rteStyle-BoxClearing"> </span>
	<p>&nbsp;</p><p><a href="#oben">nach oben</a></p>
	
	<h1><a name="iconsn"></a>Finanzen SVG Icons</h1>

<div class="ntm_KachelnNorm boxgrid " title="Beschaffung">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/beschaffung.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/beschaffung.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/beschaffung.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/beschaffung.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Beschaffung_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/beschaffung_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/beschaffung_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/beschaffung_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/beschaffung_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Euro">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Euro.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Euro.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Euro.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Euro.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Euro_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Euro_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Euro_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Euro_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Euro_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="European-Euro">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/European-Euro.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/European-Euro.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/European-Euro.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/European-Euro.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="European-Euro_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/European-Euro_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/European-Euro_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/European-Euro_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/European-Euro_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Finance1">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Finance1.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Finance1.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Finance1.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Finance1.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Finance1_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Finance1_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Finance1_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Finance1_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Finance1_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Finance2">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Finance2.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Finance2.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Finance2.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Finance2.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Finance2_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Finance2_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Finance2_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Finance2_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Finance2_w.svg</div></div>
		</div>
	</div>
</div>	
<div class="ntm_KachelnNorm boxgrid " title="Finance3">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Finance3.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Finance3.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Finance3.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Finance3.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Finance3_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Finance3_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Finance3_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Finance3_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Finance3_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Fund">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/fund.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/fund.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/fund.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/fund.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Fund_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/fund_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/fund_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/fund_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/fund_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Geschenk">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/geschenk.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/geschenk.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/geschenk.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/geschenk.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Geschenk_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/geschenk_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/geschenk_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/geschenk_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/geschenk_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Immobilien">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/immobilien.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/immobilien.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/immobilien.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/immobilien.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Immobilien_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/immobilien_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/immobilien_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/immobilien_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/immobilien_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="sparschwein">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/sparschwein.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/sparschwein.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/sparschwein.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/sparschwein.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="sparschwein_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/sparschwein_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/sparschwein_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/sparschwein_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/sparschwein_w.svg</div></div>
		</div>
	</div>
</div>	
<div class="ntm_KachelnNorm boxgrid " title="sparschwein1">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/sparschwein1.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/sparschwein1.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/sparschwein1.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/sparschwein1.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="sparschwein1_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/sparschwein1_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/sparschwein1_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/sparschwein1_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/sparschwein1_w.svg</div></div>
		</div>
	</div>
</div>	
<div class="ntm_KachelnNorm boxgrid " title="Spende">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/spende.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/spende.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/spende.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/spende.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Spende_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/spende_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/spende_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/spende_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/spende_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Steuern">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/steuern.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/steuern.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/steuern.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/steuern.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Steuern_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/steuern_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/steuern_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/steuern_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/steuern_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Werbung">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/werbung.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/werbung.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/werbung.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/werbung.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Werbung_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/werbung_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/werbung_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/werbung_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/werbung_w.svg</div></div>
		</div>
	</div>
</div>

	<span class="ms-rteStyle-BoxClearing"> </span>
	<p>&nbsp;</p><p><a href="#oben">nach oben</a></p>
	
	<h1><a name="iconsn"></a>Zeit SVG Icons</h1>
	
<div class="ntm_KachelnNorm boxgrid " title="Alarm">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/alarm.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/alarm.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/alarm.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/alarm.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Alarm">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/alarm_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/alarm_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/alarm_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/alarm_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="DateTime">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/DateTime.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/DateTime.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/DateTime.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/DateTime.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="DateTime_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/DateTime_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/DateTime_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/DateTime_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/DateTime_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Gebucht">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Gebucht.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Gebucht.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Gebucht.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Gebucht.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Gebucht_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Gebucht_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Gebucht_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Gebucht_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Gebucht_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="neu1">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/neu1.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/neu1.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/neu1.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/neu1.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="neu_gruen">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/neu_gruen.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/neu_gruen.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/neu_gruen.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/neu_gruen.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="neu_rot">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/neu_rot.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/neu_rot.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/neu_rot.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/neu_rot.svg</div></div>
		</div>
	</div>
</div><div class="ntm_KachelnNorm boxgrid " title="neu">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/neu.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/neu.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/neu.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/neu.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="neu_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/neu_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/neu_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/neu_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/neu_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Stoppuhr">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/stoppuhr.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/stoppuhr.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/stoppuhr.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/stoppuhr.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Stoppuhr_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/stoppuhr_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/stoppuhr_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/stoppuhr_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/stoppuhr_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Stoppuhr1">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/stoppuhr1.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/stoppuhr1.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/stoppuhr1.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/stoppuhr1.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Stoppuhr1_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/stoppuhr1_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/stoppuhr1_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/stoppuhr1_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/stoppuhr1_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Termin">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/termin.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/termin.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/termin.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/termin.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Termin_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/termin_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/termin_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/termin_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/termin_w.svg</div></div>
		</div>
	</div>
</div>

<div class="ntm_KachelnNorm boxgrid " title="Uhr">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/uhr.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/uhr.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/uhr.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/uhr.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Uhr_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/uhr_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/uhr_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/uhr_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/uhr_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Wecker">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/wecker.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/wecker.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/wecker.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/wecker.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Wecker_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/wecker_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/wecker_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/wecker_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/wecker_w.svg</div></div>
		</div>
	</div>
</div>

	<span class="ms-rteStyle-BoxClearing"> </span>
	<p>&nbsp;</p><p><a href="#oben">nach oben</a></p>
	
	<h1><a name="iconsn"></a>Umwelt und Soziales SVG Icons</h1>

<div class="ntm_KachelnNorm boxgrid " title="Baloon">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Baloon.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Baloon.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Baloon.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Baloon.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Baloon_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Baloon_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Baloon_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Baloon_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Baloon_w.svg</div></div>
		</div>
	</div>
</div>	
<div class="ntm_KachelnNorm boxgrid " title="BetrieblicheAltersvorsorge">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/BetrieblicheAltersvorsorge.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/BetrieblicheAltersvorsorge.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/BetrieblicheAltersvorsorge.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/BetrieblicheAltersvorsorge.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="BetrieblicheAltersvorsorge_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/BetrieblicheAltersvorsorge_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/BetrieblicheAltersvorsorge_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/BetrieblicheAltersvorsorge_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/BetrieblicheAltersvorsorge_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="familie">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/familie.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/familie.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/familie.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/familie.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="familie_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/familie_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/familie_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/familie_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/familie_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="geburtstag">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/geburtstag.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/geburtstag.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/geburtstag.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/geburtstag.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="geburtstag_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/geburtstag_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/geburtstag_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/geburtstag_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/geburtstag_w.svg</div></div>
		</div>
	</div>
</div>		
<div class="ntm_KachelnNorm boxgrid " title="global">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/global.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/global.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/global.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/global.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="global_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/global_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/global_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/global_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/global_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="globe">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/globe.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/globe.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/globe.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/globe.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="globe1">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/globe1.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/globe1.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/globe1.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/globe1.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Globus">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/globus.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/globus.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/globus.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/globus.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Globus_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/globus_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/globus_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/globus_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/globus_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Haus">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/haus.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/haus.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/haus.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/haus.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Haus_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/haus_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/haus_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/haus_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/haus_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/herz.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/herz.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/herz.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/herz.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/herz_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/herz_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/herz_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/herz_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/herz_rot.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/herz_rot.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/herz_rot.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/herz_rot.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="kids">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/kids.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/kids.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/kids.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/kids.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="kids_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/kids_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/kids_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/kids_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/kids_w.svg</div></div>
		</div>
	</div>
</div>	
<div class="ntm_KachelnNorm boxgrid " title="Nachhaltigkeit">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/nachhaltigkeit.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/nachhaltigkeit.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/nachhaltigkeit.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/nachhaltigkeit.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Nachhaltigkeit_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/nachhaltigkeit_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/nachhaltigkeit_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/nachhaltigkeit_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/nachhaltigkeit_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="OldMan">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/OldMan.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/OldMan.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/OldMan.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/OldMan.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="OldMan_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/OldMan_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/OldMan_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/OldMan_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/OldMan_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Personalentwicklung">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Personalentwicklung.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Personalentwicklung.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Personalentwicklung.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Personalentwicklung.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Personalentwicklung_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Personalentwicklung_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Personalentwicklung_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Personalentwicklung_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Personalentwicklung_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Personalveraenderungen">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Personalveraenderungen.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Personalveraenderungen.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Personalveraenderungen.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Personalveraenderungen.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Personalveraenderungen_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Personalveraenderungen_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Personalveraenderungen_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Personalveraenderungen_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Personalveraenderungen_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="SBV">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/SBV.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/SBV.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/SBV.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/SBV.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="SBV_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/SBV_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/SBV_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/SBV_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/SBV_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Sozial">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/sozial.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/sozial.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/sozial.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/sozial.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Sozial_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/sozial_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/sozial_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/sozial_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/sozial_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Steckdose">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Steckdose.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Steckdose.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Steckdose.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Steckdose.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Steckdose_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Steckdose_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Steckdose_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Steckdose_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Steckdose_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="striped-sphere1">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/striped-sphere1.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/striped-sphere1.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/striped-sphere1.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/striped-sphere1.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="striped-sphere1_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/striped-sphere1_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/striped-sphere1_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/striped-sphere1_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/striped-sphere1_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="striped-sphere2">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/striped-sphere2.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/striped-sphere2.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/striped-sphere2.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/striped-sphere2.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="striped-sphere2_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/striped-sphere2_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/striped-sphere2_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/striped-sphere2_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/striped-sphere2_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Stromstecker">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Stromstecker.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Stromstecker.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Stromstecker.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Stromstecker.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Stromstecker_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Stromstecker_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Stromstecker_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Stromstecker_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Stromstecker_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Sun">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Sun.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Sun.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Sun.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Sun.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Sun_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Sun_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Sun_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Sun_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Sun_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Umwelt">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/umwelt.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/umwelt.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/umwelt.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/umwelt.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Umwelt_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/umwelt_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/umwelt_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/umwelt_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/umwelt_w.svg</div></div>
		</div>
	</div>
</div>

	<span class="ms-rteStyle-BoxClearing"> </span>
	<p>&nbsp;</p><p><a href="#oben">nach oben</a></p>

    <h1><a name="iconsn"></a>Gesundheit SVG Icons</h1>

<div class="ntm_KachelnNorm boxgrid " title="Apfel">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Apfel.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Apfel.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Apfel.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Apfel.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Apfel_gruen">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Apfel_gruen.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Apfel_gruen.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Apfel_gruen.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Apfel_gruen.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Apfel_rot">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Apfel_rot.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Apfel_rot.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Apfel_rot.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Apfel_rot.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Apfel_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Apfel_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Apfel_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Apfel_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Apfel_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Erste-Hilfe">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Erste-Hilfe.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Erste-Hilfe.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Erste-Hilfe.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Erste-Hilfe.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Erste-Hilfe_gruen">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Erste-Hilfe_gruen.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Erste-Hilfe_gruen.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Erste-Hilfe_gruen.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Erste-Hilfe_gruen.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Erste-Hilfe_rot">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Erste-Hilfe_rot.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Erste-Hilfe_rot.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Erste-Hilfe_rot.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Erste-Hilfe_rot.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Erste-Hilfe_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Erste-Hilfe_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Erste-Hilfe_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Erste-Hilfe_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Erste-Hilfe_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Erste-Hilfe1">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Erste-Hilfe1.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Erste-Hilfe1.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Erste-Hilfe1.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Erste-Hilfe1.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Erste-Hilfe-Koffer">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Erste-Hilfe-Koffer.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Erste-Hilfe-Koffer.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Erste-Hilfe-Koffer.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Erste-Hilfe-Koffer.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Erste-Hilfe-Koffer_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Erste-Hilfe-Koffer_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Erste-Hilfe-Koffer_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Erste-Hilfe-Koffer_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Erste-Hilfe-Koffer_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Gesundheit">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Gesundheit.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Gesundheit.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Gesundheit.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Gesundheit.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Gesundheit_rot">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Gesundheit_rot.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Gesundheit_rot.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Gesundheit_rot.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Gesundheit_rot.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Gesundheit_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Gesundheit_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Gesundheit_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Gesundheit_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Gesundheit_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Gesundheit1">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Gesundheit1.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Gesundheit1.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Gesundheit1.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Gesundheit1_rot">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Gesundheit1_rot.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Gesundheit1_rot.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Gesundheit1_rot.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Gesundheit1_rot.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Gesundheit1_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Gesundheit1_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Gesundheit1_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Gesundheit1_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Gesundheit1_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="hospital">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/hospital.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/hospital.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/hospital.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/hospital.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="hospital-sign">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/hospital-sign.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/hospital-sign.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/hospital-sign.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/hospital-sign.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="hospital-sign_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/hospital-sign_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/hospital-sign_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/hospital-sign_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/hospital-sign_w.svg</div></div>
		</div>
	</div>
</div>

	<span class="ms-rteStyle-BoxClearing"> </span>
	<p>&nbsp;</p><p><a href="#oben">nach oben</a></p>
	
	<h1><a name="iconsn"></a>Sport SVG Icons</h1>
	
<div class="ntm_KachelnNorm boxgrid " title="Badminton">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/badminton.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/badminton.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/badminton.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/badminton.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Badminton_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/badminton_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/badminton_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/badminton_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/badminton_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Basketball">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/basketball.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/basketball.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/basketball.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/basketball.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Basketball_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/basketball_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/basketball_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/basketball_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/basketball_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Bowling">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/bowling.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/bowling.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/bowling.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/bowling.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Bowling_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/bowling_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/bowling_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/bowling_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/bowling_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Drachenbootrennen">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Drachenbootrennen.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Drachenbootrennen.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Drachenbootrennen.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Drachenbootrennen.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Drachenbootrennen_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Drachenbootrennen_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Drachenbootrennen_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Drachenbootrennen_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Drachenbootrennen_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Fussball">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/fussball.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/fussball.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/fussball.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/fussball.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Fussball_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/fussball_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/fussball_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/fussball_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/fussball_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Golf">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/golf.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/golf.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/golf.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/golf.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Golf_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/golf_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/golf_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/golf_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/golf_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Kicker">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Kicker.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Kicker.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Kicker.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Kicker.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Kicker_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Kicker_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Kicker_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Kicker_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Kicker_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Laufen">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/laufen.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/laufen.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/laufen.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/laufen.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Laufen_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/laufen_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/laufen_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/laufen_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/laufen_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Radsport">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Radsport.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Radsport.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Radsport.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Radsport.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Radsport_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Radsport_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Radsport_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Radsport_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Radsport_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Rueckengymnastik">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Rueckengymnastik.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Rueckengymnastik.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Rueckengymnastik.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Rueckengymnastik.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Rueckengymnastik_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Rueckengymnastik_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Rueckengymnastik_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Rueckengymnastik_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Rueckengymnastik_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Schwimmen">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Schwimmen.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Schwimmen.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Schwimmen.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Schwimmen.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Schwimmen_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Schwimmen_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Schwimmen_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Schwimmen_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Schwimmen_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Squash">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/squash.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/squash.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/squash.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/squash.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Squash_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/squash_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/squash_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/squash_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/squash_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Tennis">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/tennis.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/tennis.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/tennis.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/tennis.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Tennis_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/tennis_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/tennis_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/tennis_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/tennis_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Volleyball">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/volleyball.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/volleyball.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/volleyball.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/volleyball.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Volleyball_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/volleyball_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/volleyball_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/volleyball_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/volleyball_w.svg</div></div>
		</div>
	</div>
</div>	

	<span class="ms-rteStyle-BoxClearing"> </span>
	<p>&nbsp;</p><p><a href="#oben">nach oben</a></p>
	
	<h1><a name="iconsn"></a>Diverse SVG Icons</h1>
	
<div class="ntm_KachelnNorm boxgrid " title="Ampelfrau_gruen">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Ampelfrau_gruen.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Ampelfrau_gruen.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Ampelfrau_gruen.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Ampelfrau_gruen.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Ampelfrau_rot">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Ampelfrau_rot.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Ampelfrau_rot.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Ampelfrau_rot.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Ampelfrau_rot.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Ampelmann">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Ampelmann.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Ampelmann.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Ampelmann.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Ampelmann.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Ampelmann_gruen">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Ampelmann_gruen.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Ampelmann_gruen.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Ampelmann_gruen.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Ampelmann_gruen.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Ampelmann_gruen_rechts">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Ampelmann_gruen_rechts.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Ampelmann_gruen_rechts.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Ampelmann_gruen_rechts.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Ampelmann_gruen_rechts.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Ampelmann_rot">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/Ampelmann_rot.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/Ampelmann_rot.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/Ampelmann_rot.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/Ampelmann_rot.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Iso">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/iso.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/iso.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/iso.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/iso.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="Iso_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/iso_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/iso_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/iso_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/iso_w.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="reserviert">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/reserviert.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/reserviert.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/reserviert.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/reserviert.svg</div></div>
		</div>
	</div>
</div>
<div class="ntm_KachelnNorm boxgrid " title="reserviert_w">
	<div class="ntm_CtIcon Hellblau" style="background-image: url('/_layouts/15/ZAP/Icons/reserviert_w.svg');">
		<div class="cover boxcaption" link="/_layouts/15/ZAP/Icons/reserviert_w.svg" target="_self" >
			<div class="ntm_BoxCT"><div class="ntm_BoxTitle">/_layouts/15/ZAP/Icons/reserviert_w.svg</div></div>
			<div class="ntm_BoxCT"><div class="ntm_BoxText">/_layouts/15/ZAP/Icons/reserviert_w.svg</div></div>
		</div>
	</div>
</div>

	<span class="ms-rteStyle-BoxClearing"> </span>
	<p>&nbsp;</p><p><a href="#oben">nach oben</a></p>


  <p>&nbsp;</p>  

</asp:Content>
<%@ Register TagPrefix="wssuc" TagName="TopNavBar" src="~/_controltemplates/15/TopNavBar.ascx" %>
<asp:Content ID="Content6" contentplaceholderid="PlaceHolderTopNavBar" runat="server">
  <wssuc:TopNavBar id="IdTopNavBar" runat="server" Version="4" ShouldUseExtra="true"/>
</asp:Content>
<asp:Content ID="Content7" contentplaceholderid="PlaceHolderHorizontalNav" runat="server"/>
<asp:Content ID="Content8" contentplaceholderid="PlaceHolderSearchArea" runat="server"/>
<asp:Content ID="Content9" contentplaceholderid="PlaceHolderTitleBreadcrumb" runat="server">
  <SharePoint:UIVersionedContent ID="UIVersionedContent1" UIVersion="3" runat="server"><ContentTemplate>
	<asp:SiteMapPath
		SiteMapProvider="SPXmlContentMapProvider"
		id="ContentMap"
		SkipLinkText=""
		NodeStyle-CssClass="ms-sitemapdirectional"
		RootNodeStyle-CssClass="s4-die"
		PathSeparator="&#160;&gt; "
		PathSeparatorStyle-CssClass = "s4-bcsep"
		runat="server" />
  </ContentTemplate></SharePoint:UIVersionedContent>
  <SharePoint:UIVersionedContent ID="UIVersionedContent2" UIVersion="4" runat="server"><ContentTemplate>
	<SharePoint:ListSiteMapPath
		runat="server"
		SiteMapProviders="SPSiteMapProvider,SPXmlContentMapProvider"
		RenderCurrentNodeAsLink="false"
		PathSeparator=""
		CssClass="s4-breadcrumb"
		NodeStyle-CssClass="s4-breadcrumbNode"
		CurrentNodeStyle-CssClass="s4-breadcrumbCurrentNode"
		RootNodeStyle-CssClass="s4-breadcrumbRootNode"
		HideInteriorRootNodes="true"
		SkipLinkText="" />
  </ContentTemplate></SharePoint:UIVersionedContent>
</asp:Content>
