<%@ Assembly Name="Microsoft.SharePoint.ApplicationPages, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%> <%@ Page Language="C#" DynamicMasterPageFile="~masterurl/default.master" Inherits="Microsoft.SharePoint.ApplicationPages.SettingsPage"   EnableViewState="false"	%> <%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %> <%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Import Namespace="Microsoft.SharePoint" %> <%@ Assembly Name="Microsoft.Web.CommandUI, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="wssawc" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> <%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=16.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Register TagPrefix="wssuc" TagName="LinkSection" src="~/_controltemplates/15/LinkSection.ascx" %>

<%@ Import Namespace="Microsoft.SharePoint.Publishing" %>
<asp:Content ID="Content1" contentplaceholderid="PlaceHolderPageTitle" runat="server">
	Site Hierarchie
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="PlaceHolderPageDescription">
	This page shows Sites and their hierarchical subsites
</asp:Content>
<asp:Content ID="Content2" ContentPlaceholderID="PlaceHolderAdditionalPageHead" runat="server">
	<style type="text/css">

		.ntm_SiteMapCont ul {
			margin-bottom	: .4em;
		}
		.ntm_BodyCont,
        #NTM_MainNavCont {
			max-width		: 1148px;
			margin-left		: auto;
			margin-right	: auto;
		}
		#s4-titlerow,
        #NTM_MainNavCont {position: relative;}

	</style>
</asp:Content>
<asp:Content ID="Content4" contentplaceholderid="PlaceHolderPageImage" runat="server"/>
<asp:Content ID="Content5" contentplaceholderid="PlaceHolderMain" runat="server">
	<!-- 
	/_layouts/15/ntm_SiteHierarchieZAP.aspx
	/_layouts/15/ntm_SiteHierarchieZAP.aspx?ntm_showListsPermission=1
	ms-rtestate-field 
	-->
<div class="ntm_SiteMapCont">
	<%
		bool showLists = false;
		try
		{
			if (!string.IsNullOrEmpty(Page.Request.QueryString["ntm_showListsPermission"]))
			{
				showLists = true;
			}
			SPSecurity.RunWithElevatedPrivileges(delegate()
			{
				using (SPSite siteCollection = SPControl.GetContextSite(Context))
				{
					using (SPWeb site = siteCollection.OpenWeb())
					{
						site.Site.CatchAccessDeniedException = false;
						int i = 1;

						//Start Websitesammlungs Root
						//Response.Write("<ul class=\"ntm_UListStart\"><li><b><a href=\""+site.Url +"\" title=\"" + site.Url.ToString() + "\">" + site.Title.ToString() + "</a></b></li>");

						//Pages 
						PublishingWeb publishingWeb = null;
						
						if (PublishingWeb.IsPublishingWeb(site))
						{
							publishingWeb = PublishingWeb.GetPublishingWeb(site);
							PublishingPageCollection pages = publishingWeb.GetPublishingPages();

							//Start Websitesammlungs Root Pages
							Response.Write("<ul class=\"ntm_UListSub\">");
							string letter = string.Empty;
							foreach (PublishingPage page in pages)
							{
								var thisPageName = page.Name.ToString();
								if (thisPageName == "PageNotFoundError.aspx" || thisPageName == "conversationresults.aspx") {
								}
								else {
									letter = Successor(letter);
									Response.Write("<li><i><a href=\"" + page.PublishingWeb.Url + "/" + page.Url.ToString() + "\"  title=\"\">" + page.Title.ToString() + "</a></i></li>");
									//Response.Write("<li><i><a href=\"" + page.PublishingWeb.Url + "/" + page.Url.ToString() + "\"  title=\"" + page.Name.ToString() + "\">" + page.Title.ToString() + "</a></i></li>");
								}
							}
							Response.Write("</ul>");

						}

						if (showLists)
						{

							Response.Write("<ul>");
							Response.Write("<li><b>Lists und Libs</b></li>");
							foreach (SPList list in site.Lists)
							{
								Response.Write("<li><a href=\"" + list.DefaultViewUrl + "\">" + list.Title + "</a>");
								if (list.HasUniqueRoleAssignments)
								{
									Response.Write("  <img src='/_layouts/15/images/ZAP/berechtigung_eigene.png' alt='eigene Berechtigung'  title='eigene Berechtigung' />");

									Response.Write("<ul>");
									try
									{
										foreach (SPRoleAssignment ass in list.RoleAssignments)
										{
											Response.Write("<li>" + ass.Member.Name.ToString() + " - Perms: ");
											foreach (SPRoleDefinition def in ass.RoleDefinitionBindings)
											{
												Response.Write(def.Name + ", ");
											}
											Response.Write("</li>");
										}
									}
									catch (System.UnauthorizedAccessException e)
									{
									}
									Response.Write("</ul>");
								}
								else Response.Write(" <img src='/_layouts/15/images/ZAP/berechtigung_vererbt.png' alt='vererbte Berechtigung' title='vererbte Berechtigung' /> ");

								Response.Write("</li>");
							}
							Response.Write("</ul>");
						}
						getSubWeb(site, i.ToString(),showLists);
						//Start Websitesammlungs Root Ende
						//Response.Write("</ul>");

					}
				}
			});


		}
		catch (Exception x)
		{
			Response.Write(x.Message);
		}

	%>
</div>

<script runat="server">
	
	private string Successor(string structureString)
	{
		if (string.IsNullOrEmpty(structureString))
			return "a";

		for (int i = structureString.Length - 1; i >= 0; --i)
		{
			if (structureString[i] < 'z')
			{
				char c = structureString[i];
				c++;
				StringBuilder builder = new StringBuilder();
				if (i > 0)
					builder.Append(structureString.Substring(0, i));
				builder.Append(c);
				if (i < structureString.Length - 1)
					builder.Append(new string('a', structureString.Length - i - 1));
				return builder.ToString();
			}
		}

		return new string('a', structureString.Length + 1);
	}

	private SPWebCollection getSubWeb(SPWeb web, string strPrefix, bool showLists)
	{
		int i = 0;
		int y = 0;

		SPWebCollection coll = web.GetSubwebsForCurrentUser();
		PublishingWeb publishingWeb = null;

		Response.Write("<ul>");
		SPSecurity.RunWithElevatedPrivileges(delegate()
		{

			web.Site.CatchAccessDeniedException = false;
			foreach (SPWeb myWeb in coll)
			{

				Response.Write("<li title=\"" + myWeb.Url + "\"><b><a href=\"" + myWeb.Url + "\" title=\"" + myWeb.Url + "\">" + myWeb.Title + "</a></b></li>");

				if (showLists)
				{
				   
					Response.Write("<ul>");
					Response.Write("<li><b>Lists und Libs</b></li>");

					foreach (SPList list in myWeb.Lists)
					{
						Response.Write("<li><a href=\"" + list.DefaultViewUrl + "\">" + list.Title + "</a>");
						if (list.HasUniqueRoleAssignments)
						{
							Response.Write("  <img src='/_layouts/15/images/ZAP/berechtigung_eigene.png' alt='eigene Berechtigungen' title='eigene Berechtigungen' />");

							Response.Write("<ul>");
							try
							{
								foreach (SPRoleAssignment ass in list.RoleAssignments)
								{
									Response.Write("<li>" + ass.Member.Name.ToString() + " - Perms: ");
									foreach (SPRoleDefinition def in ass.RoleDefinitionBindings)
									{
										Response.Write(def.Name + ", ");
									}
									Response.Write("</li>");
								}
							}
							catch (UnauthorizedAccessException)
							{
							}
							Response.Write("</ul>");
						}
						else Response.Write(" <img src='/_layouts/15/images/ZAP/berechtigung_vererbt.png' alt='vererbte Berechtigung' title='vererbte Berechtigung' />");

						Response.Write("</li>");
					}
					Response.Write("</ul>");

				}
				i++;
		   		y++;

				//Pages 
				publishingWeb = PublishingWeb.GetPublishingWeb(myWeb);
				if (PublishingWeb.IsPublishingWeb(myWeb))
				{
					PublishingPageCollection pages = publishingWeb.GetPublishingPages();
					Response.Write("<ul class=\"ntm_UListSub"+ y +"\">");
					
					string letter = string.Empty;
					foreach (PublishingPage page in pages)
					{
						var thisPageName = page.Name.ToString();
						if (thisPageName == "PageNotFoundError.aspx" || thisPageName == "conversationresults.aspx" || thisPageName == "default.aspx" || thisPageName == "home.aspx") {
						}
						else {
							letter = Successor(letter);
							Response.Write("<li><i><a href=\"" + page.PublishingWeb.Url + "/" + page.Url.ToString() + "\" title=\"\">" + page.Title.ToString() + "</a></i></li>");
							//Response.Write("<li><i><a href=\"" + page.PublishingWeb.Url + "/" + page.Url.ToString() + "\" title=\"" + page.Name.ToString() + "\">" + page.Title.ToString() + "</a></i></li>");
						}
					}

					Response.Write("</ul>");

				}
				coll = getSubWeb(myWeb, strPrefix + "." + i, showLists);
			}
			Response.Write("</ul>");
		});
		return coll;
	}

</script>
	<div style="display:none;">
	<SharePoint:FeatureLinkSections runat="server"
		id="SettingLinksV4"
		Location="Microsoft.SharePoint.SiteSettings"
		OnAddGroup="OnAddGroup"
		OnAddLink="OnAddLink"
		LinkSectionControl="LinkSectionLevel1.ascx" />
	</div>
</asp:Content>

<asp:Content ID="Content6" contentplaceholderid="PlaceHolderTitleBreadcrumb" runat="server">
	<SharePoint:ListSiteMapPath ID="ListSiteMapPath1"
		runat="server"
		SiteMapProviders="SPSiteMapProvider,SPXmlContentMapProvider"
		RenderCurrentNodeAsLink="false"
		PathSeparator=""
		CssClass="s4-breadcrumb"
		NodeStyle-CssClass="s4-breadcrumbNode"
		CurrentNodeStyle-CssClass="s4-breadcrumbCurrentNode"
		RootNodeStyle-CssClass="s4-breadcrumbRootNode"
		HideInteriorRootNodes="true"
		SkipLinkText="" />
  </ContentTemplate>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderId="PlaceHolderPageTitleInTitleArea" runat="server">
	Sitemap
</asp:Content>
